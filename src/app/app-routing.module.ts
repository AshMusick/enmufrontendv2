import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { LoginComponent } from '../app/login/login.component';
import { AuthGuard } from './core-services/auth.guard';
import { MainMenuComponent } from '../app/main-menu/main-menu.component';


const appRoutes: Routes = [
  { path: 'pages', 
    loadChildren: () => import('../app/pages/pages.module').then(m => m.PagesModule), 
    canActivate: [AuthGuard] 
  },
  { path: 'main', component: MainMenuComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
   // otherwise redirect to main
  { path: '**', redirectTo: 'main' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, 
      { preloadingStrategy: PreloadAllModules, 
      enableTracing: false, 
      useHash : true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
