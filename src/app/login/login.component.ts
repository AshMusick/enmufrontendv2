import { Component, OnInit } from '@angular/core';
import { User, Login } from './login';
import {LoginService} from './login.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../alert/alert.service'
import { AuthService } from '../core-services/auth.service'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [LoginService, FormBuilder, AlertService],
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  signUpForm : FormGroup;
  loginForm : FormGroup;
  users: User[];
  currentUser: User;

  session: boolean = false;
  submitted: boolean = false;
  loading: boolean = false;
  returnUrl: string;

  registering: boolean = false;

  constructor(private loginService: LoginService, 
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private authService: AuthService) { 

  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/main';
    if(this.authService.currentUser){
      this.router.navigate([this.returnUrl])
    }
      this.loginForm = this.formBuilder.group({
        user: ['', Validators.required],
        password: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]]   
    });
    this.signUpForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', Validators.required],
      firstName: ['', [Validators.required]],
      lastName: ['',[Validators.required]],
      email: ['', [Validators.required, Validators.email]]   
  }, {
    validator: MustMatch('password', 'confirmPassword')
});
    
  }

  toggleForm(){
    this.registering = !this.registering;
  }

  submitRegistrationForm(){
    let user = {
          password1 : this.signUpForm.value.password,
          password2: this.signUpForm.value.confirmPassword,
          first_name : this.signUpForm.value.firstName,
          last_name : this.signUpForm.value.lastName,
          email : this.signUpForm.value.email,
          username : this.signUpForm.value.userName
        }
    let username = user.username
    this.loginService.register(user)
    .subscribe(tokenObject =>{
      let token = tokenObject.key;
      const myUser: User = { username, token } as User;
      this.authService.login(myUser);
      this.loginService.getUserDetails(token)
        .subscribe(user =>{
          if(user.pk){
            this.loginService.getUser(token, user.pk)
              .subscribe(_user=>{
                if(_user.id){
                  this.authService.updateUser(_user);
                  this.alertService.success('Registración fue éxitosa! Si la página no redirecciona, por favor refrescá la página.'+
                  'En caso de ser una aplicación, cerrala completamente y volvé a abrir.', true);
                  this.router.navigate([this.returnUrl])
                }
              });
            }
        });
    })
  }

  submitForm() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
        this.alertService.error('Se debe llenar todos los campos con datos válidos.')
        return;
    }

    this.loading = true;
    let username = this.f.user.value;
    let email = this.f.email.value;
    let password = this.f.password.value;
    let login: Login = {username, email, password} as Login;
    this.loginService.logIn(login)
      .subscribe((tokenObject) => {
      if(tokenObject.key){
        let token = tokenObject.key;
        const myUser: User = { username, password, token } as User;
        this.authService.login(myUser);
        this.loginService.getUserDetails(token)
          .subscribe(user =>{
            if(user.pk){
              this.loginService.getUser(token, user.pk)
                .subscribe(_user=>{
                  if(_user.id){
                    this.authService.updateUser(_user);
                    this.alertService.success('Login fue éxitosa! Si la página no redirecciona, por favor refrescá la página.'+
                    'En caso de ser una aplicación, cerrala completamente y volvé a abrir.', true);
                    this.router.navigate([this.returnUrl])
                  }
                })
              
            }
            
          })

      } else {
        this.alertService.error('Login inválido. Por favor volver a intentar.'); 
        this.loading = false;
      }
      
      }), ()=>{
         this.alertService.error('Login inválido. Por favor volver a intentar.'); 
         this.loading = false;
      }
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  get ff() { return this.signUpForm.controls; }
}

function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}
