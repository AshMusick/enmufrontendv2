import { Injectable} from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { User, Token, Login } from './login';
import { HttpErrorHandler, HandleError } from '../core-services/http-error-handler.service';

import { environment } from '../../environments/environment'
import { AlertService } from '../alert/alert.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    //'Authorization': 'Token ' + localStorage.getItem('token')
  })
};

@Injectable({providedIn: 'root'})
export class LoginService {
  userUrl = environment.backendServer + '/api/users/'
  authUrl = environment.backendServer + '/api/rest-auth/login/'
  userDetails = environment.backendServer + '/api/rest-auth/user/'
  registerUrl = environment.backendServer + '/api/rest-auth/registration/'
  token: Token;
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler,
    private alertService: AlertService
    ) {
    this.handleError = httpErrorHandler.createHandleError('LoginService');
  }

  /** GET users from the server */
  getUsers (token): Observable<User[]> {
    httpOptions.headers =
      httpOptions.headers.set('Authorization', 'Token ' + token);
    return this.http.get<User[]>(this.userUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getUsers', []))
      );
  }

  /** GET my user from the server */
  getUser (token, id): Observable<User> {
    httpOptions.headers =
    httpOptions.headers.set('Authorization', 'Token ' + token);
    const user: User = {} as User;
    const url =`${this.userUrl}${id}/`;
    return this.http.get<User>(url, httpOptions)
      .pipe(
        catchError(this.handleError('getUsers', user))
      );
  }

  /** GET user details from the server */
  getUserDetails (token): Observable<any> {
    httpOptions.headers =
      httpOptions.headers.set('Authorization', 'Token ' + token);
    return this.http.get<User[]>(this.userDetails, httpOptions)
      .pipe(
        catchError(this.handleError('getUsers', []))
      );
  }

  logIn (login: Login): Observable<Token> {
    httpOptions.headers =
      httpOptions.headers.delete('Authorization');
    return this.http.post<Token> (this.authUrl, login, httpOptions)
      .pipe(
        catchError(this.handleError('authentication', this.token))
      )
  }

  /**POST: register as a new user, adds an inactive user with no privileges */
  register (user: any) : Observable<any>{
    httpOptions.headers =
      httpOptions.headers.delete('Authorization');
    return this.http.post<any>(this.registerUrl, user, httpOptions)
    .pipe(
      catchError(err => {
        const error = err.error;
        let message = "User was not created because: ";
        let meaningfulMessage = false;
        if(error.email) {
          meaningfulMessage = true;
          message+= `${error.email.join()}. `;
        }

        if(error.username) {
          meaningfulMessage = true;
          message+= `${error.username.join()}. `;
        }

        if(error.password1) {
          meaningfulMessage = true
          message+= `${error.password1.join()}. `;
        }

        if(error.password2) {
          meaningfulMessage = true
          message+= `${error.password2.join()}. `;
        }

        if(error.first_name) {
          meaningfulMessage = true
          message+= `${error.first_name.join()}. `;
        }

        if(error.last_name) {
          meaningfulMessage = true
          message+= `${error.last_name.join()}. `;
        }

        if (meaningfulMessage) {
          this.alertService.error(message);
        }
        return of(err);
      })
    );
  }

  /** POST: add a new user to the database as an admin */
  addUser (user: User, token): Observable<User> {
    httpOptions.headers =
      httpOptions.headers.set('Authorization', 'Token ' + token);
    return this.http.post<User>(this.userUrl, user, httpOptions)
      .pipe(
        catchError(this.handleError('addUser', user))
      );
  }

  /** PUT: update the user on the server. Returns the updated user upon success. */
  updateUser (user: User, token): Observable<User> {
    httpOptions.headers =
      httpOptions.headers.set('Authorization', 'Token ' + token);
      
      const url = `${this.userUrl}${user.id}/`;
      return this.http.put<User>(url, user, httpOptions)
      .pipe(
        catchError(this.handleError('updateUser', user))
      );
  }
}


