export interface User {
    id: number;
    password: string;
    is_staff: boolean;
    is_active: boolean;
    username: string;
    first_name: string;
    last_name: string;
    email: string;
    protocols: [];
    token: string;
}

export interface Token {
    key: string;
}

export interface Login {
    username: string;
    email: string;
    password: string;
}
