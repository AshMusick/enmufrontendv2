import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent }    from './login.component';
//import { HeroDetailComponent }  from './hero-detail/hero-detail.component';

const loginRoutes: Routes = [
  { path: 'login', redirectTo: '/login' },
  { path: 'login',  component: LoginComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(loginRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class LoginRoutingModuleModule { }