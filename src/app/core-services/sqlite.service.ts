import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Subject } from 'rxjs';

import { Answer } from '../pages/subtests/interfaces/question';
import { PossibleAnswer } from '../pages/subtests/interfaces/possible-answers';
import { Protocol, Participant } from '../pages/protocol/protocol';

@Injectable({
  providedIn: 'root'
})
export class SqliteService {

  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
 
  constructor(private plt: Platform, private sqlitePorter: SQLitePorter, private sqlite: SQLite, private http: HttpClient) {
    if(this.plt){
      if(this.plt.is('android')){
        this.plt.ready().then(() => {
          this.sqlite.create({
            name: 'enmu.db',
            location: 'default'
          })
          .then((db: SQLiteObject) => {
              this.database = db;
              console.log('about to seed')
              this.seedDatabase();
          });
        });
      }
    }
    
  }
 
  //function to create initial database structures if they don't already exist
  //ddl and dml come from enmu.sql en assets
  seedDatabase() {
    this.http.get('../../assets/enmu.sql', { responseType: 'text'})
    .subscribe(sql => {
      this.sqlitePorter.importSqlToDb(this.database, sql)
        .then(_ => {
          this.dbReady.next(true);
        })
        .catch(e => console.error(JSON.stringify(e)));
    });
  }

  getDatabaseState(){
    return this.dbReady.asObservable();
  }

  //generic functions for the cases when the interface in TypeScript is identical to the Json that comes from executeSql
  loadData(table : string) : Promise<any[]>{
    console.log(`about to load ${table}`)
    return this.database.executeSql(`SELECT * FROM ${table}`, [])
      .then(data =>{
        console.log(`just loaded ${table}`)
        let dataTable: any[] = [];
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            dataTable.push(data.rows.item(i));
          }
        }
        return dataTable;
      })
  }

  loadDataById(id : number, table : string, identityColumn : string) : Promise<any> {
    return this.database.executeSql(`SELECT * FROM ${table} where ${identityColumn} = ?`, [id]).then(data => {
      if (data.rows.length > 0) {
        return data.rows.item(0)
      }    
    });
  }

  loadLatest(table : string, identityColumn : string) {
    return this.database.executeSql(`SELECT * FROM ${table} where ${identityColumn} = (select max(${identityColumn}) from ${table})`, []).then(data => {
      if (data.rows.length > 0) {
        return data.rows.item(0);
      }   
    });
  }

  /*specific functions that we have to use when the interface of the object returned by executeSql
  is different from the TypeScript interface
  
  TODO: refactor interface keys to match sql columns so that more of the database queries can be
  executed generically */
  loadPossibleAnswers() : Promise<PossibleAnswer[]>{
    console.log('about to load possible answers')
    return this.database.executeSql('SELECT * FROM answer_bank', []).then(data => {
      let possibleAnswers: any[] = [];
      if (data.rows.length > 0) {
        console.log('loaded possible answers')
        for (var i = 0; i < data.rows.length; i++) {
          let topic = data.rows.item(i).topic_id;
          let answerId = data.rows.item(i).answer_id;
          let answer = data.rows.item(i).answer;
          let possibleAnswer : PossibleAnswer = {topic, answerId, answer} as PossibleAnswer;
          possibleAnswers.push(possibleAnswer);
        }
      }
      return possibleAnswers;
    });
  }

  loadProtocol(id : number) : Promise<Protocol> {
    return this.database.executeSql('SELECT * FROM protocols where id_protocol = ?', [id]).then(data => {
      if (data.rows.length > 0) {
        data.rows.item(0).test_admin = data.rows.item(0).test_admin_id;
        delete data.rows.item(0).test_admin_id;
        return data.rows.item(0);
      } else {
        console.log('protocol not found')
      }   
    }).catch(e => console.log(JSON.stringify(e)));
  }

  loadProtocolByOnlineId(onlineId : number) : Promise<Protocol> {
    return this.database.executeSql('SELECT * FROM protocols where online_id = ?', [onlineId]).then(data => {
      if (data.rows.length > 0) {
        data.rows.item(0).test_admin = data.rows.item(0).test_admin_id;
        delete data.rows.item(0).test_admin_id;
        return data.rows.item(0);
      }    
    });
  }

  loadProtocols() : Promise<Protocol[]> {
    return this.database.executeSql('SELECT * FROM protocols', []).then(data => {
      let protocols: any[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          data.rows.item(i).test_admin = data.rows.item(i).test_admin_id;
          delete data.rows.item(i).test_admin_id;
          protocols.push(data.rows.item(i));
        }
      }
      return protocols; 
    });
  }

  sendNewAnswer(answer : Answer) {
    const answerAsArray = [answer.id_item, answer.id_protocol, answer.answer, answer.id_attribute, (answer.online_id || null)]
    return this.database.executeSql('INSERT INTO answers (id_item, id_protocol, answer, id_attribute, online_id) VALUES (?, ?, ?, ?, ?)', answerAsArray).then(() =>{
      return this.loadLatest('answers', 'id_answer')
        .then(data => data);
    })
  }

  sendNewPossibleAnswer(possibleAnswer : PossibleAnswer) {
    const possibleAnswerAsArray = [possibleAnswer.answer, possibleAnswer.topic]
    return this.database.executeSql('INSERT INTO answer_bank (answer, topic_id) VALUES (?, ?)', possibleAnswerAsArray).then(data => {
      return this.loadLatestPossibleAnswer()
        .then(data => data);
    });
  }

  loadLatestPossibleAnswer() : Promise<PossibleAnswer> {
    return this.database.executeSql('SELECT * FROM answer_bank where answer_id = (select max(answer_id) from answer_bank)', []).then(data => {
      if (data.rows.length > 0) {
        let topic = data.rows.item(0).topic_id;
        let answerId = data.rows.item(0).answer_id;
        let answer = data.rows.item(0).answer;
        let possibleAnswer : PossibleAnswer = {topic, answerId, answer} as PossibleAnswer;
        return possibleAnswer;
      }   
    });
  }

 sendNewProtocol(protocol: Protocol) {
    const protocolAsArray = [protocol.age, 
                              protocol.case_num, 
                              protocol.group_num,
                              protocol.id_participant,
                              protocol.start_date,
                              protocol.test_admin,
                              (protocol.online_id||null)]
    return this.database.executeSql('INSERT INTO protocols (age, case_num, group_num, ' +
    'id_participant, start_date, test_admin_id, online_id)' +
    'VALUES (?, ?, ?, ?, ?, ?, ?)', protocolAsArray).then(() => {
      return this.loadLatestProtocol()
        .then(data => data).catch(e => console.log(JSON.stringify(e)));
    }).catch(e => console.log(JSON.stringify(e)));
  }

 loadLatestProtocol() {
    return this.database.executeSql('SELECT * FROM protocols where id_protocol = (select max(id_protocol) from protocols)', []).then(data => {
      if (data.rows.length > 0) {
        data.rows.item(0).test_admin = data.rows.item(0).test_admin_id;
        delete data.rows.item(0).test_admin_id;
        return data.rows.item(0);
      }   
    });
  }

  sendParticipantUpdate(participant : Participant){
    const participantAsArray = [participant.birthday, participant.last_name, participant.name, participant.school_years, participant.sexo, (participant.online_id || null)]
    return this.database.executeSql(`UPDATE participants SET birthday = ?, last_name = ?, name = ?, school_years = ?, ` +
                                      `sexo = ?, online_id = ? WHERE id_participant = ${participant.id_participant}`, participantAsArray)
      .then(() =>{
        return this.loadDataById(participant.id_participant, 'participants', 'id_participant')
          .then(data => data)
      })
  }

 sendNewParticipant(participant : Participant) {
    const participantAsArray = [participant.birthday, participant.last_name, participant.name, participant.school_years, participant.sexo, (participant.online_id || null)]
    return this.database.executeSql('INSERT INTO participants (birthday, last_name, name, school_years, sexo, online_id) VALUES (?, ?, ?, ?, ?, ?)', participantAsArray)
    .then(() => {
      return this.loadLatest('participants', 'id_participant')
        .then(data => data).catch(e=>console.log(JSON.stringify(e)));
    }).catch(e=>console.log(JSON.stringify(e)));
  }
  
  deleteAnswer(id: Number) {
    return this.database.executeSql('DELETE FROM answers where id_answer = ?', [id]).then(() => {
      return true;
    });
  }

  deleteProtocolAnswers(protocolId: Number) {
    return this.database.executeSql('DELETE FROM answers where id_protocol = ?', [protocolId]).then(() => {
      return true;
    });
  }
 
  sendAnswerUpdate(answer: Answer) {
    return this.database.executeSql(`UPDATE answers SET answer = ?, online_id = ? WHERE id_answer = ${answer.id_answer}`, 
                                      [answer.answer, (answer.online_id || null)])
      .then(() => {
        return this.loadDataById(answer.id_answer, 'answers', 'id_answer')
          .then(data => data);
    })
  }

  /**
   * Function used when downloading from the server. We use the online_id as the parameter
   * and update the id_version to the current id_version on the protocol object in the server
   * because the version is the only thing that can be edited after a protocol has already been
   * created.
   */
  editProtocolLocally(protocol: Protocol){
    return this.database.executeSql(`UPDATE protocols SET id_version = ? WHERE online_id = ${protocol.id_protocol}`
                        ,[protocol.id_version]).then(()=>{
                          return this.loadProtocolByOnlineId(protocol.online_id)
                            .then(data => data)
                      })
  }

  sendProtocolUpdate(protocol: Protocol) {
    const protocolAsArray = [protocol.age, 
                            protocol.case_num, 
                            protocol.group_num,
                            protocol.id_participant,
                            protocol.id_version,
                            protocol.start_date,
                            protocol.test_admin,
                            (protocol.online_id || null)]

    return this.database.executeSql(`UPDATE protocols SET age = ?, case_num = ?, group_num = ?, ` +
                                    `id_participant = ?, id_version = ?, start_date = ?, test_admin_id = ?, online_id = ?` +
                                    `WHERE id_protocol = ${protocol.id_protocol}`, protocolAsArray).then(() => {
    
      return this.loadProtocol(protocol.id_protocol)
        .then(data => data);
    })
  }

  cleanDb(){
    const sql = 'DELETE FROM answers; DELETE FROM protocols; DELETE FROM participants;'
    this.sqlitePorter.importSqlToDb(this.database, sql)
        .then(_ => {
          return true;
        })
  }
}
