import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutofocusModule } from 'angular-autofocus-fix';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { SidebarModule } from 'ng-sidebar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AlertModule } from '../alert/alert.module'

import { PagesComponent } from './pages.component';
import { ProtocolComponent } from './protocol/protocol.component';
import { NewprotocolComponent } from './protocol/newprotocol/newprotocol.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { PagesRoutingModule } from './pages-routing.module';
import { ChooseProtocolComponent } from './choose-protocol/choose-protocol.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { UsersComponent } from './users/users.component';
import { ClickOutsideModule } from 'ng-click-outside';

@NgModule({
  declarations: [
    PagesComponent,
    ProtocolComponent,
    NewprotocolComponent,
    ChooseProtocolComponent,
    UsersComponent
  ],
  imports: [
    CommonModule,
    SidebarModule,
    NgbModule,
    FontAwesomeModule,
    AutofocusModule,
    AccordionModule.forRoot(),
    BsDropdownModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    AlertModule,
    PagesRoutingModule,
    ClickOutsideModule
  ],
  providers :[Network],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
  //bootstrap: [PagesComponent]
})
export class PagesModule {}