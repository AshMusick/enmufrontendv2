import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';

import { AuthGuard } from '../core-services/auth.guard';
import { NewprotocolComponent } from './protocol/newprotocol/newprotocol.component';
import { ChooseProtocolComponent } from './choose-protocol/choose-protocol.component';
import { UsersComponent } from './users/users.component';

const pageRoutes: Routes = [

  { path: '', component: PagesComponent, children: [
    { path: 'subtests/:id', 
    loadChildren: () => import('./subtests/subtests.module').then(m => m.SubtestsModule ) },
    { path: 'report', 
    loadChildren : () => import('./report/report.module').then(m => m.ReportModule) },
    { path: 'newprotocol', component: NewprotocolComponent },
    { path: 'choose-protocol', component: ChooseProtocolComponent },
    { path: 'users', component: UsersComponent },
    { path: '**', redirectTo: 'newprotocol' }
  ], 
  canActivate: [AuthGuard]},
  
] 

@NgModule({
  imports: [
    RouterModule.forChild(pageRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PagesRoutingModule { }
