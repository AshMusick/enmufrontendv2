import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ReportComponent } from './report.component';
import { ReportRoutingModule } from './report-routing.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
  declarations: [
    ReportComponent
  ],
  imports: [
    CommonModule,
    NgxDatatableModule,
    ReportRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReportModule {}
