import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { Report } from './report';
import { ReportService } from './report.service';
import { CsvDataService } from './csv-data.service';
import { ProtocolService } from '../protocol/protocol.service';
import { Protocol, Participant, Group } from '../protocol/protocol';
import { LoginService } from 'src/app/login/login.service';
import { User } from 'src/app/login/login';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  providers: [ReportService,CsvDataService, ProtocolService, LoginService],
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  public report: Report;
  public partial_reports: Report[];
  public reports: Report[];
  private participants : Participant[];
  private protocols : Protocol[];
  private groups : Group[];
  private testAdmins : User[];

  //public rows:Array<any> = [];
  /*public columns:Array<any> = [
    {title: 'Protocol', name: 'protocol', sort: false, filtering: {filterString: '', placeholder: 'Filter by name'}},
    {
      title: 'Reading',
      name: 'reading',
      sort: false,
      filtering: {filterString: '', placeholder: 'Filter by reading'}
    },
    {title: 'WL Trial 1', sort: false, className: ['text-success'], name: 'wl trial 1' },
    {title: 'WL Trial 2', sort: false, name: 'wl trial 2', filtering: {filterString: '', placeholder: 'Filter by wl trial 1'}},
    {title: 'WL Trial 3', sort: false, className: 'text-warning', name: 'wl trial 3'},
    {title: 'WL Trial 123', sort: false, name: 'wl trial 123'}
  ];
  public page:number = 1;
  public itemsPerPage:number = 10;
  public maxSize:number = 5;
  public numPages:number = 1;
  public length:number = 0;

  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-striped', 'table-bordered']
  };*/

  


  constructor(private reportService: ReportService, 
    private route: ActivatedRoute, 
    private router: Router, 
    private csvDataService : CsvDataService,
    private protocolService : ProtocolService,
    private loginService : LoginService
    ) {
    
  }

  ngOnInit() {
    this.getReports();
    
  }

  getReports(): void {
    this.reportService.getReports()
      .subscribe(reports => {
        this.partial_reports = reports;
        this.getProtocolInfo();
      });
  }

  getProtocolInfo(): void {
    this.protocolService.getProtocols()
      .subscribe(protocols => {
        this.protocols = protocols;
        this.getGroups();
        this.getTestAdmins();
      })
  }

  getGroups(){
    this.protocolService.getGroups()
      .subscribe(groups=>{
        this.groups = groups;
      })
  }

  getTestAdmins(){
    this.loginService.getUsers(localStorage.getItem('token'))
     .subscribe(users =>{
       this.testAdmins = users;
       this.getParticipantInfo();
     })
  }

  getParticipantInfo(): void {
    this.protocolService.getParticipants()
    .subscribe(participants => {
      this.participants = participants;
      let pr : Protocol
      let pa : Participant;
      let ta : User;
      let re : Report = {} as Report;
        this.reports = this.partial_reports.map(report => {
          pr = this.protocols.filter(protocol => protocol.id_protocol === report.id_protocol)[0];
          pa = this.participants.filter(participant => participant.id_participant === pr.id_participant)[0];        
          ta = this.testAdmins.filter(testAdmin => testAdmin.id === pr.test_admin)[0];
          re.Test_admin = ta.username;
          re.Test_admin_id = pr.test_admin;
          re.Participant_id = pa.id_participant;
          re.Case= pr.id_protocol + "_" + pr.test_admin;
          re.Group = this.groups.filter(group => group.id_group === pr.group_num)[0].group_name;
          re.Age = pr.age;    
          re.Schooling = pa.school_years;
          re.Educational_level = pr.id_version == 1 ? 'High' : 'Low';
          if(pa.sexo == 1) {
            re.Gender = "male";
          }
          if(pa.sexo == 2) {
            re.Gender = "female";
          }
          const rep : Report = {...re, ...report} as Report;
          return rep;  
        })

    })
  }

  export(): void {
    // download the file using old school javascript method
    this.csvDataService.exportToCsv('report.csv', this.reports)
    // get the data as base64 or json object for json type - this will be helpful in ionic or SSR
    
  }
}
