import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment'
import { HttpErrorHandler, HandleError } from '../../core-services/http-error-handler.service';

import { Report } from './report'
import { User } from 'src/app/login/login';
import { AuthService } from 'src/app/core-services/auth.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'token ' + localStorage.getItem('token')
  })
};



@Injectable()
export class ReportService {

  currentUser : User;
  privilege: boolean;
  reportsUrl: string;

  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler,
    private authService: AuthService) {
    this.setPrivilege(); 
    this.handleError = httpErrorHandler.createHandleError('ReportService');
  }

  setPrivilege(){
    this.authService.currentUser.subscribe(user =>{
      this.privilege = user.is_staff 
      this.reportsUrl = environment.backendServer + `/api/vscores${this.privilege ? '' : 'user'}/`
    })
  }

  /** GET reports from the server */
  getReports (): Observable<Report[]> {
    return this.http.get<Report[]>(this.reportsUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getReports', []))
      );
  }
}
