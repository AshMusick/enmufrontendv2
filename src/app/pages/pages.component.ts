import { Component, OnInit, ElementRef } from '@angular/core';
import { AuthService } from '../core-services/auth.service'
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css'],
  providers: [AuthService]
})

export class PagesComponent implements OnInit {

  _opened: boolean;

  constructor(private authService: AuthService, 
    private elementRef: ElementRef,
    private menu: MenuController){
    this._opened = false;
  }

  ngOnInit() {
    this.toggleSidebar();
  }
  /*ngAfterViewInit(){
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#fff';
  }*/
  

  public loggedIn() {
    if(this.authService.currentUserValue) {
      return true;
    }
  }
   
  public toggleSidebar() {
    if(this.authService.currentUserValue) {
      this._opened = true;
    }
  }

  onClickOutside() {
    this.menu.close();
  }

}
