import { Platform } from '@ionic/angular';
import { Component, OnInit, Injector } from '@angular/core';
import { take, takeUntil } from 'rxjs/operators'
import { Protocol, Participant } from '../protocol/protocol';
import { ProtocolService } from '../protocol/protocol.service';
import { SqliteProtocolService } from '../protocol/sqlite-protocol.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../core-services/auth.service';
import { User } from '../../login/login';
import { Network } from '@ionic-native/network/ngx';
import { SubtestDownloadService } from '../subtests/service/sync/subtest-download.service';
import { AlertService } from '../../alert/alert.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-choose-protocol',
  templateUrl: './choose-protocol.component.html',
  providers: [ ProtocolService, AuthService, SqliteProtocolService],
  styleUrls: ['./choose-protocol.component.css']
})
export class ChooseProtocolComponent implements OnInit {

  participants: Participant[];
  currentParticipant: Participant;
  
  protocols: Protocol[];
  currentProtocol: Protocol;

  currentUser: User;
  protocolService: any;

  offline : boolean = false;
  downloaded : Subject<boolean> = new Subject();

  constructor(private injector: Injector,
    private platform : Platform, 
    private authService: AuthService,
    private router: Router,
    private network : Network,
    alertService : AlertService,
    private subtestDownloadService : SubtestDownloadService) {
    if(platform.is('android') && this.network.type == 'none'){
      this.offline = true;
      this.protocolService = this.injector.get<SqliteProtocolService>(SqliteProtocolService);
    } else {
      this.protocolService = this.injector.get<ProtocolService>(ProtocolService);
    }
      

    if(this.authService.currentUserValue) {
      let username = this.authService.currentUserValue.username;
      let password = this.authService.currentUserValue.password;
      const newUser: User = { username, password } as User;
      this.currentUser = newUser;
    } 
  }

  ngOnInit() {
    this.protocolService.getProtocolDataState().subscribe(rdy =>
      {
        if(rdy){
          this.getProtocols();
        }
      }); 
  }

  getProtocols(): void {
    this.protocolService.getProtocols()
      .subscribe(protocols => {
        (this.protocols = protocols);
        this.getParticipants();
      });
  }

  getParticipants(): void {
    this.protocolService.getParticipants()
      .pipe(take(2))
      .subscribe(participants => {
        (this.participants = participants);
        this.composeProtocol();
      });
  }

  composeProtocol(): void {
    let protocols = [];
    let index = 0;
    this.protocols.forEach(protocol => {
      let id = protocol.id_participant;
      let participant = this.participants.filter(participant => participant.id_participant === id)[0]
      protocol.participant = participant.name + ' ' + participant.last_name;
      protocol.participantObject = participant;
      if(index > 0){
        protocols.push(protocol)
      }
      else{
        protocols[0] = protocol;
        
      }
      index++;
    })

    this.protocols = protocols;
  };

  chooseProtocol(protocol: Protocol){
    this.currentProtocol = protocol;
  }

  getProtocol(): number {
    return this.currentProtocol.id_protocol;
  }

  downloadProtocol(protocol : Protocol, participant : Participant){
    this.subtestDownloadService.downloadProtocol(protocol, participant);
    this.subtestDownloadService.getProto()
    .pipe(takeUntil(this.downloaded))
    .subscribe(res =>{
      if(res.id_protocol){
        this.currentProtocol = res;
        this.downloaded.next(true);
        this.router.navigate(['pages/subtests', this.currentProtocol.id_protocol]);
      }
    })
  }

  canDownload(){
    return !this.offline && this.platform.is('android');
  }
}

  
