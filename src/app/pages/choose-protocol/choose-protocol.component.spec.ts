import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseProtocolComponent } from './choose-protocol.component';

describe('ChooseProtocolComponent', () => {
  let component: ChooseProtocolComponent;
  let fixture: ComponentFixture<ChooseProtocolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseProtocolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseProtocolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
