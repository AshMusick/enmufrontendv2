//import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { SqliteService } from '../../core-services/sqlite.service'

//import { HttpErrorHandler, HandleError } from '../../core-services/http-error-handler.service';
import { Protocol, Participant, Group } from './protocol';

@Injectable({providedIn: 'root'})
export class SqliteProtocolService {

  private protocolsReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
 
  protocols = new BehaviorSubject([]);
  protocol : BehaviorSubject<Protocol> = new BehaviorSubject(null);
  participants = new BehaviorSubject([]);
  participant : BehaviorSubject<Participant> = new BehaviorSubject(null);
  observableGroups = new BehaviorSubject([
    {id_group : 1, group_name : 'Clínico'},
    {id_group : 2, group_name : 'Control'},
    {id_group : 3, group_name : 'Sin grupo'}
  ]);
 
  constructor( /* private plt: Platform, */ private sqliteService : SqliteService ) {
    this.loadProtocolData();
  }
 
  loadProtocolData() { 

    this.sqliteService.getDatabaseState().subscribe(rdy =>{
      if(rdy){
        console.log('about to execute promises')
        this.sqliteService.loadProtocols()
            .then(data => this.protocols.next(data));
        this.sqliteService.loadData('participants')
          .then(data => this.participants.next(data));

        this.protocolsReady.next(true);
      }
    })
    
  }
 
  getProtocolDataState() {
    return this.protocolsReady.asObservable();
  }

  getProtocols(): Observable<Protocol[]> {
    return this.protocols.asObservable();
  }

  getParticipants(): Observable<Participant[]> {
    return this.participants.asObservable();
  }

  getGroups() : Observable<Group[]> {
    return this.observableGroups.asObservable();
  }

  addProtocol(protocol: Protocol) {
    this.protocol.next(protocol);
    this.sqliteService.sendNewProtocol(protocol)
      .then(data => {
        this.protocol.next(data)
      });
    return this.protocol.asObservable();
  }

  addParticipant(participant : Participant) : Observable<Participant> {
    this.participant.next(participant);
    this.sqliteService.sendNewParticipant(participant)
      .then(data =>{
        this.participant.next(data)
      });
    return this.participant.asObservable();
  }

}
