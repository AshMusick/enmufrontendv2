export interface Protocol {
    id_protocol: number;
    id_participant: number;
    test_admin: number;
    id_version: number;
    participant: string;
    start_date: string;
    case_num: number;
    group_num: number;
    age: number;
    online_id : number; //attribute used when using app and local database
    participantObject: Participant; //TODO::refactor to use participant for everything
}

export interface Participant {
    id_participant: number;
    last_name : string;
    name : string;
    sexo : number;
    school_years : number;
    birthday : string;
    online_id : number; //attribute used when using app and local database
}

export interface Group {
    id_group: number;
    group_name: string;
}

export interface Language {
    id_translation: number;
    language: string;   
}

export interface Version {
    id_version: number;
    description: string;
}