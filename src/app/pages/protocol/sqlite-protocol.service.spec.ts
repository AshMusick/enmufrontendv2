import { TestBed } from '@angular/core/testing';

import { SqliteProtocolService } from './sqlite-protocol.service';

describe('SqliteProtocolService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SqliteProtocolService = TestBed.get(SqliteProtocolService);
    expect(service).toBeTruthy();
  });
});
