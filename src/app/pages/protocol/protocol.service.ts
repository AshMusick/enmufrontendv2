import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';


import { HttpErrorHandler, HandleError } from '../../core-services/http-error-handler.service';
import { Protocol, Participant, Group, Language } from './protocol';
import { environment } from '../../../environments/environment'
import { User } from 'src/app/login/login';
import { AuthService } from 'src/app/core-services/auth.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'token ' + localStorage.getItem('token')
  })
};

@Injectable({providedIn:"root"})
export class ProtocolService {

  private protocolsReady: BehaviorSubject<boolean> = new BehaviorSubject(true);
  
  privilege: boolean;

  protocolUrl: string;
  participantUrl = environment.backendServer + '/api/participants/'
  groupUrl = environment.backendServer + '/api/protocol-groups/'
  languageUrl = environment.backendServer + '/api/translations/'
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler,
    private authService: AuthService) {
    this.setPrivilege(); 
    this.handleError = httpErrorHandler.createHandleError('ProtocolService');
  }

  setPrivilege(){
    this.authService.currentUser.subscribe(user =>{
      this.privilege = user.is_staff 
      this.protocolUrl =  environment.backendServer + `/api/protocols${this.privilege ? '' : 'user'}/`;
    })
  }

  getProtocolDataState() {
    return this.protocolsReady.asObservable();
  }

  /** GET protocols from the server */
  getProtocols (): Observable<Protocol[]> {
    return this.http.get<Protocol[]>(this.protocolUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getProtocols', []))
      );
  }

  /** GET protocols from the server */
  getParticipants (): Observable<Participant[]> {
    return this.http.get<Participant[]>(this.participantUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getParticipants', []))
      );
  }

  /** GET protocol groups from the server */
  getGroups (): Observable<Group[]> {
    return this.http.get<Group[]>(this.groupUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getGroups', []))
      );
  }

  /** GET translation options from the server */
  getLanguages (): Observable<Language[]> {
    return of([
      {id_translation: 1, language: "Spanish"},
      {id_translation: 2, language: "English"}
    ])
    /* return this.http.get<Language[]>(this.languageUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getLanguages', []))
      ); */
  }

  /** POST: add a new participant to the database */
  addParticipant (participant: Participant): Observable<Participant> {
    return this.http.post<Participant>(this.participantUrl, participant, httpOptions)
      .pipe(
        catchError(this.handleError('addParticipant', participant))
      );
  }

  /** POST: add a new protocol to the database */
  addProtocol (protocol: Protocol): Observable<Protocol> {
    return this.http.post<Protocol>(this.protocolUrl, protocol, httpOptions)
      .pipe(
        catchError(this.handleError('addProtocol', protocol))
      );
  }
}
