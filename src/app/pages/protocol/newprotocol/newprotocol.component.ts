import { Platform } from '@ionic/angular'
import { Component, OnInit, ElementRef, ViewChild, ViewEncapsulation, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

import { Protocol, Participant, Group, Language, Version } from '../protocol';
import { ProtocolService } from '../protocol.service';
import { SqliteProtocolService } from '../sqlite-protocol.service'
import { Router } from '@angular/router';
import { faCalendar, faToggleOff, faToggleOn } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/core-services/auth.service';
import { User } from 'src/app/login/login';
import { AlertService } from 'src/app/alert/alert.service';
import { versions } from './versions';

@Component({
  selector: 'app-newprotocol',
  templateUrl: './newprotocol.component.html',
  providers: [ FormBuilder ],
  styleUrls: ['./newprotocol.component.css'],
  encapsulation: ViewEncapsulation.None // Use to disable CSS Encapsulation for this component
})
export class NewprotocolComponent implements OnInit {

  faCalendar = faCalendar;
  faToggleOff = faToggleOff;
  faToggleOn = faToggleOn;

  createParticipant: FormGroup;
  createProtocol: FormGroup;
  submitted: boolean;
  loading: boolean;
  group: number;

  currentUser : User;

  participants: Participant[];
  currentParticipant: Participant;
  @ViewChild('myParticipant', {static: false}) myParticipant: ElementRef;
  
  protocols: Protocol[];
  groups: Group[];
  currentProtocol: Protocol;
  languages: Language[];

  askVersion = false;
  version: Version;
  versions: Version[];

  nuevo: boolean = true;
  protocolService : any;

  constructor(private injector: Injector,
    private platform : Platform, 
    private formBuilder: FormBuilder, 
    private router: Router,
    private authService : AuthService,
    private alertService : AlertService) {
      if(platform.is('android')){
        this.protocolService = injector.get<SqliteProtocolService>(SqliteProtocolService); 
      } else 
        this.protocolService = injector.get<ProtocolService>(ProtocolService); 
        
      this.submitted = false;
      this.loading = false;
  }

  ngOnInit() {
    this.getGroups();
    this.getLanguages();
    this.versions = versions;
    this.createParticipant = this.formBuilder.group({
      name: ['', Validators.required],
      last_name: ['', Validators.required],
      birthday: ['', Validators.required],
      school_years: ['', Validators.required],
      sexo: ['', Validators.required]
  });
    this.createProtocol = this.formBuilder.group({
      group_num: ['', Validators.required],
      language: ['', Validators.required]
    });

    this.protocolService.getProtocolDataState().subscribe(rdy => {
      if (rdy) {
      this.getParticipants();
      this.getProtocols();
       } else {console.log('not ready')} 
    });

    this.getUser();
  }

  getUser() : void {
    this.currentUser = this.authService.currentUserValue
  }

  getParticipants(): void {
    this.protocolService.getParticipants()
      .subscribe(participants => {
        (this.participants = participants);
      });
  }

  toggleNuevo(): void {
    this.nuevo = !this.nuevo; 
  }

  getProtocols(): void {
    this.protocolService.getProtocols()
    .subscribe(protocols => {
      this.protocols = protocols;
    })
  }

  getGroups(): void {
    this.protocolService.getGroups()
    .subscribe(groups => {
      this.groups = groups;
    })
  }

  getLanguages(): void {
    this.protocolService.getLanguages()
    .subscribe(languages => {
      this.languages = languages;
    })
  }

  // hack since we cannot define short/long version in reading test for English, let user choose
  languageChanged(): void {
    // 2 represents English
    if(this.ff.language.value == '2') {
      this.createProtocol = this.formBuilder.group({
        group_num: [this.ff.group_num.value, Validators.required],
        language: [this.ff.language.value, Validators.required],
        version: [ this.version ? this.version.id_version : "", Validators.required]
      });
      this.askVersion = true;
    } else {
      // save version in case we want to add it back to the form later
      this.version = this.versions.filter(v => v.id_version == this.ff.version.value)[0];
      this.askVersion = false;
      this.createProtocol = this.formBuilder.group({
        group_num: [this.ff.group_num.value, Validators.required],
        language: [this.ff.language.value, Validators.required]
      });
    }
  }

  registerParticipant() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.createParticipant.invalid) {
        return;
    }

    this.loading = true;

    let name = this.f.name.value;
    let last_name = this.f.last_name.value;
    let birthday =  this.dateParser(this.f.birthday.value);
    let school_years = this.f.school_years.value;
    let sexo = 1; //masculino
    if(this.f.sexo.value == 'femenino') {
      sexo = 2;
    }
    if(this.f.sexo.value == 'otros') {
      sexo = 3;
    }

    const newParticipant: Participant = { name, last_name, birthday, school_years, sexo } as Participant;
    this.protocolService.addParticipant(newParticipant)
    .subscribe(participant =>
      { 
        this.currentParticipant = participant;
        this.participants.push(participant);
        this.alertService.success('Participante fue creado con éxito!')
        this.loading = false;
        this.submitted = false;
      });  
  }

  selectParticipant(participant: Participant){
    this.currentParticipant = participant;
  }

  addNewProtocol(){
    this.submitted = true;
    // stop here if form is invalid
    if (this.createProtocol.invalid) {
      return;
    }
    let id_participant = this.currentParticipant.id_participant;
    let test_admin = this.currentUser.id;
    let today: Date = new Date();
    let birthday: Date = new Date(this.currentParticipant.birthday)
    let age = Math.round((today.valueOf() - birthday.valueOf())/(365 * 3600 * 24 * 1000));
    
    let todayJson = {"year" : today.getFullYear(),
                      "month" : today.getMonth() + 1,
                      "day" : today.getDate() };
    
    if(this.ff.group_num.value != null) {
      this.group = this.ff.group_num.value
    }
    let id_version;
    if(this.askVersion){
      id_version = this.ff.version.value;
    }
    let group_num = this.group;
    let start_date = this.dateParser(todayJson);
    const newProtocol: Protocol = { id_participant, test_admin, start_date, age, group_num, id_version } as Protocol;
    
    this.protocolService.addProtocol(newProtocol)
      .subscribe(protocol => {
        if(protocol.id_protocol){
          this.currentProtocol = protocol;
          this.protocols.push(protocol);
          this.router.navigate(['/pages/subtests', this.currentProtocol.id_protocol])

        }
    }) 
  }

  dateParser(date) {
    let month = new String(date.month);
    month.length == 2 ? month = date.month : month = "0" + date.month;
    let day = new String(date.day);
    day.length == 2 ? day = date.day : day = "0" + date.day;
    return date.year + "-" + month + "-" + day;

  }

  // convenience getter for easy access to form fields
  get f() { return this.createParticipant.controls; }
  get ff() { return this.createProtocol.controls; }
}
