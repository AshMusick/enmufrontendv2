import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NewprotocolComponent }    from './newprotocol.component';
//import { HeroDetailComponent }  from './hero-detail/hero-detail.component';

const newProtocolRoutes: Routes = [
  { path: 'newprotocol', redirectTo: '/newprotocol' },
  { path: 'newprotocol',  component: NewprotocolComponent, outlet: "pagesOutlet" }
];

@NgModule({
  imports: [
    RouterModule.forChild(newProtocolRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class NewProtocolRoutingModuleModule { }
