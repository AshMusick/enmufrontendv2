import { Version } from "../protocol";

export const versions: Version[] = [
    {
        id_version: 4,
        description: "Short"
    },
    {
        id_version: 5,
        description: "Long"
    }
];