import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewprotocolComponent } from './newprotocol.component';

describe('NewprotocolComponent', () => {
  let component: NewprotocolComponent;
  let fixture: ComponentFixture<NewprotocolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewprotocolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewprotocolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
