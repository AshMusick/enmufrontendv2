import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../core-services/auth.service';

@Component({
  selector: 'app-protocol',
  templateUrl: './protocol.component.html',
  providers: [ AuthService ],
  styleUrls: ['./protocol.component.css']
})
export class ProtocolComponent implements OnInit {

  privilege : boolean;

  constructor(private authService: AuthService, private router: Router) {
    this.setPrivilege(); 
  }

  ngOnInit() {
    
  }

  setPrivilege(){
    this.authService.currentUser.subscribe(user =>{
      this.privilege = user.is_staff
    })
  }
  
  salir(){
    this.authService.logout();
    this.router.navigate(['login'])
  }

}
