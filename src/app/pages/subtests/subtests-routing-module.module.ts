import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubtestsComponent }    from './component/subtests.component';
import { Page404SubtestComponent } from './page404-subtest/page404-subtest.component';
import { ScoresComponent } from './scores/scores.component';


const subtestsRoutes: Routes = [
  { path: 'score', component: ScoresComponent },
  { path: '', component: SubtestsComponent }, 
  { path: '**', component: Page404SubtestComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(subtestsRoutes)
  ],
  exports: [
    RouterModule
  ],
})
export class SubtestsRoutingModuleModule { }
