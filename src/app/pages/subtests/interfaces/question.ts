export interface Question {
    id_testItem: number;
    id_question: number;
    description: string;
    correct_answer: string;
    id_answer: number;
    id_item: number;
    id_protocol: number;
    id_attribute: number;
    answer: string;
    correct: boolean;
    order_fix: number;
    value: number;
    false_positive: number;
}

export interface QuestionAttribute {
    id_que_att: number;
    id_question: number;
    id_attribute: number;
    re_order_key: number;
    value: number;
    false_positive: number;
}

export interface Answer {
    id_answer: number;
    id_item: number;
    id_protocol: number;
    id_attribute: number;
    answer: string;
    online_id: number; //attribute only used in app with local db
}