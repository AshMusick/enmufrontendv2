export interface Subtest {
    id_subtest: number;
    description: string;
}

export interface QuestionVersion {
    id_question: number;
    id_version: number;
    id_item: number;
    value: number;
}

