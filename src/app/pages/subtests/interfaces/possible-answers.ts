export class PossibleAnswer {
    answerId: number;
    topic: number;
    answer: string;
}
