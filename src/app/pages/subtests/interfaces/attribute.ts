export interface Attribute {
    id_attribute: number;
    description: string;
    id_subtest: number;
}
