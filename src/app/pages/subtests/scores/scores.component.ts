import { Component, OnInit } from '@angular/core';
import { QuickScore, Score } from './score';
import { ScoresService } from './scores.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Participant, Protocol } from '../../protocol/protocol';
import { SubtestsService } from '../service/subtests.service';


@Component({
  selector: 'app-scores',
  templateUrl: './scores.component.html',
  providers: [ ScoresService, SubtestsService ],
  styleUrls: ['./scores.component.css']
})
export class ScoresComponent implements OnInit {

  details : JSON = null;
  score : QuickScore;
  finished : boolean = false;
  protocolId : number;
  showDetails : boolean = false;
  participant : Participant;
  participantString : String;
  protocol : Protocol;
  participantId : number;

  constructor(private scoresService : ScoresService, private subtestsService : SubtestsService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.protocolId = +params.get('id');
      this.getScores(this.protocolId);
      this.getProtocol(this.protocolId);
 });
  }

  getScores(idProtocol:number) {
    this.scoresService.scorePrep(idProtocol);
    setTimeout(() => 
    {
      this.scoresService.Score()
      .subscribe(score => {
        this.score = score;
        this.finished = true;
        this.getDetails();
      })
    },
    5000);
    
  }

  getParticipant(idParticipant : number) {
    this.subtestsService.getParticipant(idParticipant)
       .subscribe(participant =>{
         this.participant = participant;
         this.participantString = participant.name + ' ' + participant.last_name;
       })
  }

  getProtocol(id: number){
    this.subtestsService.getProtocol(id)
      .subscribe(protocol =>{
        this.protocol = protocol;
        this.participantId = protocol.id_participant;
        this.getParticipant(this.participantId)
      })
  }



  getDetails() {
      this.details = this.scoresService.getDetails()   
  }

  toggleDetails() {
    this.showDetails = !this.showDetails;
  }

  saveScore() {
    this.scoresService.saveScore();
  }

  deleteScore(){
    this.scoresService.deleteScore();
  }

}
