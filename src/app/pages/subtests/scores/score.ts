export interface Score {
    id_score: number;
    id_protocol: number;
    score: number;
    id_component: number;
    deleted: number;
}

export interface MyComponent {
    id: number;
    name: string;
    id_subtest: number;
}

export interface QuickScore {
    score: number;
    percent: number;
    mean: number;
    subtest1: number;
    errors: number;
    seconds: number;
    subtest2: number;
    subtest2Percent: number;
    subtest3I: number;
    subtest3IPercent: number;
    subtest3D: number;
    subtest3DPercent: number;
    subtest4: number;
    subtest4Percent: number;
    subtest5: number;
    subtest5Percent: number;
    subtest6: number;
    subtest6Percent: number;
    subtest7: number;
    subtest7Percent: number;
    subtest8: number;
    subtest8Percent: number;
    subtest9: number;
    subtest9Percent: number;
    subtest10: number;
    subtest10Percent: number;
}