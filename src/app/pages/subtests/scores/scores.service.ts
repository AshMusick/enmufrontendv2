import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject} from 'rxjs';


import { SubtestsService } from '../service/subtests.service'
import { Protocol, Participant, Group } from '../../protocol/protocol';
import { Subtest, QuestionVersion } from '../interfaces/subtest';
import { Attribute } from '../interfaces/attribute'
import { QuestionAttribute, Question, Answer } from '../interfaces/question';
import { MyComponent, Score, QuickScore } from './score';
import { isNullOrUndefined } from 'util';
import { AlertService } from 'src/app/alert/alert.service';
import { ProtocolService } from '../../protocol/protocol.service';
import { LoginService } from 'src/app/login/login.service';


@Injectable()
export class ScoresService {

  constructor(private subtestsService:SubtestsService, private alertService: AlertService, private protocolService: ProtocolService, private usersService : LoginService) { }

  protocol: Protocol;
  participant: Participant;
  subtests: Subtest[];
  attributes: Attribute[];
  questionAttributes: QuestionAttribute[];
  questions: Question[];
  testItems: Question[];
  FULLTestItems: Question[];
  protocolId: number;
  answers: Answer[];
  questionVersions: QuestionVersion[];
  components: MyComponent[];
  allScores: Score[];
  scores: Score[];
  group: Group;
  testAdmin : string;

  quickScore : QuickScore = {} as QuickScore;
  private currentScore: BehaviorSubject<QuickScore>;
  public currentQuickScore: Observable<QuickScore>;
  
  details: JSON;

  seeDetails: boolean = false;

  scoredProtocols: number[];
  
  getDetails() {
    return this.details;
  }
 
   getProtocol(id: number){
     this.subtestsService.getProtocol(id)
       .subscribe(protocol =>{
         this.protocol = protocol;
         this.getQuestionVersions();
         this.getParticipant(this.protocol.id_participant);
         this.getGroup(protocol.group_num);
         this.getTestAdmin(protocol.test_admin);
       })
   }
 
   getParticipant(id: number){
     this.subtestsService.getParticipant(id)
       .subscribe(participant =>{
         this.participant = participant;
       })
   }

   getGroup(id: number){
     this.protocolService.getGroups()
      .subscribe(groups =>{
        this.group = groups.filter(group => group.id_group === id)[0];
      })
   }

   getTestAdmin(id_admin : number){
     this.usersService.getUser(localStorage.getItem('token'), id_admin)
      .subscribe(user =>{
        this.testAdmin = user.first_name + ' ' + user.last_name;
      })
   }
 
   getSubtests(protocolId:number): void {
     this.protocolId = protocolId;
     this.subtestsService.getSubtests()
       .subscribe(subtests => {
         (this.subtests = subtests);
         this.getAttributes();
       });
   }
 
   getAttributes(): void {
     this.subtestsService.getAttributes()
       .subscribe(attributes => {
         (this.attributes = attributes);
         this.getQuestionAttributes();
       });
   }
 
   getQuestionAttributes(): void {
     this.subtestsService.getQuestionAttributes()
       .subscribe(questionAttributes => {
         (this.questionAttributes = questionAttributes)
       this.getQuestions();
       });
   }
 
   getQuestions(): void {
     this.subtestsService.getQuestions()
       .subscribe(questions => {
         (this.questions = questions);
         this.getAnswers(this.protocolId);
       });
   }
 
   getAnswers(protocol : number): void {
     this.subtestsService.getAnswers()
       .subscribe(answers => {
         this.answers = answers.filter(answer => answer.id_protocol === protocol);
         this.getTestItems();
       });
   }
 
   getQuestionVersions(): void {
     this.subtestsService.getQuestionVersions()
       .subscribe(questionVersions => {
           this.questionVersions = questionVersions;
       });
   }
 
   getComponents(): void {
     this.subtestsService.getComponents()
       .subscribe(components => {
           this.components = components;
       });
   }

   getScores(): void {
    this.subtestsService.getScores()
      .subscribe(scores => {
        this.allScores = scores;
        this.scores = scores.filter(score => score.id_protocol === this.protocolId);
      });
  }

  getTestItems(): void {
    let testItems = [];
    
    this.questionAttributes.forEach(record => {
      let id_testItem = record.id_que_att;
      let id_question = record.id_question;
      let order_fix = record.re_order_key;
      let question = this.questions.filter(question => question.id_question === id_question)[0];
      let value = record.value;
      let description = question.description;
      let correct_answer = question.correct_answer;
      let id_attribute = record.id_attribute;
      let answer = '';
      let correct = false;
      if(correct_answer === '0') {
        answer = '0';
        correct = true;
      } 
      const newQuestion: Question = { id_testItem, id_question, description, correct_answer, id_attribute, answer, correct, order_fix, value} as Question;
      
      if(testItems.length === 0){
        testItems[0] = newQuestion;
      }
      else{
        testItems.push(newQuestion);
      }
    });
    this.testItems = testItems;
  
  
    this.answers.forEach(answer => { 
      let question = this.testItems.filter(question => question.id_testItem === answer.id_item)[0];
          if(question) { 
            question.answer = answer.answer 
            question.answer === question.correct_answer ? question.correct = true : question.correct = false;
            question.id_item = answer.id_item;
            question.id_answer = answer.id_answer;
            question.id_protocol = answer.id_protocol;
          };
    });
    this.FULLTestItems = this.testItems;
    if(this.protocol.id_version) {
      this.setVersionQuestions();
    }
  }

  setVersionQuestions(){
    let testItems = [];
    let questions = [];
    let questionVersions = [];
    
    questionVersions = this.questionVersions.filter(record => record.id_version === this.protocol.id_version);
    questionVersions.forEach(record => {
      let id_question = record.id_question;
      let question = this.questions.filter(question => question.id_question === id_question)[0];
      let testItemGroup = this.FULLTestItems.filter(testItem => testItem.id_question === id_question);
      if(questions.length === 0){
        
        questions[0] = question;
        let i = 0;
        
        testItemGroup.forEach(item => {
          if(i === 0){
            testItems[i] = item;
          }
          else{
            testItems.push(item);
          }
          i++;
        })
        
      }
      else{
        questions.push(question);
        testItemGroup.forEach(item => {
          testItems.push(item);
        })
      } 
    });
    this.questions = questions;
    this.testItems = testItems;  
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
  
  scorePrep(protocolId: number) {
    this.getSubtests(protocolId);
    this.getProtocol(protocolId);
    this.getComponents();
    this.getScores();
  }

  // TODO:: major refactor
  Score() : Observable<QuickScore> {
    let maxScore = 0;
    let percent = 0;
    let subScore1 = 0;
    let subScore2 = 0;
    let wltrial1 =0;
    let wltrial2 =0;
    let wltrial3 =0;
    let subPercent2 = 0;
    let maxScore2 = 0;
    let subScore3I = 0;
    let subPercent3I = 0;
    let maxScore3I = 0;
    let subScore3D = 0;
    let subPercent3D = 0;
    let maxScore3D = 0;
    let subScore4 = 0;
    let subPercent4 = 0;
    let maxScore4 = 0;
    let arrowsPart1 = 0;
    let arrowsPart2 = 0;
    let subScore5 = 0;
    let purchasedItems = 0;
    let purchasedItemsScore = 0;
    let purchasedDuplicateItems = 0;
    let purchasedItemsOmitted = 0;
    let blocksTravelled = 0;
    let blocksScore = 0;
    let iei = 0;
    let ieiScore = 0;
    let savedCoins = 0;
    let exceededCoins = 0;	
    let subPercent5 = 0;
    let maxScore5 = 0;
    let subScore6 = 0;
    let pointsLines1 = 0;
    let pointsLines2 = 0;
    let pointsLines3 = 0;
    let pointsLines4 = 0;
    let subPercent6 = 0;
    let maxScore6 = 0;
    let subScore7 = 0;
    let personageSpontaneous = 0;
    let personageCues = 0;
    let subPercent7 = 0;
    let maxScore7 = 0;
    let subtest7mem = {}; // we need a memory mechanism because points can only be given for spontaneous or cued, but not both
    let subScore8 = 0;
    let subPercent8 = 0;
    let maxScore8 = 0;
    let subScore9 = 0;
    let maxScore9 = 60; // fixed max
    let subPercent9 = 0;
    let subScore10 = 0;
    let subPercent10 = 0;
    let maxScore10 = 0;
    let truePositives = 0;
    let unrelatedFalsePositives = 0;
    let relatedFalsePositives = 0;
    let ptIAcierto = 0;
    let flowerHitsImmed = 0;
    let leafHitsImmed = 0;
    let handHitsImmed = 0;
    let buildingHitsImmed = 0;
    let ptDAcierto = 0;
    let leafHitsDelayed = 0;
    let buildingHitsDelayed = 0;
    let errors = 0;
    let seconds = 0;
    let sumOfPercents = 0;
    this.testItems.forEach(question => {
      switch(question.id_attribute){
        case 1:
        case 2: //reading(subtest 1)
            if(question.description === '¿Cuántos errores?')
              errors = parseInt(question.answer) || 0; 
            if(question.description === '¿Cuántos segundos?')
              seconds = parseInt(question.answer) || 0;
          break;
        case 3:
        case 4:
        case 5: //word memory(subtest 2)
          maxScore += question.value;
          maxScore2 += question.value;
          if(question.correct){
            switch(question.id_attribute){
              case 3:
                wltrial1+= question.value;
                break;
              case 4:
                wltrial2+= question.value;
                break;
              case 5:
                wltrial3+= question.value;
                break;
            }
            subScore2+= question.value;
          }  
          break;
        case 6: //delayed trial (subtest 8)
          maxScore+= question.value;
          maxScore8+= question.value;
          if(question.correct){
            subScore8+= question.value;
          }  
        break;
        case 7: 
        case 17: //visual memory (subtest 3)
          if(question.correct_answer === '1') {
            maxScore3I+= question.value;
            if (question.answer === '1'){
              ptIAcierto+= question.value;
              if(this.protocol.id_version < 3) { // Spanish protocol... id version 1 or 2
                switch(question.description.charAt(0)){
                  case 'F': 
                    flowerHitsImmed+= question.value;
                    break;
                  case 'E':
                    buildingHitsImmed+= question.value;
                    break;
                  case 'M':
                    handHitsImmed+= question.value;
                    break;
                  case 'H':
                    leafHitsImmed+= question.value;
                    break;
                }
            } else { // English protocol
              switch(question.description.charAt(0)){
                case 'F': 
                  flowerHitsImmed+= question.value;
                  break;
                case 'B':
                  buildingHitsImmed+= question.value;
                  break;
                case 'H':
                  handHitsImmed+= question.value;
                  break;
                case 'L':
                  leafHitsImmed+= question.value;
                  break;
              }
            }
            }
          }
          break;
        case 8:
        case 18: 
          if(question.correct_answer === '1') {
            maxScore3D+= question.value;
            if (question.answer === '1') {
              ptDAcierto+= question.value;
              if(this.protocol.id_version < 3) { // Spanish protocol... id version 1 or 2
                switch(question.description.charAt(0)){
                  case 'E':
                    buildingHitsDelayed+= question.value;
                    break;
                  case 'H':
                    leafHitsDelayed+= question.value;
                    break;
                }
              } else { // English protocol
                switch(question.description.charAt(0)){
                  case 'B':
                    buildingHitsDelayed+= question.value;
                    break;
                  case 'L':
                    leafHitsDelayed+= question.value;
                    break;
                }
              }
            }  
          }
          break;
        case 9:
          maxScore = maxScore + parseFloat(question.correct_answer);
          maxScore4 = maxScore4 + parseFloat(question.correct_answer);
          if(!isNullOrUndefined(question.answer)&& question.answer != ''){
            let pointsToAdd = this.calculatePointsUptoMatchAndSubtractPointsOverMatch(
              parseFloat(question.correct_answer), parseFloat(question.answer)
              );
            subScore4+= pointsToAdd;
            arrowsPart1+= pointsToAdd;
          }; 
          break; 
        case 10: //flechas (subtest 4)
          maxScore4 = maxScore4 + parseFloat(question.correct_answer);
          if(!isNullOrUndefined(question.answer)&& question.answer != ''){
            let pointsToAdd = this.calculatePointsUptoMatchAndSubtractPointsOverMatch(
              parseFloat(question.correct_answer), parseFloat(question.answer)
              );
            subScore4+= pointsToAdd;
            arrowsPart2+= pointsToAdd;
          };
          break;
        case 11: //subtest 5
          if(this.protocol.id_version < 3){ // id 1 & 2 is Spanish version
            switch(question.description) {
              case 'Cantidad de elementos comprados':
                if(!isNullOrUndefined(question.answer)&& question.answer != ''){
                  purchasedItems = parseInt(question.answer);
                  switch(purchasedItems) {
                    case 1:
                      subScore5+= 2;
                      purchasedItemsScore += 2;
                      break;
                    case 2:
                      subScore5+= 7;
                      purchasedItemsScore += 7;
                      break;
                    case 3:
                      subScore5+= 16;
                      purchasedItemsScore += 16;
                      break;
                    case 4:
                      subScore5+= 29;
                      purchasedItemsScore += 29;
                      break;
                    case 5:
                      subScore5+= 48;
                      purchasedItemsScore += 48;
                      break;
                    case 6:
                      subScore5+= 77;
                      purchasedItemsScore += 77;
                      break;
                    default:  
                      break;
                  }
                } 
                maxScore = maxScore + 77;
                maxScore5 = maxScore5 + 77;
                break;
              case 'Cantidad de cuadras recorridas':

                let points = 0;
                let answer;
                
                if(!isNullOrUndefined(question.answer)&& question.answer != ''){
                  answer = parseInt(question.answer);
                    if(answer > 33){
                      points = 0;
                    }
                    else if(answer > 30 && answer <= 33) { 
                      points = 12
                    }
                    else if(answer > 27 && answer <= 30) { 
                      points = 36
                    }
                    else if(answer > 24 && answer <= 27) { 
                      points = 44
                    }
                    else if(answer > 21 && answer <= 24) { 
                      points = 52
                    }
                    else if((answer > 18 && answer <= 21) || (answer > 8 && answer <= 10)) { 
                      points = 60
                    }
                    else if((answer > 15 && answer <= 18) || (answer > 10 && answer <= 12)) { 
                      points = 68
                    }
                    else if(answer > 12 && answer <= 15) { 
                      points = 76
                    }
                    blocksTravelled = answer;
                    blocksScore = points;
                    subScore5 = subScore5 + points;
                }
                maxScore5 = maxScore5 + 76;
                maxScore = maxScore + 76;
                break;
              case 'Entradas Incorrectas': //Indice de entradas correctas es 10 - entradas incorrectas
                iei = !isNullOrUndefined(question.answer)&& question.answer != '' ? parseInt(question.answer) : 0;
                ieiScore = iei <= 10 ? 10 - iei : 0;
                subScore5+= ieiScore;
                maxScore5+= 10;
                maxScore+= 10;
                break;
              case 'Cantidad de elementos comprados más que una vez':
                purchasedDuplicateItems = !isNullOrUndefined(question.answer)&& question.answer != '' ? parseInt(question.answer) : 0;
                break;  
              case 'Cantidad de elementos omitidos':
                purchasedItemsOmitted = !isNullOrUndefined(question.answer)&& question.answer != '' ? parseInt(question.answer) : 0;
                break;
              case 'Cantidad de monedas ahorradas':
                savedCoins = !isNullOrUndefined(question.answer)&& question.answer != '' ? parseInt(question.answer) : 0;
                break;
              case 'Cantidad de monedas excedidas':
                exceededCoins = !isNullOrUndefined(question.answer)&& question.answer != '' ? parseInt(question.answer) : 0;
                break;
              default:
                break;
            }
          } else { // English version
            switch(question.description) {
              case 'Number of items purchased':
                if(!isNullOrUndefined(question.answer)&& question.answer != ''){
                  purchasedItems = parseInt(question.answer);
                  switch(purchasedItems) {
                    case 1:
                      subScore5+= 2;
                      purchasedItemsScore += 2;
                      break;
                    case 2:
                      subScore5+= 7;
                      purchasedItemsScore += 7;
                      break;
                    case 3:
                      subScore5+= 16;
                      purchasedItemsScore += 16;
                      break;
                    case 4:
                      subScore5+= 29;
                      purchasedItemsScore += 29;
                      break;
                    case 5:
                      subScore5+= 48;
                      purchasedItemsScore += 48;
                      break;
                    case 6:
                      subScore5+= 77;
                      purchasedItemsScore += 77;
                      break;
                    default:  
                      break;
                  }
                } 
                maxScore = maxScore + 77;
                maxScore5 = maxScore5 + 77;
                break;
              case 'Number of blocks traveled':
  
                let points = 0;
                let answer;
                
                if(!isNullOrUndefined(question.answer)&& question.answer != ''){
                  answer = parseInt(question.answer);
                    if(answer > 33){
                      points = 0;
                    }
                    else if(answer > 30 && answer <= 33) { 
                      points = 12
                    }
                    else if(answer > 27 && answer <= 30) { 
                      points = 36
                    }
                    else if(answer > 24 && answer <= 27) { 
                      points = 44
                    }
                    else if(answer > 21 && answer <= 24) { 
                      points = 52
                    }
                    else if((answer > 18 && answer <= 21) || (answer > 8 && answer <= 10)) { 
                      points = 60
                    }
                    else if((answer > 15 && answer <= 18) || (answer > 10 && answer <= 12)) { 
                      points = 68
                    }
                    else if(answer > 12 && answer <= 15) { 
                      points = 76
                    }
                    blocksTravelled = answer;
                    blocksScore = points;
                    subScore5 = subScore5 + points;
                }
                maxScore5 = maxScore5 + 76;
                maxScore = maxScore + 76;
                break;
              case 'Number of incorrect entries': //Indice de entradas correctas es 10 - entradas incorrectas
                iei = !isNullOrUndefined(question.answer)&& question.answer != '' ? parseInt(question.answer) : 0;
                ieiScore = iei <= 10 ? 10 - iei : 0;
                subScore5+= ieiScore;
                maxScore5+= 10;
                maxScore+= 10;
                break;
              case 'Number of items purchased more than once':
                purchasedDuplicateItems = !isNullOrUndefined(question.answer)&& question.answer != '' ? parseInt(question.answer) : 0;
                break;  
              case 'Number of items omitted':
                purchasedItemsOmitted = !isNullOrUndefined(question.answer)&& question.answer != '' ? parseInt(question.answer) : 0;
                break;
              case 'Number of coins saved':
                savedCoins = !isNullOrUndefined(question.answer)&& question.answer != '' ? parseInt(question.answer) : 0;
                break;
              case 'Number of coins exceeded':
                exceededCoins = !isNullOrUndefined(question.answer)&& question.answer != '' ? parseInt(question.answer) : 0;
                break;
              default:
                break;
            }
          }
          break;
        case 12: //puntos y lineas (subtest 6)
          maxScore6 = maxScore6 + 
           (['1','2'].includes(question.description.charAt(7)) ?
                parseFloat(question.correct_answer)
                : parseFloat(question.correct_answer) * 3);
          if(!isNullOrUndefined(question.answer)&& question.answer != ''){
            switch(question.description.charAt(7)) {
              case '1':
                pointsLines1 = parseFloat(question.answer);
                subScore6 += parseFloat(question.answer);
                break;
              case '2':
                pointsLines2 = parseFloat(question.answer);
                subScore6 += parseFloat(question.answer);
                break;
              case '3':
                pointsLines3 = parseFloat(question.answer) * 3;
                subScore6 += (parseFloat(question.answer) * 3);
                break;
              case '4':
                pointsLines4 = parseFloat(question.answer) * 3;
                subScore6 += (parseFloat(question.answer) * 3);
                break;
            }
          };
          break;
        case 13:
            maxScore7  = this.updateMaxScore7(maxScore7, question, subtest7mem);
            if(question.correct) {
              subScore7+= question.value;
              personageSpontaneous+= question.value;
            }
            break;
        case 14: //personaje (subtest 7)
          maxScore7  = this.updateMaxScore7(maxScore7, question, subtest7mem);
          if(question.correct) {
            subScore7+= question.value;
            personageCues+= question.value;
          }
          break; 
        case 15: //subtest 11 animales
          if(question.answer) {
            subScore9++;
          };  
          break; 
        
        case 16: //subtest 9 reconocimiento
          if(question.correct_answer !== '0') {
            maxScore++;
            maxScore10++;
            if(question.correct) {
              subScore10++;
              truePositives++;
            };
          }
          else{
            if(!question.correct){
              subScore10--;
              if(question.description.charAt(question.description.length - 1) === '*') {
                relatedFalsePositives++;
              }
              else{
                unrelatedFalsePositives++;
              }
            }
          }
          
            
          break; 
        default:
          break;
      }
    });
    subScore3I = ptIAcierto;
    subScore3D = ptDAcierto;
    // Currently score is composed of: 
    // word trials 1,2,3 & delayed + visual memory immediate & delayed + party + personage + animals
    this.quickScore.score = subScore2 + subScore8 + subScore3I + subScore3D + subScore5 + subScore7 + subScore9;
    maxScore = maxScore2 + maxScore8 + maxScore3I + maxScore3D + maxScore5 + maxScore7 + maxScore9;
    percent = 100 * this.quickScore.score / maxScore;
    this.quickScore.percent = Math.round(percent * 100)/100; // TODO:: confirm that this is how percentage should be calculated

    this.quickScore.errors = errors;
    this.quickScore.seconds = seconds;
    if (this.quickScore.seconds > 0) {
      subScore1 = (60 * (215 - errors))/seconds;
      this.quickScore.subtest1 = Math.round(subScore1 * 100)/100;
    }
    else 
      this.quickScore.subtest1 = 0;
    this.quickScore.subtest2 = subScore2;
    subPercent2 = 100 * subScore2/maxScore2;
    this.quickScore.subtest2Percent = Math.round(subPercent2*100)/100;
    this.quickScore.subtest3I = Math.round(subScore3I * 100)/100;
    subPercent3I = 100 * subScore3I/maxScore3I;
    this.quickScore.subtest3IPercent = Math.round(subPercent3I*100)/100;
    this.quickScore.subtest3D = Math.round(subScore3D * 100)/100;
    subPercent3D = 100 * subScore3D/maxScore3D;
    this.quickScore.subtest3DPercent = Math.round(subPercent3D*100)/100;
    this.quickScore.subtest4 = subScore4;
    subPercent4 = 100 * subScore4/maxScore4;
    this.quickScore.subtest4Percent = Math.round(subPercent4*100)/100;
    this.quickScore.subtest5 = subScore5;
    subPercent5 = 100 * subScore5/maxScore5;
    this.quickScore.subtest5Percent = Math.round(subPercent5*100)/100;
    this.quickScore.subtest6 = subScore6;
    subPercent6 = 100 * subScore6/maxScore6;
    this.quickScore.subtest6Percent = Math.round(subPercent6*100)/100;
    this.quickScore.subtest7 = subScore7;
    subPercent7 = 100 * subScore7/maxScore7;
    this.quickScore.subtest7Percent = Math.round(subPercent7*100)/100;
    this.quickScore.subtest8 = subScore8;
    subPercent8 = 100 * subScore8/maxScore8;
    this.quickScore.subtest8Percent = Math.round(subPercent8*100)/100;
    this.quickScore.subtest9 = subScore9;
    subPercent9 = 100 * subScore9/60;
    this.quickScore.subtest9Percent = Math.round(subPercent9*100)/100;
    this.quickScore.subtest10 = subScore10;
    subPercent10 = 100 * subScore10/maxScore10;
    this.quickScore.subtest10Percent = Math.round(subPercent10*100)/100;
    sumOfPercents = this.quickScore.subtest2Percent + this.quickScore.subtest3DPercent + this.quickScore.subtest3IPercent + this.quickScore.subtest4Percent
    + this.quickScore.subtest5Percent + this.quickScore.subtest6Percent + this.quickScore.subtest7Percent + this.quickScore.subtest8Percent + this.quickScore.subtest9Percent;
    this.quickScore.mean = Math.round(sumOfPercents * 100 / 7)/100;
    let gender = 'masculino'
    if(this.participant.sexo == 2) {
      gender = 'femenino'
    }
    if(this.participant.sexo == 3) {
      gender = 'otros';
    }
    let obj: any =
    {
    "Test Admin" : this.testAdmin, "Test Admin ID" : this.protocol.test_admin, "Participant ID" : this.participant.id_participant, "Case": this.protocol.test_admin + '_' + this.protocol.id_protocol,	
    "Age": this.protocol.age,	"Group": this.group.group_name,	"Gender": gender, "Schooling" :	this.participant.school_years, "Educational level": "PENDIENTE DEFINIR",	"Reading Test":	subScore1, "WL Trial 1 score": wltrial1,	
    "WL Trial 2 score": wltrial2,	"WL Trial 3 score": wltrial3,	"WL TRIALS 1-3 SCORE": subScore2,	"% WL TRIALS 1-3": this.quickScore.subtest2Percent,	"DELAYED TRIAL SCORE": this.quickScore.subtest8,
    "% DELAYED TRIAL": this.quickScore.subtest8Percent,	"True Positives": truePositives,	"Related false positives": relatedFalsePositives,	"Unrelated false Positives": unrelatedFalsePositives,	"Total False Positives": relatedFalsePositives + unrelatedFalsePositives,	
    "DISCRIMINATION INDEX": subScore10, "% DISCRIMINATION INDEX": this.quickScore.subtest10Percent,	"Flower Hits Immed.": flowerHitsImmed,	"Leaf Hits Immed.":leafHitsImmed,	"Hand Hits Immed.":handHitsImmed,	"Building Hits Immed.":buildingHitsImmed,
    "TOTAL VISUAL MEM. IMMED.":this.quickScore.subtest3I,	"% VISUAL MEM. IMMED.":this.quickScore.subtest3IPercent,	"Leaf Hits Delayed":leafHitsDelayed,	"Building Hits Delayed":buildingHitsDelayed,
    "TOTAL VISUAL MEM. DELAYED":this.quickScore.subtest3D, "% VISUAL MEM. DELAYED":this.quickScore.subtest3DPercent,	
    "Arrows Part 1": arrowsPart1,	"Arrows Part 2": arrowsPart2, "TOTAL ARROWS" : this.quickScore.subtest4,	"% ARROWS" : this.quickScore.subtest4Percent,	
    "Purchased Items": purchasedItems,	"Purchased items score": purchasedItemsScore,	
    "Blocks travelled": blocksTravelled,	"Blocks score": blocksScore, 	"IEI": iei, "IEI score": ieiScore,	
    "Purchased items duplicates": purchasedDuplicateItems, "Purchased items omissions": purchasedItemsOmitted, "Saved coins": savedCoins,	"Exceeded Coins": exceededCoins, 
    "TOTAL PARTY":this.quickScore.subtest5,	"% PARTY": this.quickScore.subtest5Percent,	"Points & Lines 1":pointsLines1,	"Points & Lines 2":pointsLines2,
    "Points & Lines 3":pointsLines3,	"Points & Lines 4":pointsLines4, "TOTAL POINTS & LINES": this.quickScore.subtest6, "% POINTS & LINES": this.quickScore.subtest6Percent,
    "Personage spontaneous":personageSpontaneous,	"Personage cues": personageCues, "TOTAL PERSONAGE":this.quickScore.subtest7,	"% PERSONAGE": this.quickScore.subtest7Percent,	
    "TOTAL ANIMALS": this.quickScore.subtest9, "% ANIMALS":this.quickScore.subtest9Percent,	
    "TOTAL SCORE/SUM": this.quickScore.score, "TOTAL SCORE/PERCENT": this.quickScore.percent, "TOTAL SCORE/MEAN": this.quickScore.mean
    }
    this.details = obj;
    this.currentScore = new BehaviorSubject<QuickScore>(this.quickScore);
    this.currentQuickScore = this.currentScore;
    return this.currentScore.asObservable();  
  }

  updateMaxScore7(highestPossibleScore, question, mem){
    // For spontaneous, we will start by adding up values for possible right answers
    if(question.id_attribute == 13){
      highestPossibleScore+= question.value;
      mem[question.id] = question.value;
    // For cues, if cued value is higher than the spontaneous one, we should subtract the spontaneous value and add the cued value instead
    } else if (mem[question.id] < question.value){
      highestPossibleScore-= mem[question.id];
      highestPossibleScore+= question.value;
    }

    return highestPossibleScore;
  }

  saveScore(){
    if(this.scores.length > 0) {
      this.updateScores();
    }
    else{
      this.addScores();
    }
    this.getScores();
  }

  addScores(){
    this.components.forEach((component) => {
      let id_protocol = this.protocolId;
      let score = typeof(this.details[component.name]) == "number" ?  this.details[component.name] : null;
      let id_component = component.id;

      const newScore: Score = { id_protocol, score, id_component } as Score;

      this.subtestsService
      .addScore(newScore)
      .subscribe(score => {
        this.scores.push(score)});
        this.alertService.success("Se añadió el protocolo al reporte con éxito!")
    });
  }

    updateScores(){
      this.scores.forEach((score) => {
        let changingScore = score;
        let component = this.components.filter(component => component.id === score.id_component)[0];
        changingScore.score = typeof(this.details[component.name]) == "number" ?  this.details[component.name] : null;
        changingScore.deleted = null;
        this.subtestsService
          .updateScore(changingScore)
          .subscribe(score => {
          // replace the score in the scores list with update from server
          const ix = score ? this.scores.findIndex(s => s.id_score === score.id_score) : -1;
          if (ix > -1) {
            this.scores[ix] = score;
          }
          this.alertService.success("Se actualizó el protocolo en el reporte con éxito!")
        });
      });
    }

    deleteScore(){
      this.scores.forEach((score) => {
        let changingScore = score;
        let component = this.components.filter(component => component.id === score.id_component)[0];
        changingScore.score = typeof(this.details[component.name]) == "number" ?  this.details[component.name] : null;
        changingScore.deleted = 1;
  
        this.subtestsService
          .updateScore(changingScore)
          .subscribe(score => {
          // replace the score in the scores list with update from server
          const ix = score ? this.scores.findIndex(s => s.id_score === score.id_score) : -1;
          if (ix > -1) {
            this.scores[ix] = score;
          }
          this.alertService.error("Se sacó el protocolo del reporte con éxito!")
        });
      });
    }

    // Helper functions
    calculatePointsUptoMatchAndSubtractPointsOverMatch(match : number, answer : number) : number {
      return answer <= match
      ? answer // you get all the points from your answer if you didn't go over
      : match * 2 - answer; // points over correct answer have negative value
    }
}


