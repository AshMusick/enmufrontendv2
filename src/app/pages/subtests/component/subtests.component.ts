import { Platform } from '@ionic/angular';
import { Component, ElementRef, ViewChild, OnInit, Injector, ViewEncapsulation } from '@angular/core';
import {debounceTime, distinctUntilChanged, map, switchMap, takeUntil} from 'rxjs/operators';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

import { Subtest, QuestionVersion } from '../interfaces/subtest';
import { Attribute } from '../interfaces/attribute';
import { Question, QuestionAttribute, Answer } from '../interfaces/question';
import { SubtestsService } from '../service/subtests.service';
import { SqliteSubtestsService } from '../service/sync/sqlite-subtests.service'
import { Protocol, Participant } from '../../protocol/protocol'
import {Observable, Subject} from 'rxjs';
import { PossibleAnswer } from '../interfaces/possible-answers';
import { AlertService } from '../../../alert/alert.service'
import { isNullOrUndefined } from 'util';

import { faCircle, faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { SubtestUploadService } from '../service/sync/subtest-upload.service';
import { translations } from './translations';


@Component({
  selector: 'app-subtests',
  templateUrl: './subtests.component.html',
  styleUrls: ['./subtests.component.css'],
  encapsulation : ViewEncapsulation.None
})

export class SubtestsComponent implements OnInit {

  @ViewChild('myAnswer', {static: false}) myAnswer: ElementRef;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.simpleAnswerBank.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )
  private endSubsciption = new Subject();
  result: {} = {};
  arrowsTotal : number;
  subtests: Subtest[];
  currentSubtest: Subtest;
  subtestIndex: number;
  
  faCircle = faCircle;
  faCheckCircle = faCheckCircle;

  attributes: Attribute[];
  subtestAttributes: Attribute[];
  attribute: Attribute;

  questions: Question[];
  testItems: Question[];
  FULLTestItems : Question[];
  attributeQuestions: Question[][];
  question: Question;
  selectQuestion: Question;

  questionAttributes: QuestionAttribute[];
  questionIdList: QuestionAttribute[];
  match: QuestionAttribute;

  questionVersion: QuestionVersion;
  questionVersions: QuestionVersion[];

  answers: Answer[];
  answer: Answer;

  answerOption: PossibleAnswer;
  answerBank: PossibleAnswer[];
  simpleAnswerBank = [];

  protocolId: number;
  protocol: Protocol;
  participant: Participant;

  tabEnter: boolean;
  
  errors: number;
  seconds: number;
  
  subtestsService : any;
  editing = false;

  language = "Spanish";
  translations: any;

  constructor(private platform : Platform, 
    private injector: Injector, 
    private route: ActivatedRoute,
    private router: Router, 
    private alertService: AlertService,
    private subtestUploadService : SubtestUploadService) {
    
    this.platform.backButton.subscribeWithPriority(10, () => {
        this.router.navigate(['choose-protocol']);
    });

    if(platform.is('android')){
      this.subtestsService = this.injector.get<SqliteSubtestsService>(SqliteSubtestsService); 
    } else 
      this.subtestsService = this.injector.get<SubtestsService>(SubtestsService); 
    
    this.subtestIndex = 0;
    this.tabEnter = false;
  }

  ngOnInit() {
    this.translations = translations;
    this.subtestsService.getSubtestsDataState()
    .pipe(takeUntil(this.endSubsciption))
    .subscribe(rdy => {
      if(rdy){
        this.route.paramMap
          .pipe(takeUntil(this.endSubsciption))
          .subscribe((params: ParamMap) => {
            this.protocolId = +params.get('id');
            this.getProtocol(this.protocolId);
          });
      }
    });        
  }
  
  ngOnDestroy(){
    this.endSubsciption.next(true);
  }

  score(){
    if(this.platform.is('android')){
      console.log('1) score function called')
      if(this.protocol.online_id){
        console.log('2) protocol has online id')
        this.subtestUploadService.editProtocol(this.participant.online_id, this.protocol, this.answers)
          .subscribe(finished => {
            console.log('7) finished, about to clean')
            if(finished){
            this.subtestUploadService.cleanLocal()
            this.router.navigate(['score', this.protocol.id_protocol])
          }});
      }else 
        console.log('2) ERROR missing online id')
        this.subtestUploadService.uploadProtocol(this.participant, this.protocol, this.answers)
          .subscribe(finished => {if(finished){
            this.subtestUploadService.cleanLocal()
            this.router.navigate(['score', this.protocol.id_protocol])
          }});
    } else {
      this.router.navigate(['score', this.protocol.id_protocol])
    }

  }

  getProtocol(id: number){
    this.subtestsService.getProtocol(id)
      .pipe(takeUntil(this.endSubsciption))
      .subscribe(protocol =>{
        if(protocol.id_protocol){
          this.protocol = protocol;
          this.getSubtests();
          this.getAnswerBank();
          this.getQuestionVersions();
          this.getParticipant(this.protocol.id_participant);
        }
      })
  }

  getParticipant(id: number){
    this.subtestsService.getParticipant(id)
    .pipe(takeUntil(this.endSubsciption))  
    .subscribe(participant =>{
        this.participant = participant;
      })
  }

  getSubtests(): void {
    this.subtestsService.getSubtests()
    .pipe(takeUntil(this.endSubsciption))  
    .subscribe(subtests => {
        this.subtests = subtests;
        this.getAttributes();
      });
  }

  getAttributes(): void {
    this.subtestsService.getAttributes()
    .pipe(takeUntil(this.endSubsciption))  
    .subscribe(attributes => {
        (this.attributes = attributes);
        this.translationStuffIfNeeded();
        this.getQuestionAttributes();
      });
  }

  translationStuffIfNeeded(): void {
    if([4,5].includes(this.protocol.id_version)){
      // hack until reading test has been designed for English version
      this.subtests.shift();
      this.subtestIndex++;
      this.tellUserAboutVersionInEnglish();
      // set up ui translations
      this.language = "English"
      this.translateSubtests(this.language);
      this.translateAttributes(this.language)
    }
  }

  getQuestionAttributes(): void {
    this.subtestsService.getQuestionAttributes()
    .pipe(takeUntil(this.endSubsciption))  
    .subscribe(questionAttributes => {
        (this.questionAttributes = questionAttributes)
      this.getQuestions();
      });
  }

  getQuestions(): void {
    this.subtestsService.getQuestions()
    .pipe(takeUntil(this.endSubsciption))  
    .subscribe(questions => {
        (this.questions = questions);
        this.getAnswers(this.protocolId);
      });
  }

  getAnswers(protocol : number): void {
    this.subtestsService.getAnswers()
    .pipe(takeUntil(this.endSubsciption))  
    .subscribe(answers => {
        this.answers = answers.filter(answer => answer.id_protocol === protocol);
        this.getTestItems();
        this.showSubtest(this.subtestIndex);
      });
  }

  getQuestionVersions(): void {
    this.subtestsService.getQuestionVersions()
    .pipe(takeUntil(this.endSubsciption))  
    .subscribe(questionVersions => {
          this.questionVersions = questionVersions;
      });
  }

  getAnswerBank(): void {
    this.subtestsService.getPossibleAnswers()
    .pipe(takeUntil(this.endSubsciption))  
    .subscribe(answerOptions => {
        (this.answerBank = answerOptions)
      let index = 0;
      this.answerBank.forEach(object => {
        if(index === 0){
          this.simpleAnswerBank[0] = object.answer
        }
        else{
          this.simpleAnswerBank.push(object.answer)
        }
         index++ 
        });
      });
  }

  getTestItems(): void {
    let testItems : Question[] = [];
    this.questionAttributes.forEach(record => {
      let id_testItem = record.id_que_att;
      let id_question = record.id_question;
      let order_fix = record.re_order_key;
      let question = this.questions.filter(question => question.id_question === id_question)[0];
      let description = question.description;
      let correct_answer = question.correct_answer;
      let id_attribute = record.id_attribute;
      let answer = '';
      let correct = false;
      if(correct_answer == '0') {
        answer = '0';
        correct = true;
      } 
      const newQuestion: Question = { id_testItem, id_question, description, correct_answer, id_attribute, answer, correct, order_fix} as Question;
      testItems.push(newQuestion);
        
    });

    
    this.testItems = testItems;
  
  
    this.answers.forEach(answer => {
      let question = this.testItems.filter(question => question.id_testItem === answer.id_item)[0];
          if(question) { 
            question.answer = answer.answer 
            question.answer === question.correct_answer ? question.correct = true : question.correct = false;
            question.id_item = answer.id_item;
            question.id_answer = answer.id_answer;
            question.id_protocol = answer.id_protocol;
          };
    });
    this.FULLTestItems = this.testItems;
    if(this.protocol.id_version || false) {
      this.setVersionQuestions();
    }
  }

  validateVersion() {
    let errorQuestion = this.testItems.filter(question => question.id_attribute === 1)[0];
    let secondsQuestion = this.testItems.filter(question => question.id_attribute === 2)[0];
    if (errorQuestion.answer == ""|| secondsQuestion.answer == "") {
      return false;
    }
    else {
      let errors = parseInt(errorQuestion.answer); 
      let seconds = parseInt(secondsQuestion.answer);
      let version = this.pickVersion(errors, seconds);
      if(version == this.protocol.id_version) {
        return true;
      } else {
        this.setVersion(version);
        return true;
      }
    } 
  }

  setVersion(version: number) {
    let protocol = this.protocol;
    protocol.id_version = version;
    this.subtestsService.updateProtocol(protocol)
    .pipe(takeUntil(this.endSubsciption))  
    .subscribe(proto =>{
        this.protocol = proto;
        if(this.protocol.id_version != null) {
          this.setVersionQuestions();
        }
      });
    
  }

  pickVersion(errors: number, seconds: number){
    let subScore1;
    this.errors = errors;
    this.seconds = seconds;
    subScore1 = (60 * (215 - errors))/seconds;

    if (subScore1 > 95) {
      return 2
    }
    else {
      return 1
    }
  }

  setVersionQuestions(){
    let testItems: Question[] = [];
    let questions = [];
    let questionVersions = [];
    questionVersions = this.questionVersions.filter(record => record.id_version === this.protocol.id_version);
    questionVersions.forEach(record => {
      let id_question = record.id_question;
      let question = this.questions.filter(question => question.id_question === id_question)[0];
      let testItemGroup = this.FULLTestItems.filter(testItem => testItem.id_question === id_question);
      
      if(questions.length === 0){
        
        questions[0] = question;
        let i = 0;
        
        testItemGroup.forEach(item => {
          if(i === 0){
            testItems[i] = item;
          }
          else{
            testItems.push(item);
          }
          i++;
        })
        
      }
      else{
        questions.push(question);
        testItemGroup.forEach(item => {
          testItems.push(item);
        })
      } 
    });
    this.questions = questions;
    this.testItems = testItems; 
  }

  tellUserAboutVersionInEnglish(): void {
    let version = "short"
    if(this.protocol.id_version === 5) {
      version = "long"
    }
    this.alertService.success('Starting ' + version + ' version');
  }

  showNextSubtest(): void {
      if(this.subtestIndex === 1){
        if(this.validateVersion()){
          this.getSubtests();
          let version = "corta"
          if(this.protocol.id_version === 2) {
            version = "larga"
          }
          this.alertService.success('Se inicia la version ' + version );
        }
        else {
          this.alertService.error('Se debe completar esta sección para poder seguir!');
        }
      }
      else {
        this.showSubtest(this.subtestIndex);
      }
    }
  
  showPreviousSubtest(): void {
      this.showSubtest(this.subtestIndex - 2);
    }

  getSubtestIndexBaseline(): number {
    return ([4,5].includes(this.protocol.id_version)) ? 2 : 1;
  }

  // used for all but spanish
  translateSubtests(language): void {
    this.subtests.map(subtest => {
      subtest.description = this.translations[language][subtest.description];
    })
  }

  // used for all but spanish
  translateAttributes(language): void {
    this.attributes.map(attribute => {
      attribute.description = this.translations[language][attribute.description] || attribute.description;
    })
  }
  showSubtest(index: number) {
    index < (this.subtests.length + this.getSubtestIndexBaseline() - 1) && index > 0 ? this.subtestIndex = index + 1 : this.subtestIndex = this.getSubtestIndexBaseline();
    this.currentSubtest = this.subtests.filter(subtest => subtest.id_subtest === this.subtestIndex)[0];
    this.showAttributes(this.subtestIndex); 
  }

  showAttributes(params): void {
    this.subtestAttributes = this.attributes.filter(attribute => attribute.id_subtest === params);
    let questionList : Question[] = [];
    let listOflists : Question[][] = [[]];
    let index = 0;

    this.subtestAttributes.forEach(attribute => {
      questionList = this.showQuestions(attribute.id_attribute);
    
      if(index > 0){
        listOflists.push(questionList);
      }
      else {
        listOflists[0] = questionList;
      }
      index++;
    });
    this.attributeQuestions = listOflists;
  }

  setFocus(){
    this.myAnswer.nativeElement.value = this.selectQuestion.answer;
    this.myAnswer.nativeElement.focus();
  }
  
  showQuestions(id_attribute : number): Question[] { 
    
    let questions = this.testItems.filter(q => q.id_attribute === id_attribute);

    function isReordered(q : Question) {
      return !isNullOrUndefined(q.order_fix);
    }
    
    if(questions.every(isReordered)) {
      questions = questions.sort(function(a, b){
        return a.order_fix - b.order_fix;
      })
    }

    return questions; 
  }

  nextQuestion() {
    let id :number;
    let question;
    let qTry;

    if(isNullOrUndefined(this.selectQuestion.order_fix)) {
      id = this.selectQuestion.id_testItem + 1;
      this.attributeQuestions.forEach((array) =>{
        qTry = array.filter(question => question.id_testItem === id )[0];
        if(qTry) {
          question = qTry;
        }
      }) 
    }
    else{
      id = this.selectQuestion.order_fix + 1;
      this.attributeQuestions.forEach((array) =>{
        qTry = array.filter(question => question.order_fix === id )[0];
        if(qTry) {
          question = qTry;
        }
      })
    }  
    
    if (question == null) {
      question = this.attributeQuestions[0][0];
    }
    
    this.SelectQuestion(question);
    this.setFocus();
    return; 
    
  }

  add(question: Question, answer: string, id_protocol: number, id_attribute: number): Answer {
    if(!answer && question.correct_answer != '0'){
      if(this.tabEnter) { 
        this.nextQuestion(); 
        this.tabEnter = false;
      }

      return;
    }

    this.answer = undefined;
    answer = (answer || '').trim();
    this.question = question;
    this.question.answer = answer;
    this.question.answer === this.question.correct_answer ? this.question.correct = true : this.question.correct = false;
    let id_item = question.id_testItem;
    

    const newAnswer: Answer = { id_item, id_protocol, answer, id_attribute } as Answer;
    this.answer = newAnswer;
    this.subtestsService
      .addAnswer(newAnswer)
      .pipe(takeUntil(this.endSubsciption))
      .subscribe(answer_ => {
        this.answers.push(answer_)
      });

      if(id_attribute === 15) {
        this.addOption(answer);
      }
      if(this.tabEnter) { this.nextQuestion(); }
    return newAnswer;
  }
  

  addOption(answer: string): PossibleAnswer {
    if(!answer){
      return;
    }
    let ExistingAnswer = this.simpleAnswerBank.filter(option => option === answer)[0];
    if(!!ExistingAnswer){
      return;
    }
    let topic = 1;
    answer = answer.trim();

    const newAnswer: PossibleAnswer = { topic, answer } as PossibleAnswer;
    this.subtestsService
      .addPossibleAnswer(newAnswer)
      .pipe(takeUntil(this.endSubsciption))
      .subscribe(answer => {
        this.simpleAnswerBank.push(answer.answer)});
    return newAnswer;
  }

  SelectQuestion(question: Question) {
    if(this.selectQuestion ? this.selectQuestion.id_testItem === question.id_testItem : false){
      if(!this.editing){
        this.editing = true;
        this.selectQuestion.answer = '';
        if(question.correct_answer){
          this.selectQuestion.correct = false;
        }
      }
    } else {
      this.editing = false;
      this.selectQuestion = question;
      if(question.correct_answer && !question.answer){ 
        this.selectQuestion.answer = question.correct_answer;
        this.selectQuestion.correct = true;
      }
      if(question.description === 'Cantidad de monedas excedidas'){
        if(this.testItems.filter(question => question.description === 'Cantidad de monedas ahorradas')[0].answer != '0'){
          question.answer = '0';
          this.alertService.success('El campo se llenó de manera automática. Monedas excedidas son zero si hubo monedas ahorradas.')
        }
      }
    }
  }

  toggleAndSave(question: Question){
    if(question.correct_answer == '0') {
      this.toggleZeroQuestion(question);
    }
    else {
      this.toggleQuestion(question);
    }
    this.process(question.answer, question);
  }

  toggleZeroQuestion(question: Question) {
    question.answer == '0' ? question.answer = '1' : question.answer = '0';
    question.correct = question.answer == '0';
  }

  toggleQuestion(question: Question){
    if(question.answer !== question.correct_answer){
      this.AnswerRight(question);
    }
    else {
      this.AnswerWrong(question);
    }
    this.process(question.answer, question); 
  }

  simpleToggle(question: Question, detail){
    if((detail.checked && question.correct_answer != '0') || (!detail.checked && question.correct_answer == '0')){
      this.AnswerRight(question);
      this.process(question.answer, question);
    } else if(question.answer === question.correct_answer || question.correct_answer == '0') {
      this.AnswerWrong(question);
      this.process(question.answer, question);
    }
  }

  checked(question : Question): boolean { 
    let checked = true;
    if(question.correct_answer == '0'){
      if(question.correct){
        checked = false;
      }
    }
    else {
      if(!question.correct){
        checked = false;
      }
    }
    //(question.answer == '0' && question.correct) || (question.answer !=  '0' && !question.correct) ? yes = false : yes = true;
    return checked;
  };

  AnswerRight(question) {
    question.correct = true;
    question.answer = question.correct_answer;
  }

  AnswerWrong(question) {
    question.correct = false;
    question.answer = null;
  }
  
  process(response: string, question = this.selectQuestion) {
    let answer = this.answers.filter(answer => answer.id_item === question.id_testItem)[0] || false;
    if(!!answer) {
      this.update(question);
    } else {
      this.add(question, response, this.protocolId, question.id_attribute);
    }
  }

  tabDown(event: KeyboardEvent) {
    event.preventDefault();
  }

  tabUp(event: KeyboardEvent, response: string) {
    event.preventDefault();
    this.tabEnter = true;
    this.process(response);
  }

  enter(answer: string){
    this.tabEnter = true;
    this.process(answer);
  }

  update(question = this.selectQuestion) {
    if (question) {
      let changingAnswer = this.answers.filter(answer => answer.id_item === question.id_testItem)[0];
      changingAnswer.answer = question.answer;
      question.answer === question.correct_answer ? question.correct = true : question.correct = false; 
      this.subtestsService
        .updateAnswer(changingAnswer)
        .pipe(takeUntil(this.endSubsciption))
        .subscribe(answer => {
        // replace the answer in the answers list with update from server
        const ix = answer ? this.answers.findIndex(q => q.id_answer === answer.id_answer) : -1;
        if (ix > -1) {
          this.answers[ix] = answer;
        }
      });
    }
    if(this.tabEnter){
      this.nextQuestion();
      this.tabEnter = false;
    }
  }
  
}