export const translations: any = {
    English : {
        Anterior : "Last",
        ElegirSubtest : "Choose Subtest",
        Finalizar : "Finish",
        Siguiente : "Next",
        "Subtest Aprendizaje de Palabras" : '“WORD LEARNING” SUBTEST',
        "Subtest de Memoria Visual" : '“VISUAL MEMORY” SUBTEST',
        "Subtest Contar Flechas" : '“COUNTING ARROWS” SUBTEST',
        "Subtest de la Fiesta" : '“PARTY SUBTEST”',
        "Subtest \"Puntos y Líneas\"" : '“DOTS AND LINES” SUBTEST',
        "Subtest Memoria del Personaje" : '“PERSONAGE MEMORY” SUBTEST',
        "Aprendizaje de Palabras (Recuerdo Diferido)" : '“WORD LEARNING” SUBTEST (Delayed recall)',
        "Subtest del Reconocimiento" : '“RECOGNITION” SUBTEST',
        "Subtest de Memoria Visual (cont.)" : '“VISUAL MEMORY” SUBTEST (cont.)',
        "Subtest de Animales" : 'ANIMALS SUBTEST',
        "Recuerdo Inmediato": "Inmediate Recall",
        "Recuerdo Inmediato (cont)": "Inmediate Recall (cont)",
        "Parte 1": "Part 1",
        "Parte 2": "Part 2",
        "Recuerdo Espontáneo": "Free Recall",
        "Recuerdo con Claves": "Recognition",
        "Recuerdo Diferido de La Memoria de Palabras":"Delayed Recall of Word Learning",
        "Reconocimiento": "Recognition",
        "Recuerdo Diferido": "Delayed Recall",
        "Recuerdo Diferido (cont)": "Delayed Recall (cont)"
    },
    Spanish : {
        Anterior : "Anterior",
        ElegirSubtest : "Elegir Subtest",
        Finalizar : "Finalizar",
        Siguiente : "Siguiente"
    }
}