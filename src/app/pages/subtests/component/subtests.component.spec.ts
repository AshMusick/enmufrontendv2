import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubtestsComponent } from './subtests.component';

describe('SubtestsComponent', () => {
  let component: SubtestsComponent;
  let fixture: ComponentFixture<SubtestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubtestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubtestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
