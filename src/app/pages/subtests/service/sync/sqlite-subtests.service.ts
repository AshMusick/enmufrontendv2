//import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { take, tap, takeUntil } from 'rxjs/operators'

import { Subtest, QuestionVersion } from '../../interfaces/subtest';
import { Attribute } from '../../interfaces/attribute';
import { Question, QuestionAttribute, Answer } from '../../interfaces/question';
import { MyComponent, Score } from '../../scores/score';
import { PossibleAnswer } from '../../interfaces/possible-answers';
//import { HttpErrorHandler, HandleError } from '../../core-services/http-error-handler.service';
import { Protocol, Participant } from '../../../protocol/protocol';
import { SqliteService } from 'src/app/core-services/sqlite.service';

@Injectable({providedIn : 'root'})
export class SqliteSubtestsService {

  private subtestsReady: Subject<boolean> = new Subject();
 
  subtests = new BehaviorSubject([]);
  possibleAnswers = new BehaviorSubject([]);
  possibleAnswer: Subject<PossibleAnswer> = new Subject();
  attributes = new BehaviorSubject([]);
  questions = new BehaviorSubject([]);
  questionAttributes = new BehaviorSubject([]);
  answers = new BehaviorSubject([]);
  answer: Subject<Answer> = new Subject(); 
  questionVersions = new BehaviorSubject([]);
  protocol: Subject<Protocol> = new Subject();
  components = new BehaviorSubject([]);
  scores = new BehaviorSubject([]);
  participant: Subject<Participant> = new Subject();
 
  constructor(/*private plt: Platform,*/ private sqliteService : SqliteService ) {
    this.loadSubtestData();
  }

  loadSubtestData(){
    this.sqliteService.getDatabaseState()
    .pipe(takeUntil(this.subtestsReady))
    .subscribe(rdy=>{ 
      if(rdy){
        let elementLoadCount = new BehaviorSubject(0);
        this.sqliteService.loadData('subtests')
          .then(data =>{
            this.subtests.next(data)
            elementLoadCount.next(elementLoadCount.getValue() + 1);
            });
        this.sqliteService.loadPossibleAnswers()
          .then(data =>{
            this.possibleAnswers.next(data);
            elementLoadCount.next(elementLoadCount.getValue() + 1);
          });
        this.sqliteService.loadData('attributes')
          .then(data => {
            this.attributes.next(data);
            elementLoadCount.next(elementLoadCount.getValue() + 1);
          });
        this.sqliteService.loadData('questions')
          .then(data => {
            this.questions.next(data);
            elementLoadCount.next(elementLoadCount.getValue() + 1);
          });
        this.sqliteService.loadData('questions_x_attributes')
          .then(data =>{
            this.questionAttributes.next(data);
            elementLoadCount.next(elementLoadCount.getValue() + 1);
          });
        this.sqliteService.loadData('answers')
          .then(data =>{
            this.answers.next(data);
            elementLoadCount.next(elementLoadCount.getValue() + 1);
          });
        this.sqliteService.loadData('questions_x_versions')
          .then(data =>{
            this.questionVersions.next(data)
            elementLoadCount.next(elementLoadCount.getValue() + 1);
          });
        elementLoadCount
        .subscribe(amount =>{
          if(amount == 7){
            this.subtestsReady.next(true);
          }
        })
      }
    }) 
  }
  
 
  getSubtestsDataState() {
    return this.subtestsReady.asObservable();
  }
 
  getSubtests(): Observable<Subtest[]> {
    return this.subtests.asObservable();
  }

  getPossibleAnswers(): Observable<PossibleAnswer[]> {
    return this.possibleAnswers.asObservable();
  }

  getAttributes(): Observable<Attribute[]> {
    return this.attributes.asObservable();
  }

  getQuestions(): Observable<Question[]> {
    return this.questions.asObservable();
  }
  
  getQuestionAttributes(): Observable<QuestionAttribute[]> {
    return this.questionAttributes.asObservable();
  }

  getAnswers(): Observable<Answer[]> {
    return this.answers.asObservable();
  }

  getQuestionVersions(): Observable<QuestionVersion[]> {
    return this.questionVersions.asObservable();
  }

  getParticipant(id : number): Observable<Participant> {
    this.sqliteService.loadDataById(id, 'participants', 'id_participant')
      .then(data =>(this.participant.next(data)));
    return this.participant.asObservable();
  }

  getProtocol(id : number): Observable<Protocol> {
    this.sqliteService.loadProtocol(id)
      .then(data =>{this.protocol.next(data)});
    return this.protocol.asObservable();
  }
  
  getComponents(): Observable<MyComponent[]> {
    this.sqliteService.loadData('components')
      .then(data =>(this.components.next(data)));
    return this.components.asObservable();
  }
  
  getScores(): Observable<Score[]> {
    this.sqliteService.loadData('scores')
      .then(data =>(this.scores.next(data)));
    return this.scores.asObservable();
  }

  addAnswer(answer : Answer) {
      this.sqliteService.sendNewAnswer(answer)
        .then(data =>(this.answer.next(data)));
      return this.answer.asObservable();
  }

  addPossibleAnswer(possibleAnswer : PossibleAnswer) {
    this.sqliteService.sendNewPossibleAnswer(possibleAnswer)
      .then(data =>(this.possibleAnswer.next(data)));
    return this.possibleAnswer.asObservable();
  }

  deleteAnswer(id: Number) {
    return this.sqliteService.deleteAnswer(id).then(() => {
      this.sqliteService.loadData('answers')
        .then(data =>(this.answers.next(data)));
    });
  }

  updateAnswer(answer: Answer) {
    this.sqliteService.sendAnswerUpdate(answer)
      .then(data => this.answer.next(data));
    return this.answer.asObservable();
  }

  updateProtocol(protocol : Protocol){
    this.sqliteService.sendProtocolUpdate(protocol)
      .then(data => this.protocol.next(data));
    return this.protocol.asObservable();
  }

}
