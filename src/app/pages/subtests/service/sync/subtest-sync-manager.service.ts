import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SqliteService } from '../../../../core-services/sqlite.service';
import { Subject, BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SqliteSubtestsService } from './sqlite-subtests.service';
import { SqliteProtocolService } from '../../../protocol/sqlite-protocol.service';
import { SubtestUploadService } from './subtest-upload.service';
import { Network } from '@ionic-native/network/ngx';

@Injectable({
  providedIn: 'root'
})
export class SubtestSyncManagerService {
  
  operationStatus = new Subject();
  protocolCount = new BehaviorSubject(-1);

  sqliteService : SqliteService;
  sqliteSubtestsService : SqliteSubtestsService;
  sqliteProtocolService : SqliteProtocolService;
  subtestUploadServicee : SubtestUploadService;
  

  constructor(private network : Network, plt : Platform) {
    plt.ready().then(rdy =>{
      if(rdy && plt.is('android')){
        this.sync();
      }
    })  
  }

  sync(){
    this.network.onConnect()
      .pipe(takeUntil(this.operationStatus))
      .subscribe(() => {
        console.log('network connected!');
      // We just got a connection but we need to wait briefly
        setTimeout(() => {
          if (this.network.type === 'wifi') {
            this.syncProtocols();
          }
        }, 3000);
    });
  }

  /**
   * Sync function uploads or updates protocols in the server and is called 
   * upon connecting to the internet.
   */
  syncProtocols(){
    this.sqliteProtocolService.getProtocols()
      .pipe(takeUntil(this.operationStatus))
      .subscribe(protocols =>{
        this.protocolCount.next(protocols.length)
        this.sqliteSubtestsService.getAnswers()
            .pipe(takeUntil(this.operationStatus))
            .subscribe(answers =>{
              if(answers.length > 0){
                protocols.forEach(protocol=>{
                  this.sqliteSubtestsService.getParticipant(protocol.id_participant)
                  .pipe(takeUntil(this.operationStatus))
                  .subscribe(participant=>{
                    let answerGroup = answers.filter(answer => answer.id_protocol === protocol.id_protocol);
                    if(protocol.online_id){
                      this.subtestUploadServicee.editProtocol(participant.online_id, protocol, answerGroup)
                        .pipe(takeUntil(this.operationStatus))
                        .subscribe(status =>{
                          if(status){
                            this.protocolCount.next(this.protocolCount.value - 1)
                          }
                        })
                    } else
                      this.subtestUploadServicee.uploadProtocol(participant, protocol, answerGroup)
                        .pipe(takeUntil(this.operationStatus))
                        .subscribe(status =>{
                          if(status){
                            this.protocolCount.next(this.protocolCount.value - 1)
                          }
                        })
                  })
                })
              }
          })
      })

      this.protocolCount
        .pipe(takeUntil(this.operationStatus))
        .subscribe(count => {
          if(count == 0){
            console.log('everything synced')
            this.operationStatus.next('sync finalizado con éxito');
          }
        })
  }
}
