import { TestBed } from '@angular/core/testing';

import { SqliteSubtestsService } from './sqlite-subtests.service';

describe('SqliteSubtestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SqliteSubtestsService = TestBed.get(SqliteSubtestsService);
    expect(service).toBeTruthy();
  });
});
