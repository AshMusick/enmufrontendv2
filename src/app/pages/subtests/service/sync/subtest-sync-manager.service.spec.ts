import { TestBed } from '@angular/core/testing';

import { SubtestSyncManagerService } from './subtest-sync-manager.service';

describe('SubtestSyncManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubtestSyncManagerService = TestBed.get(SubtestSyncManagerService);
    expect(service).toBeTruthy();
  });
});
