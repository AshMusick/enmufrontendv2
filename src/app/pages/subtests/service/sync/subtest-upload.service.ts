import { Injectable } from '@angular/core';
import { SubtestsService } from '../subtests.service';
import { SqliteService } from '../../../../core-services/sqlite.service';
import { Participant, Protocol } from '../../../protocol/protocol';
import { Answer } from '../../interfaces/question';
import { ProtocolService } from '../../../protocol/protocol.service';
import { Subject, BehaviorSubject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubtestUploadService {
  operationStatus = new Subject();
  operationCount = new BehaviorSubject(0);
  onlineParticipantId = new BehaviorSubject(0);
  onlineProtocolId = new BehaviorSubject(0);

  constructor(private subtestsService : SubtestsService, 
              private protocolService : ProtocolService, 
              private sqliteService : SqliteService) { 
    
  }

  
  /**2 main functions. One for a new protocol. The other to edit an existing one.
   * Both return an observable that will say 'Se subió el protocolo con éxito' upon
   * finishing. TODO:: Handle errors
   */
  uploadProtocol(participant : Participant, protocol : Protocol, answers : Answer[]){
    this.putParticipant(participant);
    this.putProtocol(protocol);
    this.putAnswers(answers);
    return this.operationStatus.asObservable();
  }

  editProtocol(onlineParticipantId : number, protocol : Protocol, answers : Answer[]){
    console.log('3) entering edit protocol function')
    this.updateProtocol(protocol, onlineParticipantId);
    this.updateAnswers(answers, protocol.online_id);
    this.onlineProtocolId.next(protocol.online_id);
    return this.operationStatus.asObservable();
  }

  /**Function that clears all answers, protocols and participants from local db */
  cleanLocal(){
    this.sqliteService.cleanDb()
  }



/*    **    **     **    **   **   **     **   **    **   **   **   **   **   **   **   **   **   **   **   **/

  /*Send participant to the server without id. 
  Use Id assigned by server to update locally stored participant with it's 'online_id'*/
  putParticipant(participant : Participant) {
    const localId = participant.id_participant
    delete participant.id_participant;
    this.protocolService.addParticipant(participant)
      .pipe(takeUntil(this.operationStatus))
      .subscribe(_participant => {
        if(_participant.id_participant) {
          _participant.online_id = _participant.id_participant;
          _participant.id_participant = localId;
          this.sqliteService.sendParticipantUpdate(_participant)
            .then(() => this.onlineParticipantId.next(_participant.online_id))
        }
      })
  }

  /*Send protocol to the server without id. 
  Use Id assigned by server to update locally stored protocol with it's 'online_id'
  It should be stored online with the online participant id as well*/
  putProtocol(protocol : Protocol){
    const localId = protocol.id_protocol;
    const localParticipant = protocol.id_participant;
    delete protocol.id_protocol;
    this.onlineParticipantId
    .pipe(takeUntil(this.operationStatus))
    .subscribe(id =>{
      if(id != 0){
        protocol.id_participant = id;
        this.protocolService.addProtocol(protocol)
          .pipe(takeUntil(this.operationStatus))
          .subscribe(_protocol => {
            if(_protocol.id_protocol){
              _protocol.online_id = _protocol.id_protocol;
              _protocol.id_protocol = localId;
              _protocol.id_participant = localParticipant;
              this.sqliteService.sendProtocolUpdate(_protocol)
                .then(() => this.onlineProtocolId.next(_protocol.online_id))
        }
      })
      }
    })
  }

  /*Send answers to the server without id. 
  Use Id assigned by server to update locally stored answers with their 'online_id'
  They should be stored online with the online protocol Id as well*/
  putAnswers(answers : Answer[]){
    let answerCount = new BehaviorSubject(0);
    this.onlineProtocolId
      .pipe(takeUntil(this.operationStatus))
      .subscribe(id =>{
      if(id != 0){
        answers.forEach(answer =>{
          const localProtocol = answer.id_protocol;
          const localId = answer.id_answer;
          delete answer.id_answer;
          answer.id_protocol = id;
          this.subtestsService.addAnswer(answer)
            .pipe(takeUntil(this.operationStatus))
            .subscribe(_answer => {
              if(_answer.id_answer){
                _answer.online_id = _answer.id_answer;
                _answer.id_answer = localId;
                _answer.id_protocol = localProtocol;
                this.sqliteService.sendAnswerUpdate(_answer)
                  .then(() => answerCount.next(answerCount.value + 1));
              }
            })
        })
      }  
    })
    
    answerCount
      .pipe(takeUntil(this.operationStatus))
      .subscribe(count => {
      if(count == answers.length){
        this.operationCount
          .pipe(takeUntil(this.operationStatus))
          .subscribe(amount =>{
          if(amount == 1) {
            this.operationStatus.next(`Se subió el protocolo con éxito`);
            this.operationStatus.unsubscribe();
          }  
        })
              
      }
    })
  }

  /* Update protocol that already exists online. We must do this with its online id.
  and online participant id. */
  updateProtocol(protocol: Protocol, onlineParticipant : number){
    protocol.id_protocol = protocol.online_id;
    protocol.id_participant = onlineParticipant;
    console.log(`4) updating protocol`)
    this.subtestsService.updateProtocol(protocol)
      .pipe(takeUntil(this.operationStatus))
      .subscribe(_protocol =>{
        if(_protocol.id_protocol){
          console.log('5) protocol updated')
          this.operationCount.next(this.operationCount.value + 1);
        }
      }), e => console.log(JSON.stringify(e))

  }

  /*Update answers that already exist online. Then add the answers that are not online.
  We will remove answers from their array as we update them so that we can insert the ones that
  remain.*/
  updateAnswers(answers : Answer[], onlineProtocol : number){
    console.log('4b) going to update answers')
    let answerCount = new BehaviorSubject(-1);
    this.subtestsService.getProtocolAnswers(onlineProtocol)
      .pipe(takeUntil(this.operationStatus))
      .subscribe(protAnswers =>{
        console.log('5b) brought the answers')
        answerCount.next(protAnswers.length)
        if(protAnswers.length > 0){
          protAnswers.forEach(answer =>{
            let ans = answers.filter(_ans => _ans.online_id == answer.id_answer)[0];
            if(ans){
              ans.id_answer = ans.online_id;
              ans.id_protocol = onlineProtocol;
              delete ans.online_id;
              this.subtestsService.updateAnswer(ans)
                .pipe(takeUntil(this.operationStatus))
                .subscribe(response => {
                  if(response.id_answer){
                    //Dear diary:
                    //9000 y algo de respuestas que no estaban online???? (borrar bbdd y intentar de nuevo)
                    //upload AND download are concerns
                    //also, why doesn't the score come up? and what are the errors that appear when I click 'finalizar'?
                    //later...what if we need to update locally rather than full download?
                    answers = answers.filter(_ans => _ans.id_answer != answer.id_answer);
                    answerCount.next(answerCount.value - 1);
                  }
                })
            } else {
              answerCount.next(answerCount.value - 1);
            }
                
          })
        } 
      })
      answerCount
        .pipe(takeUntil(this.operationStatus))
        .subscribe(amount =>{
        if(amount == 0 && answers.length != 0){
          console.log(`6) going to put the answers that were not already online ${answers.length}`);
          this.putAnswers(answers);
        };
      })
  }
}
