import { TestBed } from '@angular/core/testing';

import { SubtestDownloadService } from './subtest-download.service';

describe('SubtestDownloadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubtestDownloadService = TestBed.get(SubtestDownloadService);
    expect(service).toBeTruthy();
  });
});
