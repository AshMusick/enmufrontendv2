import { TestBed } from '@angular/core/testing';

import { SubtestUploadService } from './subtest-upload.service';

describe('SubtestUploadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubtestUploadService = TestBed.get(SubtestUploadService);
    expect(service).toBeTruthy();
  });
});
