//import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SubtestsService } from '../subtests.service';
import { SqliteService } from '../../../../core-services/sqlite.service';
import { Participant, Protocol } from '../../../protocol/protocol';
import { Answer } from '../../interfaces/question';
import { Subject, BehaviorSubject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators'
import { SqliteSubtestsService } from './sqlite-subtests.service';
import { SqliteProtocolService } from '../../../protocol/sqlite-protocol.service';

@Injectable({
  providedIn: 'root'
})
export class SubtestDownloadService {
  operationStatus = new Subject();
  localParticipantId = new BehaviorSubject(0);
  localProtocolId = new BehaviorSubject(0);
  proto = new Subject<Protocol>();

  participant : Participant;
  answers : Answer[];
  protocol : Protocol;
  //plt : Platform;

  constructor(private sqliteProtocolService : SqliteProtocolService,
              private subtestsService : SubtestsService,
              private sqliteService : SqliteService,
              private sqliteSubtestsService : SqliteSubtestsService) { 

  }

  downloadProtocol(protocol : Protocol, participant : Participant){
    this.addParticipantLocally(participant);
    this.addProtocolLocally(protocol);
    this.addAnswersLocally(protocol.id_protocol);
    this.operationStatus.subscribe(finished=>{
      if(finished){
        this.proto.next(this.protocol)
      }
    })
  }
  

  downloadExistingProtocol( protocol : Protocol ){
    this.updateProtocolLocally(protocol);
    this.updateAnswersLocally(protocol.online_id);
  }

  getProto(){
    return this.proto.asObservable();
  }
  /**Add participant locally giving it a local id with the autoincrementing pk. 
   * Save its online id in the attribute online_id. Then save the id of the result in
   * an observable to be used in addProtocolLocally(...) */
  addParticipantLocally(participant: Participant) {
    participant.online_id = participant.id_participant;
    delete participant.id_participant;
    this.sqliteProtocolService.addParticipant(participant)
      .pipe(takeUntil(this.operationStatus))
      .subscribe(participantAdded =>{
          if(participantAdded.id_participant){
            this.localParticipantId.next(participantAdded.id_participant);
            this.participant = participantAdded;
          } 
        });
  }

  /**Add protocol locally giving it a local id with the autoincrementing pk. 
   * Save its online id in the attribute online_id. Then save the id of the result in
   * an observable to be used in addAnswersLocally(...) */
  addProtocolLocally(protocol: Protocol) {
    this.localParticipantId.subscribe(id =>{
      if(id != 0){
        console.log(id)
        protocol.id_participant = id;
        protocol.online_id = protocol.id_protocol;
        delete protocol.id_protocol;
        this.sqliteProtocolService.addProtocol(protocol)
          .pipe(takeUntil(this.operationStatus))
          .subscribe(localProtocol =>{
            if(localProtocol.id_protocol){
              console.log('protocol added')
              this.localProtocolId.next(localProtocol.id_protocol)
              this.protocol = localProtocol;
            }
          });
      }
    })
  }

  /**Get answers from server and add answers locally giving them a local id with the autoincrementing pk. 
   * Save their online ids in the attribute online_id. */
  addAnswersLocally(id_protocol: number) {
    let answerCount = new BehaviorSubject(-1);
    let ans: Answer[] = [];
    this.subtestsService.getProtocolAnswers(id_protocol)
      .pipe(takeUntil(this.operationStatus))
      .subscribe(answers =>{
        answerCount.next(answers.length)
        this.localProtocolId.subscribe(id=>{
          if(id != 0){
            answers.forEach(answer =>{
              answer.id_protocol = id;
              answer.online_id = answer.id_answer;
              delete answer.id_answer;
              console.log(`This is the online id: ${answer.online_id}`);
              this.sqliteSubtestsService.addAnswer(answer)
                .pipe(takeUntil(this.operationStatus))
                .subscribe(localAnswer =>{
                  if(localAnswer.id_answer){
                    answerCount.next(answerCount.value - 1);
                    ans.push(localAnswer);
                  }
                })
            })

          }
        })

        answerCount
        .pipe(takeUntil(this.operationStatus))
        .subscribe(count =>{
          if(count == 0){
            this.answers = ans;
            this.operationStatus.next(`Se descargó el protocolo con éxito`);
            this.operationStatus.unsubscribe();
          }
        })
      })
  }
  

  updateProtocolLocally(protocol: Protocol) {
    this.sqliteService.editProtocolLocally(protocol)
      .then(localProtocol =>{
        this.localProtocolId.next(localProtocol.id_protocol);
      })
  }

  updateAnswersLocally(onlineId : number) {
    this.localProtocolId
    .pipe(takeUntil(this.operationStatus))
    .subscribe(localId=>{
      this.sqliteService.deleteProtocolAnswers(localId)
        .then((data)=>{
          if(data){
            this.addAnswersLocally(onlineId)
          }
        })
    })
  }
  
}
