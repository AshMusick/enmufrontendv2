import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Subtest, QuestionVersion } from '../interfaces/subtest';
import { Attribute } from '../interfaces/attribute';
import { Question, QuestionAttribute, Answer } from '../interfaces/question';
import { MyComponent, Score } from '../scores/score';
import { PossibleAnswer } from '../interfaces/possible-answers';
import { HttpErrorHandler, HandleError } from '../../../core-services/http-error-handler.service';
import { Protocol, Participant } from '../../protocol/protocol'

import { environment } from '../../../../environments/environment'


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'token ' + localStorage.getItem('token')
  })
};


@Injectable({providedIn : 'root'})
export class SubtestsService {
  private subtestsReady: BehaviorSubject<boolean> = new BehaviorSubject(true);

  subtestsUrl = environment.backendServer + '/api/subtests/';
  attributesUrl = environment.backendServer + '/api/attributes/';
  questionsUrl = environment.backendServer + '/api/questions/';
  answersUrl = environment.backendServer + '/api/answers/';
  protocolAnswersUrl = environment.backendServer + '/api/protocol-answers/';
  vquestionsUrl = environment.backendServer + '/api/vquestions/';
  questionAttributesUrl = environment.backendServer + '/api/questionxattributes/'; 
  possibleAnswerUrl = environment.backendServer + '/api/answerbank/';
  questionVersionUrl = environment.backendServer + '/api/questionxversions/'
  protocolUrl = environment.backendServer + '/api/protocols/';
  participantUrl = environment.backendServer + '/api/participants/';
  componentsUrl = environment.backendServer + '/api/components/';
  scoresUrl = environment.backendServer + '/api/scores/';
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('SubtestsService');
  }

  getSubtestsDataState(){
    return this.subtestsReady.asObservable();
  }

  /** GET subtests from the server */
  getSubtests (): Observable<Subtest[]> {
    return this.http.get<Subtest[]>(this.subtestsUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getSubtests', []))
      );
  }

  /** GET answer bank from the server */
  getPossibleAnswers (): Observable<PossibleAnswer[]> {
    return this.http.get<PossibleAnswer[]>(this.possibleAnswerUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getPossibleAnswers', []))
      );
  }

  /** GET attributes from the server */
  getAttributes (): Observable<Attribute[]> {
    return this.http.get<Attribute[]>(this.attributesUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getAttributes', []))
      );
  }

  /** GET questions from the server */
  getQuestions (): Observable<Question[]> {
    return this.http.get<Question[]>(this.questionsUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getQuestions', []))
      );
  }

  /** GET question attributes from the server */
  getQuestionAttributes (): Observable<QuestionAttribute[]> {
    return this.http.get<QuestionAttribute[]>(this.questionAttributesUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getQuestionAttributes', []))
      );
  }

  /** GET answers from the server */
  getAnswers (): Observable<Answer[]> {
    return this.http.get<Answer[]>(this.answersUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getAnswers', []))
      );
  }

  /** GET answers from the server for a specific protocol*/
  getProtocolAnswers (protocolId:number): Observable<Answer[]> {
    const url = `${this.protocolAnswersUrl}${protocolId}/`
    return this.http.get<Answer[]>(url, httpOptions)
      .pipe(
        catchError(this.handleError('getAnswers', []))
      );
  }

  /** GET question versions from the server */
  getQuestionVersions (): Observable<QuestionVersion[]> {
    return this.http.get<QuestionVersion[]>(this.questionVersionUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getQuestionVersions', []))
      );
  }

  /** GET question versions from the server */
  getProtocol (id : number): Observable<Protocol> {
    const url = `${this.protocolUrl}${id}/`;
    const proto: Protocol = {} as Protocol;
    return this.http.get<Protocol>(url, httpOptions)
    .pipe(
      catchError(this.handleError('getProtocol', proto))
    );    
  }

  /** GET Mycomponents from the server */
  getComponents (): Observable<MyComponent []> {
    return this.http.get<MyComponent[]>(this.componentsUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getComponents', []))
      );
  }

  /** GET scores from the server */
  getScores (): Observable<Score []> {
    return this.http.get<Score[]>(this.scoresUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getScores', []))
      );
  }

  getParticipant (id : number): Observable<Participant> {
    const url = `${this.participantUrl}${id}/`;
    const proto: Participant = {} as Participant;
    return this.http.get<Participant>(url, httpOptions)
    .pipe(
      catchError(this.handleError('getParticipant', proto))
    );    
  }


  /** POST: add a new answer to the database */
  addAnswer (answer: Answer): Observable<Answer> {
    return this.http.post<Answer>(this.answersUrl, answer, httpOptions)
      .pipe(
        catchError(this.handleError('addAnswer', answer))
      );
  }

  /** POST: add a new answer to the database */
  addPossibleAnswer (possibleAnswer: PossibleAnswer): Observable<PossibleAnswer> {
    return this.http.post<PossibleAnswer>(this.possibleAnswerUrl, possibleAnswer, httpOptions)
      .pipe(
        catchError(this.handleError('addPossibleAnswer', possibleAnswer))
      );
  }

  /** POST: add a new score to the database */
  addScore (score: Score): Observable<Score> {
    return this.http.post<Score>(this.scoresUrl, score, httpOptions)
      .pipe(
        catchError(this.handleError('addscore', score))
      );
  }

  /** DELETE: delete the answer from the server */
  deleteAnswer (id_answer: number): Observable<{}> {
    const url = `${this.answersUrl}${id_answer}`; // DELETE api/answers/42
    return this.http.delete(url, httpOptions)
      .pipe(
        catchError(this.handleError('deleteAnswer'))
      );
  }

  /** PUT: update the answer on the server. Returns the updated answer upon success. */
  updateAnswer (answer: Answer): Observable<Answer> {
      const url = `${this.answersUrl}${answer.id_answer}/`;
      return this.http.put<Answer>(url, answer, httpOptions)
      .pipe(
        catchError(this.handleError('updateAnswer', answer))
      );
  }

  /** PUT: update the protocol on the server. Returns the updated protocol upon success. */
  updateProtocol (protocol: Protocol): Observable<Protocol> {
      const url = `${this.protocolUrl}${protocol.id_protocol}/`;
      return this.http.put<Protocol>(url, protocol, httpOptions)
      .pipe(
        catchError(this.handleError('updateProtocol', protocol))
      );
  }

  /** PUT: update the score on the server. Returns the updated score upon success. */
  updateScore (score: Score): Observable<Score> {
    const url = `${this.scoresUrl}${score.id_score}/`;
    return this.http.put<Score>(url, score, httpOptions)
    .pipe(
      catchError(this.handleError('updateScore', score))
    );
}

/** DELETE: delete the score from the server */
deleteScore (id_protocol: number): Observable<{}> {
  const url = `${this.scoresUrl}${id_protocol}`; // DELETE api/scores/42
  return this.http.delete(url, httpOptions)
    .pipe(
      catchError(this.handleError('deleteScore'))
    );
}

}