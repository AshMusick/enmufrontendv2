import { TestBed } from '@angular/core/testing';

import { SubtestsService } from './subtests.service';

describe('SubtestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubtestsService = TestBed.get(SubtestsService);
    expect(service).toBeTruthy();
  });
});
