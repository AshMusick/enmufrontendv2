import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Page404SubtestComponent } from './page404-subtest.component';

describe('Page404SubtestComponent', () => {
  let component: Page404SubtestComponent;
  let fixture: ComponentFixture<Page404SubtestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Page404SubtestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Page404SubtestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
