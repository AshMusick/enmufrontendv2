import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutofocusModule } from 'angular-autofocus-fix';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { SubtestsComponent } from './component/subtests.component';
import { Page404SubtestComponent } from './page404-subtest/page404-subtest.component';
import { SubtestsRoutingModuleModule } from './subtests-routing-module.module';
import { ScoresComponent } from './scores/scores.component';
import { SubtestsService } from './service/subtests.service';
import { SqliteSubtestsService } from './service/sync/sqlite-subtests.service'
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


@NgModule({
  declarations: [
    SubtestsComponent,
    ScoresComponent,
    Page404SubtestComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    AutofocusModule,
    BsDropdownModule.forRoot(),
    NgbModule,
    SubtestsRoutingModuleModule
  ],
  providers: [
    SubtestsService,
    SqliteSubtestsService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SubtestsModule {}
