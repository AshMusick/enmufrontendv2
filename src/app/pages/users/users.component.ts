import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/login/login.service';
import { AuthService } from 'src/app/core-services/auth.service';
import { User } from 'src/app/login/login';
import { faToggleOff, faToggleOn } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [LoginService, AuthService]
})
export class UsersComponent implements OnInit {

  loginService : LoginService;
  authService : AuthService;
  currentUser : User;
  token : string;
  users : User[];

  faToggleOff = faToggleOff;
  faToggleOn = faToggleOn;

  constructor(loginService : LoginService, authService : AuthService) { 
    this.currentUser = authService.currentUserValue;
    this.authService = authService;
    this.loginService = loginService;
    this.token = localStorage.getItem('token')
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.loginService.getUsers(this.token)
      .subscribe(users => {
        (this.users = users);
      });
  }

  //TODO:: Make a report for each user's protocols
  getReport():void {
    console.log('hola')
  }

  toggleAdmin(user: User){
    user.is_staff = !user.is_staff;
    this.loginService.updateUser(user, this.token)
      .subscribe(res =>{
        this.users.forEach(u => {
          if(res.id === u.id){
            u.is_staff = res.is_staff;
          }
        })
      }) 
  }

  toggleActive(user: User){
    user.is_active = !user.is_active;
    this.loginService.updateUser(user, this.token)
      .subscribe(res =>{
        this.users.forEach(u => {
          if(res.id === u.id){
            u.is_active = res.is_active;
          }
        })
      }) 
  }

}
