--
-- PostgreSQL database dump
CREATE TABLE IF NOT EXISTS answer_bank (
    answer_id integer PRIMARY KEY AUTOINCREMENT,
    topic_id integer,
    answer text
);

--
-- Name: answer_bank_topics; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS answer_bank_topics (
    id integer PRIMARY KEY AUTOINCREMENT,
    topic text
);

--
-- Name: answer_types; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS answer_types (
    id_type integer PRIMARY KEY AUTOINCREMENT,
    description text);



--
-- Name: answers; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS answers (
    id_answer integer PRIMARY KEY AUTOINCREMENT,
    id_item integer,
    id_protocol integer,
    answer text,
    id_attribute integer,
    online_id integer --attribute only exists in offline database
);




--
-- Name: attributes; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS attributes (
    id_attribute integer PRIMARY KEY AUTOINCREMENT,
    description text,
    id_subtest integer
);

--
-- Name: calculations; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS calculations (
    id_points integer PRIMARY KEY AUTOINCREMENT,
    id_item integer,
    id_answer_type integer,
    value numeric,
    id_score integer
);

--
-- Name: components; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS components (
    id integer PRIMARY KEY AUTOINCREMENT,
    name text,
    id_subtest integer
);



--
-- Name: participants; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS participants (
    id_participant integer PRIMARY KEY AUTOINCREMENT,
    last_name text,
    name text,
    sexo integer,
    school_years integer,
    birthday date,
    online_id integer --column only included in offline database
);

--
-- Name: possible_answers; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS possible_answers (
    id_answer integer PRIMARY KEY AUTOINCREMENT,
    answer_option text,
    id_versioned_subtest integer
);




--
-- Name: protocols; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS protocols (
    id_protocol integer PRIMARY KEY AUTOINCREMENT,
    id_participant integer,
    id_version integer,
    test_admin_id integer,
    start_date date,
    group_num integer,
    case_num integer,
    age integer,
    online_id integer --column only included in offline database
);

--
-- Name: questions; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS questions (
    id_question integer PRIMARY KEY AUTOINCREMENT,
    description text,
    correct_answer text
);


--
-- Name: questions_x_attributes; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS questions_x_attributes (
    id_que_att integer PRIMARY KEY AUTOINCREMENT,
    id_attribute integer NOT NULL,
    id_question integer NOT NULL,
    re_order_key integer
);


--
-- Name: questions_x_versions; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS questions_x_versions (
    id_question integer NOT NULL,
    id_version integer NOT NULL,
    id_item integer PRIMARY KEY, 
    value numeric
);

--
-- Name: scores; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS scores (
    id_score integer PRIMARY KEY AUTOINCREMENT,
    id_protocol integer,
    score numeric,
    id_component integer,
    deleted integer
);



--
-- Name: subtests; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS subtests (
    id_subtest integer PRIMARY KEY AUTOINCREMENT,
    description text
);


CREATE VIEW IF NOT EXISTS v_questions AS
 SELECT answers.id_answer,
    answers.id_protocol,
    answers.id_item,
    questions.id_question,
    questions.description,
    questions.correct_answer,
    answers.answer
   FROM answers,
    questions
  WHERE (questions.id_question = answers.id_item)
  ORDER BY answers.id_protocol;

--
-- Name: v_scores; Type: VIEW; Schema: public; Owner: ashleymusick
--

/* CREATE VIEW v_scores AS
 SELECT ct.id_protocol,
    ct."Reading",
    ct."WL_TRIAL_1",
    ct."WL_TRIAL_2",
    ct."WL_TRIAL_3",
    ct."WL_TRIAL_123",
    ct."WL_TRIAL_123_Perc",
    ct."DELAYED_TRIAL_SCORE",
    ct."DELAYED_TRIAL__Perc",
    ct."True_Positives",
    ct."Related_false_Positives",
    ct."Unrealated false Positives",
    ct."DISCRIMINATION_INDEX",
    ct."Flower_Hits_Immed",
    ct."Hand_Commissions_Immed",
    ct."Leaf_Hits_Immed",
    ct."Hand_Hits_Immed",
    ct."Building_Hits_Immed",
    ct."Flower_Commissions_Immed",
    ct."Leaf_Commissions_Immed",
    ct."Building_Commissions_Immed",
    ct."Flower_Omissions_Immed",
    ct."Leaf_Omissions_Immed",
    ct."Hand_Omissions_Immed",
    ct."Building_Omissions_Immed",
    ct."TOTAL_VISUAL_MEM_IMMED",
    ct."VISUAL_MEM_IMMED_Perc",
    ct."Leaf_Hits_Delayed",
    ct."Building_Hits_Delayed",
    ct."TOTAL_HITS",
    ct."TOTAL_COMMISSIONS",
    ct."TOTAL_OMISSIONS",
    ct."TOTAL_HITS_DELAYED",
    ct."Leaf_Commissions_Delayed",
    ct."Building_Commissions_Delayed",
    ct."TOTAL_COMMISSIONS_DELAYED",
    ct."Leaf_Omissions_Delayed",
    ct."Building_Omissions_Delayed",
    ct."TOTAL_OMISSIONS_DELAYED",
    ct."TOTAL_VISUAL_MEM_DELAYED",
    ct."VISUAL_MEM_DELAYED_Perc",
    ct."Arrows_Part_1",
    ct."Arrows_Part_2_LE",
    ct."Arrows_Part_2_HE",
    ct."BLOCKS travelled",
    ct."Blocks score",
    ct."Saved coins",
    ct."Exceeded Coins",
    ct."Coins_score",
    ct."TOTAL PARTY",
    ct."PARTY_Perc",
    ct."Points_&_Lines_1",
    ct."Points_&_Lines_2",
    ct."Points_&_Lines_3",
    ct."Points_&_Lines_4",
    ct."TOTAL_POINTS_&_LINES",
    ct."POINTS_&_LINES_Perc",
    ct."LE_Personage_spontaneous",
    ct."LE_Personage_cues",
    ct."HE_Personage_spontaneous",
    ct."HE_Personage_cues",
    ct."TOTAL_PERSONAGE",
    ct."PERSONAGE_Perc",
    ct."TOTAL_ANIMALS",
    ct."ANIMALS_Perc"
   FROM crosstab('SELECT id_protocol, id_component, score from scores where deleted is null order by 1,2'::text, 'select m from generate_series(1,64) m'::text) ct(id_protocol integer, "Reading" numeric, "WL_TRIAL_1" numeric, "WL_TRIAL_2" numeric, "WL_TRIAL_3" numeric, "WL_TRIAL_123" numeric, "WL_TRIAL_123_Perc" numeric, "DELAYED_TRIAL_SCORE" numeric, "DELAYED_TRIAL__Perc" numeric, "True_Positives" numeric, "Related_false_Positives" numeric, "Unrealated false Positives" numeric, "DISCRIMINATION_INDEX" numeric, "Flower_Hits_Immed" numeric, "Hand_Commissions_Immed" numeric, "Leaf_Hits_Immed" numeric, "Hand_Hits_Immed" numeric, "Building_Hits_Immed" numeric, "Flower_Commissions_Immed" numeric, "Leaf_Commissions_Immed" numeric, "Building_Commissions_Immed" numeric, "Flower_Omissions_Immed" numeric, "Leaf_Omissions_Immed" numeric, "Hand_Omissions_Immed" numeric, "Building_Omissions_Immed" numeric, "TOTAL_VISUAL_MEM_IMMED" numeric, "VISUAL_MEM_IMMED_Perc" numeric, "Leaf_Hits_Delayed" numeric, "Building_Hits_Delayed" numeric, "TOTAL_HITS" numeric, "TOTAL_COMMISSIONS" numeric, "TOTAL_OMISSIONS" numeric, "TOTAL_HITS_DELAYED" numeric, "Leaf_Commissions_Delayed" numeric, "Building_Commissions_Delayed" numeric, "TOTAL_COMMISSIONS_DELAYED" numeric, "Leaf_Omissions_Delayed" numeric, "Building_Omissions_Delayed" numeric, "TOTAL_OMISSIONS_DELAYED" numeric, "TOTAL_VISUAL_MEM_DELAYED" numeric, "VISUAL_MEM_DELAYED_Perc" numeric, "Arrows_Part_1" numeric, "Arrows_Part_2_LE" numeric, "Arrows_Part_2_HE" numeric, "BLOCKS travelled" numeric, "Blocks score" numeric, "Saved coins" numeric, "Exceeded Coins" numeric, "Coins_score" numeric, "TOTAL PARTY" numeric, "PARTY_Perc" numeric, "Points_&_Lines_1" numeric, "Points_&_Lines_2" numeric, "Points_&_Lines_3" numeric, "Points_&_Lines_4" numeric, "TOTAL_POINTS_&_LINES" numeric, "POINTS_&_LINES_Perc" numeric, "LE_Personage_spontaneous" numeric, "LE_Personage_cues" numeric, "HE_Personage_spontaneous" numeric, "HE_Personage_cues" numeric, "TOTAL_PERSONAGE" numeric, "PERSONAGE_Perc" numeric, "TOTAL_ANIMALS" numeric, "ANIMALS_Perc" numeric);

 */

--
-- Name: versioned_subtests; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS versioned_subtests (
    id_versioned_subtest integer PRIMARY KEY AUTOINCREMENT,
    id_version integer NOT NULL,
    id_subtest integer NOT NULL
);




--
-- Name: versions; Type: TABLE; Schema: public; Owner: ashleymusick
--

CREATE TABLE IF NOT EXISTS versions (
    id_version integer PRIMARY KEY AUTOINCREMENT,
    description text
);



-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

--
-- Data for Name: answer_bank_topics; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--

INSERT or ignore INTO answer_bank_topics VALUES (1, 'Animals');


--
-- Data for Name: answer_bank; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--

INSERT or ignore INTO answer_bank VALUES (1, 1, 'gato');
INSERT or ignore INTO answer_bank VALUES (2, 1, 'perro');
INSERT or ignore INTO answer_bank VALUES (3, 1, 'pájaro');
INSERT or ignore INTO answer_bank VALUES (5, 1, 'toro');
INSERT or ignore INTO answer_bank VALUES (6, 1, 'abeja');
INSERT or ignore INTO answer_bank VALUES (7, 1, 'araña');
INSERT or ignore INTO answer_bank VALUES (8, 1, 'avispa');
INSERT or ignore INTO answer_bank VALUES (9, 1, 'ballena');
INSERT or ignore INTO answer_bank VALUES (10, 1, 'bisonte');
INSERT or ignore INTO answer_bank VALUES (11, 1, 'búfalo');
INSERT or ignore INTO answer_bank VALUES (12, 1, 'burro');
INSERT or ignore INTO answer_bank VALUES (13, 1, 'caballo');
INSERT or ignore INTO answer_bank VALUES (14, 1, 'camello');
INSERT or ignore INTO answer_bank VALUES (15, 1, 'canario');
INSERT or ignore INTO answer_bank VALUES (16, 1, 'cangrejo');
INSERT or ignore INTO answer_bank VALUES (17, 1, 'canguro');
INSERT or ignore INTO answer_bank VALUES (18, 1, 'caracol');
INSERT or ignore INTO answer_bank VALUES (19, 1, 'cebra');
INSERT or ignore INTO answer_bank VALUES (20, 1, 'cerdo');
INSERT or ignore INTO answer_bank VALUES (21, 1, 'chimpancé');
INSERT or ignore INTO answer_bank VALUES (22, 1, 'ciervo');
INSERT or ignore INTO answer_bank VALUES (23, 1, 'cisne');
INSERT or ignore INTO answer_bank VALUES (24, 1, 'cocodrilo');
INSERT or ignore INTO answer_bank VALUES (25, 1, 'elefante');
INSERT or ignore INTO answer_bank VALUES (26, 1, 'escarabajo');
INSERT or ignore INTO answer_bank VALUES (27, 1, 'escorpión');
INSERT or ignore INTO answer_bank VALUES (28, 1, 'foca');
INSERT or ignore INTO answer_bank VALUES (29, 1, 'gallina');
INSERT or ignore INTO answer_bank VALUES (31, 1, 'golondrina');
INSERT or ignore INTO answer_bank VALUES (32, 1, 'hipopótamo');
INSERT or ignore INTO answer_bank VALUES (33, 1, 'hormiga');
INSERT or ignore INTO answer_bank VALUES (34, 1, 'jabalí');
INSERT or ignore INTO answer_bank VALUES (35, 1, 'jirafa');
INSERT or ignore INTO answer_bank VALUES (36, 1, 'león');
INSERT or ignore INTO answer_bank VALUES (37, 1, 'loro');
INSERT or ignore INTO answer_bank VALUES (38, 1, 'mosca');
INSERT or ignore INTO answer_bank VALUES (39, 1, 'mosquito');
INSERT or ignore INTO answer_bank VALUES (40, 1, 'oso');
INSERT or ignore INTO answer_bank VALUES (41, 1, 'oveja');
INSERT or ignore INTO answer_bank VALUES (42, 1, 'perdiz');
INSERT or ignore INTO answer_bank VALUES (43, 1, 'pingüino');
INSERT or ignore INTO answer_bank VALUES (44, 1, 'pollo');
INSERT or ignore INTO answer_bank VALUES (45, 1, 'saltamontes');
INSERT or ignore INTO answer_bank VALUES (46, 1, 'serpiente');
INSERT or ignore INTO answer_bank VALUES (47, 1, 'tigre');
INSERT or ignore INTO answer_bank VALUES (48, 1, 'topo');
INSERT or ignore INTO answer_bank VALUES (50, 1, 'tortuga');
INSERT or ignore INTO answer_bank VALUES (51, 1, 'vaca');
INSERT or ignore INTO answer_bank VALUES (52, 1, 'zorro');
INSERT or ignore INTO answer_bank VALUES (4, 1, 'águila');
INSERT or ignore INTO answer_bank VALUES (30, 1, 'gallo');
INSERT or ignore INTO answer_bank VALUES (49, 1, 'leopardo');
INSERT or ignore INTO answer_bank VALUES (55, 1, 'mono');
INSERT or ignore INTO answer_bank VALUES (56, 1, 'mono');
INSERT or ignore INTO answer_bank VALUES (57, 1, 'leon');
INSERT or ignore INTO answer_bank VALUES (58, 1, 'Leon');
INSERT or ignore INTO answer_bank VALUES (59, 1, 'va');
INSERT or ignore INTO answer_bank VALUES (60, 1, 'dinosaurio');
INSERT or ignore INTO answer_bank VALUES (61, 1, 'pato');
INSERT or ignore INTO answer_bank VALUES (62, 1, 'le');
INSERT or ignore INTO answer_bank VALUES (63, 1, 'ch');
INSERT or ignore INTO answer_bank VALUES (64, 1, 'sally');
INSERT or ignore INTO answer_bank VALUES (65, 1, 'jump');
INSERT or ignore INTO answer_bank VALUES (66, 1, 'joy');
INSERT or ignore INTO answer_bank VALUES (67, 1, 'go');
INSERT or ignore INTO answer_bank VALUES (68, 1, 'apple');
INSERT or ignore INTO answer_bank VALUES (69, 1, 'lkj');
INSERT or ignore INTO answer_bank VALUES (70, 1, 'oh');
INSERT or ignore INTO answer_bank VALUES (71, 1, 'test');
INSERT or ignore INTO answer_bank VALUES (72, 1, 'fast');
INSERT or ignore INTO answer_bank VALUES (73, 1, 'new');
INSERT or ignore INTO answer_bank VALUES (74, 1, 'ok');
INSERT or ignore INTO answer_bank VALUES (75, 1, 'why');
INSERT or ignore INTO answer_bank VALUES (76, 1, 'yes');
INSERT or ignore INTO answer_bank VALUES (77, 1, 'sure');
INSERT or ignore INTO answer_bank VALUES (78, 1, 'hamster');
INSERT or ignore INTO answer_bank VALUES (79, 1, 'gorilla');
INSERT or ignore INTO answer_bank VALUES (80, 1, 'jilguero');
INSERT or ignore INTO answer_bank VALUES (81, 1, 'ji');
INSERT or ignore INTO answer_bank VALUES (82, 1, 'zebra');
INSERT or ignore INTO answer_bank VALUES (83, 1, 'víbora');
INSERT or ignore INTO answer_bank VALUES (84, 1, 'salmón');
INSERT or ignore INTO answer_bank VALUES (85, 1, 'bicho');
INSERT or ignore INTO answer_bank VALUES (86, 1, 'ca');


--
-- Data for Name: answer_types; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--



--
-- Data for Name: subtests; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--

INSERT or ignore INTO subtests VALUES (3, 'Subtest de Memoria Visual');
INSERT or ignore INTO subtests VALUES (4, 'Subtest Contar Flechas');
INSERT or ignore INTO subtests VALUES (5, 'Subtest de la Fiesta');
INSERT or ignore INTO subtests VALUES (6, 'Subtest "Puntos y Líneas"');
INSERT or ignore INTO subtests VALUES (1, 'Subtest de Velocidad de Lectura');
INSERT or ignore INTO subtests VALUES (2, 'Subtest Aprendizaje de Palabras');
INSERT or ignore INTO subtests VALUES (9, 'Subtest del Reconocimiento');
INSERT or ignore INTO subtests VALUES (10, 'Subtest de Memoria Visual (cont.)');
INSERT or ignore INTO subtests VALUES (11, 'Subtest de Animales');
INSERT or ignore INTO subtests VALUES (7, 'Subtest Memoria del Personaje');
INSERT or ignore INTO subtests VALUES (8, 'Aprendizaje de Palabras (Recuerdo Diferido)');


--
-- Data for Name: attributes; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--

INSERT or ignore INTO attributes VALUES (11, 'General', 5);
INSERT or ignore INTO attributes VALUES (12, 'General', 6);
INSERT or ignore INTO attributes VALUES (7, 'Recuerdo Inmediato', 3);
INSERT or ignore INTO attributes VALUES (9, 'Parte A', 4);
INSERT or ignore INTO attributes VALUES (10, 'Parte B', 4);
INSERT or ignore INTO attributes VALUES (1, 'Tiempo en segundos', 1);
INSERT or ignore INTO attributes VALUES (2, 'Errores', 1);
INSERT or ignore INTO attributes VALUES (3, '1', 2);
INSERT or ignore INTO attributes VALUES (4, '2', 2);
INSERT or ignore INTO attributes VALUES (5, '3', 2);
INSERT or ignore INTO attributes VALUES (15, 'General', 11);
INSERT or ignore INTO attributes VALUES (8, 'Recuerdo Diferido', 10);
INSERT or ignore INTO attributes VALUES (13, 'Recuerdo Espontáneo', 7);
INSERT or ignore INTO attributes VALUES (14, 'Recuerdo con Claves', 7);
INSERT or ignore INTO attributes VALUES (6, 'Recuerdo Diferido de La Memoria de Palabras', 8);
INSERT or ignore INTO attributes VALUES (16, 'Reconocimiento', 9);

--
-- Data for Name: versions; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--

INSERT or ignore INTO versions VALUES (1, 'Examen Original - Corta');
INSERT or ignore INTO versions VALUES (2, 'Examen Original - Larga');
INSERT or ignore INTO versions VALUES (3, 'describing');


--
-- Data for Name: protocols; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--



--
-- Data for Name: questions; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--

INSERT or ignore INTO questions VALUES (16, '14', 'Rodilla');
INSERT or ignore INTO questions VALUES (61, 'Mano: Sector 21', '0');
INSERT or ignore INTO questions VALUES (1, '¿Cuántos segundos?', NULL);
INSERT or ignore INTO questions VALUES (2, '¿Cuántos errores?', NULL);
INSERT or ignore INTO questions VALUES (4, '2', 'Oreja');
INSERT or ignore INTO questions VALUES (17, 'Pétalo 1', '0');
INSERT or ignore INTO questions VALUES (18, 'Pétalo 2', '1');
INSERT or ignore INTO questions VALUES (19, 'Pétalo 3', '1');
INSERT or ignore INTO questions VALUES (20, 'Pétalo 4', '0');
INSERT or ignore INTO questions VALUES (21, 'Pétalo 5', '1');
INSERT or ignore INTO questions VALUES (22, 'Pétalo 6', '0');
INSERT or ignore INTO questions VALUES (23, 'Pétalo 7', '0');
INSERT or ignore INTO questions VALUES (24, 'Pétalo 8', '1');
INSERT or ignore INTO questions VALUES (25, 'Pétalo 9', '0');
INSERT or ignore INTO questions VALUES (26, 'Pétalo 10', '0');
INSERT or ignore INTO questions VALUES (27, 'Pétalo 11', '0');
INSERT or ignore INTO questions VALUES (28, 'Pétalo 12', '0');
INSERT or ignore INTO questions VALUES (29, 'Hoja: Sector 1', '0');
INSERT or ignore INTO questions VALUES (30, 'Hoja: Sector 2', '1');
INSERT or ignore INTO questions VALUES (31, 'Hoja: Sector 3', '0');
INSERT or ignore INTO questions VALUES (32, 'Hoja: Sector 4', '1');
INSERT or ignore INTO questions VALUES (33, 'Hoja: Sector 5', '0');
INSERT or ignore INTO questions VALUES (34, 'Hoja: Sector 6', '0');
INSERT or ignore INTO questions VALUES (35, 'Hoja: Sector 7', '1');
INSERT or ignore INTO questions VALUES (36, 'Hoja: Sector 8', '0');
INSERT or ignore INTO questions VALUES (37, 'Hoja: Sector 9', '0');
INSERT or ignore INTO questions VALUES (38, 'Hoja: Sector 10', '0');
INSERT or ignore INTO questions VALUES (39, 'Hoja: Sector 11', '0');
INSERT or ignore INTO questions VALUES (40, 'Hoja: Sector 12', '1');
INSERT or ignore INTO questions VALUES (41, 'Mano: Sector 1', '0');
INSERT or ignore INTO questions VALUES (42, 'Mano: Sector 2', '1');
INSERT or ignore INTO questions VALUES (43, 'Mano: Sector 3', '0');
INSERT or ignore INTO questions VALUES (44, 'Mano: Sector 4', '1');
INSERT or ignore INTO questions VALUES (45, 'Mano: Sector 5', '0');
INSERT or ignore INTO questions VALUES (46, 'Mano: Sector 6', '0');
INSERT or ignore INTO questions VALUES (47, 'Mano: Sector 7', '0');
INSERT or ignore INTO questions VALUES (48, 'Mano: Sector 8', '1');
INSERT or ignore INTO questions VALUES (49, 'Mano: Sector 9', '0');
INSERT or ignore INTO questions VALUES (50, 'Mano: Sector 10', '0');
INSERT or ignore INTO questions VALUES (51, 'Mano: Sector 11', '1');
INSERT or ignore INTO questions VALUES (52, 'Mano: Sector 12', '0');
INSERT or ignore INTO questions VALUES (53, 'Mano: Sector 13', '0');
INSERT or ignore INTO questions VALUES (54, 'Mano: Sector 14', '1');
INSERT or ignore INTO questions VALUES (55, 'Mano: Sector 15', '0');
INSERT or ignore INTO questions VALUES (56, 'Mano: Sector 16', '0');
INSERT or ignore INTO questions VALUES (57, 'Mano: Sector 17', '0');
INSERT or ignore INTO questions VALUES (58, 'Mano: Sector 18', '0');
INSERT or ignore INTO questions VALUES (59, 'Mano: Sector 19', '1');
INSERT or ignore INTO questions VALUES (60, 'Mano: Sector 20', '0');
INSERT or ignore INTO questions VALUES (63, 'Mano: Sector 23', '0');
INSERT or ignore INTO questions VALUES (64, 'Edificio: Ventana 1', '0');
INSERT or ignore INTO questions VALUES (65, 'Edificio: Ventana 2', '0');
INSERT or ignore INTO questions VALUES (66, 'Edificio: Ventana 3', '1');
INSERT or ignore INTO questions VALUES (67, 'Edificio: Ventana 4', '0');
INSERT or ignore INTO questions VALUES (68, 'Edificio: Ventana 5', '1');
INSERT or ignore INTO questions VALUES (69, 'Edificio: Ventana 6', '0');
INSERT or ignore INTO questions VALUES (70, 'Edificio: Ventana 7', '0');
INSERT or ignore INTO questions VALUES (71, 'Edificio: Ventana 8', '0');
INSERT or ignore INTO questions VALUES (72, 'Edificio: Ventana 9', '0');
INSERT or ignore INTO questions VALUES (73, 'Edificio: Ventana 10', '1');
INSERT or ignore INTO questions VALUES (74, 'Edificio: Ventana 11', '0');
INSERT or ignore INTO questions VALUES (75, 'Edificio: Ventana 12', '1');
INSERT or ignore INTO questions VALUES (76, 'Edificio: Ventana 13', '0');
INSERT or ignore INTO questions VALUES (77, 'Edificio: Ventana 14', '1');
INSERT or ignore INTO questions VALUES (78, 'Edificio: Ventana 15', '0');
INSERT or ignore INTO questions VALUES (79, 'Edificio: Ventana 16', '0');
INSERT or ignore INTO questions VALUES (80, 'Edificio: Ventana 17', '0');
INSERT or ignore INTO questions VALUES (81, 'Edificio: Ventana 18', '0');
INSERT or ignore INTO questions VALUES (82, 'Edificio: Ventana 19', '0');
INSERT or ignore INTO questions VALUES (83, 'Edificio: Ventana 20', '1');
INSERT or ignore INTO questions VALUES (84, 'Edificio: Ventana 21', '0');
INSERT or ignore INTO questions VALUES (85, 'Edificio: Ventana 22', '1');
INSERT or ignore INTO questions VALUES (86, 'Edificio: Ventana 23', '0');
INSERT or ignore INTO questions VALUES (87, 'Edificio: Ventana 24', '0');
INSERT or ignore INTO questions VALUES (88, 'Flechas hacia la derecha', '11');
INSERT or ignore INTO questions VALUES (89, 'Flechas hacia la izquierda', '10');
INSERT or ignore INTO questions VALUES (90, 'Flechas hacia arriba', '11');
INSERT or ignore INTO questions VALUES (91, 'Flechas hacia arriba + izquierda', '21');
INSERT or ignore INTO questions VALUES (92, 'Cantidad de elementos comprados', NULL);
INSERT or ignore INTO questions VALUES (94, 'Cantidad de monedas ahorradas', NULL);
INSERT or ignore INTO questions VALUES (95, 'Cantidad de monedas excedidas', NULL);
INSERT or ignore INTO questions VALUES (96, 'Figura 1', '10');
INSERT or ignore INTO questions VALUES (97, 'Figura 2', '12');
INSERT or ignore INTO questions VALUES (98, 'Figura 3', '28');
INSERT or ignore INTO questions VALUES (99, 'Figura 4', '26');
INSERT or ignore INTO questions VALUES (103, '¿Cuántos años tiene?', '54');
INSERT or ignore INTO questions VALUES (104, '¿Cuál es su estado civil?', 'Casado');
INSERT or ignore INTO questions VALUES (105, '¿Cuántos hijos tiene?', '4');
INSERT or ignore INTO questions VALUES (107, '¿Cuál es su estatura?', 'Alto');
INSERT or ignore INTO questions VALUES (108, '¿En qué trabaja?', 'Soldado');
INSERT or ignore INTO questions VALUES (109, '¿En qué viaja para llegar a su trabajo?', 'En tren');
INSERT or ignore INTO questions VALUES (110, '¿Dónde nació?', 'En una isla');
INSERT or ignore INTO questions VALUES (111, '¿De qué color es la casa en la que vive?', 'Blanca');
INSERT or ignore INTO questions VALUES (112, '¿En dónde está la casa?', 'En una montaña');
INSERT or ignore INTO questions VALUES (113, '¿Qué animal tiene su casa?', 'Un perro');
INSERT or ignore INTO questions VALUES (114, '¿De qué color es el animal?', 'Negro');
INSERT or ignore INTO questions VALUES (115, '¿A qué le gusta jugar?', 'A las cartas');
INSERT or ignore INTO questions VALUES (116, '¿Qué clima no le gusta?', 'La lluvia');
INSERT or ignore INTO questions VALUES (117, '¿A qué se dedicaba su padre?', 'Cocinero');
INSERT or ignore INTO questions VALUES (118, '1', NULL);
INSERT or ignore INTO questions VALUES (119, '2', NULL);
INSERT or ignore INTO questions VALUES (120, '3', NULL);
INSERT or ignore INTO questions VALUES (121, '4', NULL);
INSERT or ignore INTO questions VALUES (122, '5', NULL);
INSERT or ignore INTO questions VALUES (123, '6', NULL);
INSERT or ignore INTO questions VALUES (124, '7', NULL);
INSERT or ignore INTO questions VALUES (125, '8', NULL);
INSERT or ignore INTO questions VALUES (126, '9', NULL);
INSERT or ignore INTO questions VALUES (127, '10', NULL);
INSERT or ignore INTO questions VALUES (128, '11', NULL);
INSERT or ignore INTO questions VALUES (129, '12', NULL);
INSERT or ignore INTO questions VALUES (130, '13', NULL);
INSERT or ignore INTO questions VALUES (131, '14', NULL);
INSERT or ignore INTO questions VALUES (132, '15', NULL);
INSERT or ignore INTO questions VALUES (133, '16', NULL);
INSERT or ignore INTO questions VALUES (134, '17', NULL);
INSERT or ignore INTO questions VALUES (135, '18', NULL);
INSERT or ignore INTO questions VALUES (136, '19', NULL);
INSERT or ignore INTO questions VALUES (137, '20', NULL);
INSERT or ignore INTO questions VALUES (138, '21', NULL);
INSERT or ignore INTO questions VALUES (139, '22', NULL);
INSERT or ignore INTO questions VALUES (140, '23', NULL);
INSERT or ignore INTO questions VALUES (141, '24', NULL);
INSERT or ignore INTO questions VALUES (5, '3', 'Cuello');
INSERT or ignore INTO questions VALUES (6, '4', 'Nube');
INSERT or ignore INTO questions VALUES (7, '5', 'Pie');
INSERT or ignore INTO questions VALUES (8, '6', 'Semilla');
INSERT or ignore INTO questions VALUES (9, '7', 'Lengua');
INSERT or ignore INTO questions VALUES (10, '8', 'Arena');
INSERT or ignore INTO questions VALUES (11, '9', 'Fuego');
INSERT or ignore INTO questions VALUES (12, '10', 'Nariz');
INSERT or ignore INTO questions VALUES (13, '11', 'Árbol');
INSERT or ignore INTO questions VALUES (3, '1', 'Piedra');
INSERT or ignore INTO questions VALUES (14, '12', 'Mano');
INSERT or ignore INTO questions VALUES (15, '13', 'Viento');
INSERT or ignore INTO questions VALUES (106, '¿Cuál es el sexo de sus hijos?', '3 muj /1 varon');
INSERT or ignore INTO questions VALUES (62, 'Mano: Sector 22', '1');
INSERT or ignore INTO questions VALUES (93, 'Cantidad de cuadras recorridas', NULL);
INSERT or ignore INTO questions VALUES (102, '¿Cómo se llama la persona de esta foto?', 'Acierto?');
INSERT or ignore INTO questions VALUES (142, '25', NULL);
INSERT or ignore INTO questions VALUES (143, '26', NULL);
INSERT or ignore INTO questions VALUES (144, '27', NULL);
INSERT or ignore INTO questions VALUES (145, '28', NULL);
INSERT or ignore INTO questions VALUES (146, '29', NULL);
INSERT or ignore INTO questions VALUES (147, '30', NULL);
INSERT or ignore INTO questions VALUES (148, '31', NULL);
INSERT or ignore INTO questions VALUES (149, '32', NULL);
INSERT or ignore INTO questions VALUES (150, '33', NULL);
INSERT or ignore INTO questions VALUES (151, '34', NULL);
INSERT or ignore INTO questions VALUES (152, '35', NULL);
INSERT or ignore INTO questions VALUES (153, '36', NULL);
INSERT or ignore INTO questions VALUES (154, '37', NULL);
INSERT or ignore INTO questions VALUES (155, '38', NULL);
INSERT or ignore INTO questions VALUES (156, '39', NULL);
INSERT or ignore INTO questions VALUES (157, '40', NULL);
INSERT or ignore INTO questions VALUES (158, '41', NULL);
INSERT or ignore INTO questions VALUES (159, '42', NULL);
INSERT or ignore INTO questions VALUES (160, '43', NULL);
INSERT or ignore INTO questions VALUES (161, '44', NULL);
INSERT or ignore INTO questions VALUES (162, '45', NULL);
INSERT or ignore INTO questions VALUES (163, '46', NULL);
INSERT or ignore INTO questions VALUES (164, '47', NULL);
INSERT or ignore INTO questions VALUES (165, '48', NULL);
INSERT or ignore INTO questions VALUES (166, '49', NULL);
INSERT or ignore INTO questions VALUES (167, '50', NULL);
INSERT or ignore INTO questions VALUES (168, '51', NULL);
INSERT or ignore INTO questions VALUES (169, '52', NULL);
INSERT or ignore INTO questions VALUES (170, '53', NULL);
INSERT or ignore INTO questions VALUES (171, '54', NULL);
INSERT or ignore INTO questions VALUES (172, '55', NULL);
INSERT or ignore INTO questions VALUES (173, '56', NULL);
INSERT or ignore INTO questions VALUES (174, '57', NULL);
INSERT or ignore INTO questions VALUES (175, '58', NULL);
INSERT or ignore INTO questions VALUES (176, '59', NULL);
INSERT or ignore INTO questions VALUES (177, '60', NULL);
INSERT or ignore INTO questions VALUES (178, '', '');
INSERT or ignore INTO questions VALUES (180, 'ARENA', 'Arena');
INSERT or ignore INTO questions VALUES (181, 'Agua *', '0');
INSERT or ignore INTO questions VALUES (182, 'Redondo', '0');
INSERT or ignore INTO questions VALUES (183, 'Frío', '0');
INSERT or ignore INTO questions VALUES (184, 'Pájaro', '0');
INSERT or ignore INTO questions VALUES (185, 'MANO', 'Mano');
INSERT or ignore INTO questions VALUES (186, 'Montaña *', '0');
INSERT or ignore INTO questions VALUES (187, 'Cabeza *', '0');
INSERT or ignore INTO questions VALUES (188, 'OREJA', 'Oreja');
INSERT or ignore INTO questions VALUES (189, 'PIE', 'Pie');
INSERT or ignore INTO questions VALUES (190, 'Noche', '0');
INSERT or ignore INTO questions VALUES (191, 'Camino', '0');
INSERT or ignore INTO questions VALUES (192, 'Pluma', '0');
INSERT or ignore INTO questions VALUES (193, 'VIENTO', 'Viento');
INSERT or ignore INTO questions VALUES (194, 'NARIZ', 'Nariz');
INSERT or ignore INTO questions VALUES (195, 'FUEGO', 'Fuego');
INSERT or ignore INTO questions VALUES (196, 'SEMILLA', 'Semilla');
INSERT or ignore INTO questions VALUES (197, 'Ojo *', '0');
INSERT or ignore INTO questions VALUES (198, 'Pierna *', '0');
INSERT or ignore INTO questions VALUES (199, 'RODILLA', 'Rodilla');
INSERT or ignore INTO questions VALUES (200, 'Cielo *', '0');
INSERT or ignore INTO questions VALUES (201, 'NUBE', 'Nube');
INSERT or ignore INTO questions VALUES (202, 'PIEDRA', 'Piedra');
INSERT or ignore INTO questions VALUES (203, 'Rama *', '0');
INSERT or ignore INTO questions VALUES (204, 'LENGUA', 'Lengua');
INSERT or ignore INTO questions VALUES (205, 'CUELLO', 'Cuello');
INSERT or ignore INTO questions VALUES (206, 'Perro', '0');
INSERT or ignore INTO questions VALUES (207, 'ÁRBOL', 'Árbol');
INSERT or ignore INTO questions VALUES (208, 'Mano: Sector 24', '0');


--
-- Data for Name: questions_x_versions; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--

INSERT or ignore INTO questions_x_versions VALUES (1, 1, 1, NULL);
INSERT or ignore INTO questions_x_versions VALUES (2, 1, 2, NULL);
INSERT or ignore INTO questions_x_versions VALUES (3, 1, 3, NULL);
INSERT or ignore INTO questions_x_versions VALUES (4, 1, 4, NULL);
INSERT or ignore INTO questions_x_versions VALUES (5, 1, 5, NULL);
INSERT or ignore INTO questions_x_versions VALUES (6, 1, 6, NULL);
INSERT or ignore INTO questions_x_versions VALUES (7, 1, 7, NULL);
INSERT or ignore INTO questions_x_versions VALUES (8, 1, 8, NULL);
INSERT or ignore INTO questions_x_versions VALUES (9, 1, 9, NULL);
INSERT or ignore INTO questions_x_versions VALUES (10, 1, 10, NULL);
INSERT or ignore INTO questions_x_versions VALUES (3, 2, 168, NULL);
INSERT or ignore INTO questions_x_versions VALUES (2, 2, 167, NULL);
INSERT or ignore INTO questions_x_versions VALUES (17, 1, 11, NULL);
INSERT or ignore INTO questions_x_versions VALUES (18, 1, 12, NULL);
INSERT or ignore INTO questions_x_versions VALUES (19, 1, 13, NULL);
INSERT or ignore INTO questions_x_versions VALUES (20, 1, 14, NULL);
INSERT or ignore INTO questions_x_versions VALUES (21, 1, 15, NULL);
INSERT or ignore INTO questions_x_versions VALUES (22, 1, 16, NULL);
INSERT or ignore INTO questions_x_versions VALUES (23, 1, 17, NULL);
INSERT or ignore INTO questions_x_versions VALUES (24, 1, 18, NULL);
INSERT or ignore INTO questions_x_versions VALUES (25, 1, 19, NULL);
INSERT or ignore INTO questions_x_versions VALUES (26, 1, 20, NULL);
INSERT or ignore INTO questions_x_versions VALUES (27, 1, 21, NULL);
INSERT or ignore INTO questions_x_versions VALUES (28, 1, 22, NULL);
INSERT or ignore INTO questions_x_versions VALUES (29, 1, 23, NULL);
INSERT or ignore INTO questions_x_versions VALUES (30, 1, 24, NULL);
INSERT or ignore INTO questions_x_versions VALUES (31, 1, 25, NULL);
INSERT or ignore INTO questions_x_versions VALUES (32, 1, 26, NULL);
INSERT or ignore INTO questions_x_versions VALUES (33, 1, 27, NULL);
INSERT or ignore INTO questions_x_versions VALUES (34, 1, 28, NULL);
INSERT or ignore INTO questions_x_versions VALUES (35, 1, 29, NULL);
INSERT or ignore INTO questions_x_versions VALUES (37, 1, 30, NULL);
INSERT or ignore INTO questions_x_versions VALUES (38, 1, 31, NULL);
INSERT or ignore INTO questions_x_versions VALUES (39, 1, 32, NULL);
INSERT or ignore INTO questions_x_versions VALUES (40, 1, 33, NULL);
INSERT or ignore INTO questions_x_versions VALUES (41, 1, 34, NULL);
INSERT or ignore INTO questions_x_versions VALUES (42, 1, 35, NULL);
INSERT or ignore INTO questions_x_versions VALUES (43, 1, 36, NULL);
INSERT or ignore INTO questions_x_versions VALUES (44, 1, 37, NULL);
INSERT or ignore INTO questions_x_versions VALUES (45, 1, 38, NULL);
INSERT or ignore INTO questions_x_versions VALUES (46, 1, 39, NULL);
INSERT or ignore INTO questions_x_versions VALUES (47, 1, 40, NULL);
INSERT or ignore INTO questions_x_versions VALUES (48, 1, 41, NULL);
INSERT or ignore INTO questions_x_versions VALUES (49, 1, 42, NULL);
INSERT or ignore INTO questions_x_versions VALUES (50, 1, 43, NULL);
INSERT or ignore INTO questions_x_versions VALUES (51, 1, 44, NULL);
INSERT or ignore INTO questions_x_versions VALUES (53, 1, 45, NULL);
INSERT or ignore INTO questions_x_versions VALUES (54, 1, 46, NULL);
INSERT or ignore INTO questions_x_versions VALUES (55, 1, 47, NULL);
INSERT or ignore INTO questions_x_versions VALUES (56, 1, 48, NULL);
INSERT or ignore INTO questions_x_versions VALUES (57, 1, 49, NULL);
INSERT or ignore INTO questions_x_versions VALUES (58, 1, 50, NULL);
INSERT or ignore INTO questions_x_versions VALUES (59, 1, 51, NULL);
INSERT or ignore INTO questions_x_versions VALUES (60, 1, 52, NULL);
INSERT or ignore INTO questions_x_versions VALUES (61, 1, 53, NULL);
INSERT or ignore INTO questions_x_versions VALUES (62, 1, 54, NULL);
INSERT or ignore INTO questions_x_versions VALUES (63, 1, 55, NULL);
INSERT or ignore INTO questions_x_versions VALUES (1, 2, 166, NULL);
INSERT or ignore INTO questions_x_versions VALUES (154, 1, 140, NULL);
INSERT or ignore INTO questions_x_versions VALUES (153, 1, 139, NULL);
INSERT or ignore INTO questions_x_versions VALUES (152, 1, 138, NULL);
INSERT or ignore INTO questions_x_versions VALUES (151, 1, 137, NULL);
INSERT or ignore INTO questions_x_versions VALUES (150, 1, 136, NULL);
INSERT or ignore INTO questions_x_versions VALUES (149, 1, 135, NULL);
INSERT or ignore INTO questions_x_versions VALUES (148, 1, 134, NULL);
INSERT or ignore INTO questions_x_versions VALUES (147, 1, 133, NULL);
INSERT or ignore INTO questions_x_versions VALUES (146, 1, 132, NULL);
INSERT or ignore INTO questions_x_versions VALUES (145, 1, 131, NULL);
INSERT or ignore INTO questions_x_versions VALUES (144, 1, 130, NULL);
INSERT or ignore INTO questions_x_versions VALUES (143, 1, 129, NULL);
INSERT or ignore INTO questions_x_versions VALUES (142, 1, 128, NULL);
INSERT or ignore INTO questions_x_versions VALUES (141, 1, 127, NULL);
INSERT or ignore INTO questions_x_versions VALUES (140, 1, 126, NULL);
INSERT or ignore INTO questions_x_versions VALUES (139, 1, 125, NULL);
INSERT or ignore INTO questions_x_versions VALUES (177, 1, 163, NULL);
INSERT or ignore INTO questions_x_versions VALUES (138, 1, 124, NULL);
INSERT or ignore INTO questions_x_versions VALUES (137, 1, 123, NULL);
INSERT or ignore INTO questions_x_versions VALUES (136, 1, 122, NULL);
INSERT or ignore INTO questions_x_versions VALUES (135, 1, 121, NULL);
INSERT or ignore INTO questions_x_versions VALUES (134, 1, 120, NULL);
INSERT or ignore INTO questions_x_versions VALUES (133, 1, 119, NULL);
INSERT or ignore INTO questions_x_versions VALUES (132, 1, 118, NULL);
INSERT or ignore INTO questions_x_versions VALUES (176, 1, 162, NULL);
INSERT or ignore INTO questions_x_versions VALUES (175, 1, 161, NULL);
INSERT or ignore INTO questions_x_versions VALUES (174, 1, 160, NULL);
INSERT or ignore INTO questions_x_versions VALUES (173, 1, 159, NULL);
INSERT or ignore INTO questions_x_versions VALUES (172, 1, 158, NULL);
INSERT or ignore INTO questions_x_versions VALUES (171, 1, 157, NULL);
INSERT or ignore INTO questions_x_versions VALUES (170, 1, 156, NULL);
INSERT or ignore INTO questions_x_versions VALUES (169, 1, 155, NULL);
INSERT or ignore INTO questions_x_versions VALUES (168, 1, 154, NULL);
INSERT or ignore INTO questions_x_versions VALUES (167, 1, 153, NULL);
INSERT or ignore INTO questions_x_versions VALUES (166, 1, 152, NULL);
INSERT or ignore INTO questions_x_versions VALUES (165, 1, 151, NULL);
INSERT or ignore INTO questions_x_versions VALUES (164, 1, 150, NULL);
INSERT or ignore INTO questions_x_versions VALUES (163, 1, 149, NULL);
INSERT or ignore INTO questions_x_versions VALUES (162, 1, 148, NULL);
INSERT or ignore INTO questions_x_versions VALUES (161, 1, 147, NULL);
INSERT or ignore INTO questions_x_versions VALUES (160, 1, 146, NULL);
INSERT or ignore INTO questions_x_versions VALUES (159, 1, 145, NULL);
INSERT or ignore INTO questions_x_versions VALUES (158, 1, 144, NULL);
INSERT or ignore INTO questions_x_versions VALUES (157, 1, 143, NULL);
INSERT or ignore INTO questions_x_versions VALUES (131, 1, 117, NULL);
INSERT or ignore INTO questions_x_versions VALUES (156, 1, 142, NULL);
INSERT or ignore INTO questions_x_versions VALUES (155, 1, 141, NULL);
INSERT or ignore INTO questions_x_versions VALUES (130, 1, 116, NULL);
INSERT or ignore INTO questions_x_versions VALUES (129, 1, 115, NULL);
INSERT or ignore INTO questions_x_versions VALUES (128, 1, 114, NULL);
INSERT or ignore INTO questions_x_versions VALUES (127, 1, 113, NULL);
INSERT or ignore INTO questions_x_versions VALUES (126, 1, 112, NULL);
INSERT or ignore INTO questions_x_versions VALUES (125, 1, 111, NULL);
INSERT or ignore INTO questions_x_versions VALUES (124, 1, 110, NULL);
INSERT or ignore INTO questions_x_versions VALUES (123, 1, 109, NULL);
INSERT or ignore INTO questions_x_versions VALUES (122, 1, 108, NULL);
INSERT or ignore INTO questions_x_versions VALUES (121, 1, 107, NULL);
INSERT or ignore INTO questions_x_versions VALUES (120, 1, 106, NULL);
INSERT or ignore INTO questions_x_versions VALUES (119, 1, 105, NULL);
INSERT or ignore INTO questions_x_versions VALUES (118, 1, 104, NULL);
INSERT or ignore INTO questions_x_versions VALUES (112, 1, 103, NULL);
INSERT or ignore INTO questions_x_versions VALUES (111, 1, 102, NULL);
INSERT or ignore INTO questions_x_versions VALUES (110, 1, 101, NULL);
INSERT or ignore INTO questions_x_versions VALUES (109, 1, 100, NULL);
INSERT or ignore INTO questions_x_versions VALUES (108, 1, 99, NULL);
INSERT or ignore INTO questions_x_versions VALUES (107, 1, 98, NULL);
INSERT or ignore INTO questions_x_versions VALUES (106, 1, 97, NULL);
INSERT or ignore INTO questions_x_versions VALUES (105, 1, 96, NULL);
INSERT or ignore INTO questions_x_versions VALUES (104, 1, 95, NULL);
INSERT or ignore INTO questions_x_versions VALUES (103, 1, 94, NULL);
INSERT or ignore INTO questions_x_versions VALUES (102, 1, 93, NULL);
INSERT or ignore INTO questions_x_versions VALUES (99, 1, 92, NULL);
INSERT or ignore INTO questions_x_versions VALUES (98, 1, 91, NULL);
INSERT or ignore INTO questions_x_versions VALUES (97, 1, 90, NULL);
INSERT or ignore INTO questions_x_versions VALUES (96, 1, 89, NULL);
INSERT or ignore INTO questions_x_versions VALUES (95, 1, 88, NULL);
INSERT or ignore INTO questions_x_versions VALUES (94, 1, 87, NULL);
INSERT or ignore INTO questions_x_versions VALUES (93, 1, 86, NULL);
INSERT or ignore INTO questions_x_versions VALUES (92, 1, 85, NULL);
INSERT or ignore INTO questions_x_versions VALUES (91, 1, 84, NULL);
INSERT or ignore INTO questions_x_versions VALUES (90, 1, 83, NULL);
INSERT or ignore INTO questions_x_versions VALUES (89, 1, 82, NULL);
INSERT or ignore INTO questions_x_versions VALUES (88, 1, 81, NULL);
INSERT or ignore INTO questions_x_versions VALUES (87, 1, 80, NULL);
INSERT or ignore INTO questions_x_versions VALUES (86, 1, 79, NULL);
INSERT or ignore INTO questions_x_versions VALUES (85, 1, 78, NULL);
INSERT or ignore INTO questions_x_versions VALUES (84, 1, 77, NULL);
INSERT or ignore INTO questions_x_versions VALUES (83, 1, 76, NULL);
INSERT or ignore INTO questions_x_versions VALUES (82, 1, 75, NULL);
INSERT or ignore INTO questions_x_versions VALUES (81, 1, 74, NULL);
INSERT or ignore INTO questions_x_versions VALUES (80, 1, 73, NULL);
INSERT or ignore INTO questions_x_versions VALUES (79, 1, 72, NULL);
INSERT or ignore INTO questions_x_versions VALUES (78, 1, 71, NULL);
INSERT or ignore INTO questions_x_versions VALUES (77, 1, 70, NULL);
INSERT or ignore INTO questions_x_versions VALUES (76, 1, 69, NULL);
INSERT or ignore INTO questions_x_versions VALUES (75, 1, 68, NULL);
INSERT or ignore INTO questions_x_versions VALUES (74, 1, 67, NULL);
INSERT or ignore INTO questions_x_versions VALUES (73, 1, 66, NULL);
INSERT or ignore INTO questions_x_versions VALUES (72, 1, 65, NULL);
INSERT or ignore INTO questions_x_versions VALUES (71, 1, 64, NULL);
INSERT or ignore INTO questions_x_versions VALUES (70, 1, 63, NULL);
INSERT or ignore INTO questions_x_versions VALUES (69, 1, 62, NULL);
INSERT or ignore INTO questions_x_versions VALUES (68, 1, 61, NULL);
INSERT or ignore INTO questions_x_versions VALUES (67, 1, 60, NULL);
INSERT or ignore INTO questions_x_versions VALUES (66, 1, 59, NULL);
INSERT or ignore INTO questions_x_versions VALUES (65, 1, 58, NULL);
INSERT or ignore INTO questions_x_versions VALUES (64, 1, 57, NULL);
INSERT or ignore INTO questions_x_versions VALUES (122, 2, 287, NULL);
INSERT or ignore INTO questions_x_versions VALUES (121, 2, 286, NULL);
INSERT or ignore INTO questions_x_versions VALUES (120, 2, 285, NULL);
INSERT or ignore INTO questions_x_versions VALUES (119, 2, 284, NULL);
INSERT or ignore INTO questions_x_versions VALUES (118, 2, 283, NULL);
INSERT or ignore INTO questions_x_versions VALUES (116, 2, 281, NULL);
INSERT or ignore INTO questions_x_versions VALUES (117, 2, 282, NULL);
INSERT or ignore INTO questions_x_versions VALUES (115, 2, 280, NULL);
INSERT or ignore INTO questions_x_versions VALUES (114, 2, 279, NULL);
INSERT or ignore INTO questions_x_versions VALUES (113, 2, 278, NULL);
INSERT or ignore INTO questions_x_versions VALUES (112, 2, 277, NULL);
INSERT or ignore INTO questions_x_versions VALUES (111, 2, 276, NULL);
INSERT or ignore INTO questions_x_versions VALUES (110, 2, 275, NULL);
INSERT or ignore INTO questions_x_versions VALUES (109, 2, 274, NULL);
INSERT or ignore INTO questions_x_versions VALUES (108, 2, 273, NULL);
INSERT or ignore INTO questions_x_versions VALUES (107, 2, 272, NULL);
INSERT or ignore INTO questions_x_versions VALUES (106, 2, 271, NULL);
INSERT or ignore INTO questions_x_versions VALUES (105, 2, 270, NULL);
INSERT or ignore INTO questions_x_versions VALUES (104, 2, 269, NULL);
INSERT or ignore INTO questions_x_versions VALUES (103, 2, 268, NULL);
INSERT or ignore INTO questions_x_versions VALUES (102, 2, 267, NULL);
INSERT or ignore INTO questions_x_versions VALUES (99, 2, 265, NULL);
INSERT or ignore INTO questions_x_versions VALUES (98, 2, 264, NULL);
INSERT or ignore INTO questions_x_versions VALUES (97, 2, 263, NULL);
INSERT or ignore INTO questions_x_versions VALUES (96, 2, 262, NULL);
INSERT or ignore INTO questions_x_versions VALUES (95, 2, 261, NULL);
INSERT or ignore INTO questions_x_versions VALUES (94, 2, 260, NULL);
INSERT or ignore INTO questions_x_versions VALUES (93, 2, 259, NULL);
INSERT or ignore INTO questions_x_versions VALUES (92, 2, 258, NULL);
INSERT or ignore INTO questions_x_versions VALUES (91, 2, 257, NULL);
INSERT or ignore INTO questions_x_versions VALUES (90, 2, 256, NULL);
INSERT or ignore INTO questions_x_versions VALUES (89, 2, 255, NULL);
INSERT or ignore INTO questions_x_versions VALUES (88, 2, 254, NULL);
INSERT or ignore INTO questions_x_versions VALUES (87, 2, 253, NULL);
INSERT or ignore INTO questions_x_versions VALUES (86, 2, 252, NULL);
INSERT or ignore INTO questions_x_versions VALUES (85, 2, 251, NULL);
INSERT or ignore INTO questions_x_versions VALUES (84, 2, 250, NULL);
INSERT or ignore INTO questions_x_versions VALUES (83, 2, 249, NULL);
INSERT or ignore INTO questions_x_versions VALUES (82, 2, 248, NULL);
INSERT or ignore INTO questions_x_versions VALUES (81, 2, 247, NULL);
INSERT or ignore INTO questions_x_versions VALUES (80, 2, 246, NULL);
INSERT or ignore INTO questions_x_versions VALUES (79, 2, 245, NULL);
INSERT or ignore INTO questions_x_versions VALUES (78, 2, 244, NULL);
INSERT or ignore INTO questions_x_versions VALUES (77, 2, 243, NULL);
INSERT or ignore INTO questions_x_versions VALUES (76, 2, 242, NULL);
INSERT or ignore INTO questions_x_versions VALUES (75, 2, 241, NULL);
INSERT or ignore INTO questions_x_versions VALUES (74, 2, 240, NULL);
INSERT or ignore INTO questions_x_versions VALUES (73, 2, 239, NULL);
INSERT or ignore INTO questions_x_versions VALUES (72, 2, 238, NULL);
INSERT or ignore INTO questions_x_versions VALUES (71, 2, 237, NULL);
INSERT or ignore INTO questions_x_versions VALUES (70, 2, 236, NULL);
INSERT or ignore INTO questions_x_versions VALUES (69, 2, 235, NULL);
INSERT or ignore INTO questions_x_versions VALUES (68, 2, 234, NULL);
INSERT or ignore INTO questions_x_versions VALUES (67, 2, 233, NULL);
INSERT or ignore INTO questions_x_versions VALUES (66, 2, 232, NULL);
INSERT or ignore INTO questions_x_versions VALUES (65, 2, 231, NULL);
INSERT or ignore INTO questions_x_versions VALUES (64, 2, 230, NULL);
INSERT or ignore INTO questions_x_versions VALUES (63, 2, 228, NULL);
INSERT or ignore INTO questions_x_versions VALUES (62, 2, 227, NULL);
INSERT or ignore INTO questions_x_versions VALUES (61, 2, 226, NULL);
INSERT or ignore INTO questions_x_versions VALUES (60, 2, 225, NULL);
INSERT or ignore INTO questions_x_versions VALUES (59, 2, 224, NULL);
INSERT or ignore INTO questions_x_versions VALUES (58, 2, 223, NULL);
INSERT or ignore INTO questions_x_versions VALUES (57, 2, 222, NULL);
INSERT or ignore INTO questions_x_versions VALUES (56, 2, 221, NULL);
INSERT or ignore INTO questions_x_versions VALUES (174, 2, 340, NULL);
INSERT or ignore INTO questions_x_versions VALUES (173, 2, 339, NULL);
INSERT or ignore INTO questions_x_versions VALUES (172, 2, 338, NULL);
INSERT or ignore INTO questions_x_versions VALUES (171, 2, 337, NULL);
INSERT or ignore INTO questions_x_versions VALUES (170, 2, 336, NULL);
INSERT or ignore INTO questions_x_versions VALUES (169, 2, 335, NULL);
INSERT or ignore INTO questions_x_versions VALUES (168, 2, 334, NULL);
INSERT or ignore INTO questions_x_versions VALUES (167, 2, 332, NULL);
INSERT or ignore INTO questions_x_versions VALUES (166, 2, 331, NULL);
INSERT or ignore INTO questions_x_versions VALUES (165, 2, 330, NULL);
INSERT or ignore INTO questions_x_versions VALUES (164, 2, 329, NULL);
INSERT or ignore INTO questions_x_versions VALUES (163, 2, 328, NULL);
INSERT or ignore INTO questions_x_versions VALUES (162, 2, 327, NULL);
INSERT or ignore INTO questions_x_versions VALUES (161, 2, 326, NULL);
INSERT or ignore INTO questions_x_versions VALUES (160, 2, 325, NULL);
INSERT or ignore INTO questions_x_versions VALUES (159, 2, 324, NULL);
INSERT or ignore INTO questions_x_versions VALUES (158, 2, 323, NULL);
INSERT or ignore INTO questions_x_versions VALUES (157, 2, 322, NULL);
INSERT or ignore INTO questions_x_versions VALUES (156, 2, 321, NULL);
INSERT or ignore INTO questions_x_versions VALUES (155, 2, 320, NULL);
INSERT or ignore INTO questions_x_versions VALUES (154, 2, 319, NULL);
INSERT or ignore INTO questions_x_versions VALUES (153, 2, 318, NULL);
INSERT or ignore INTO questions_x_versions VALUES (152, 2, 317, NULL);
INSERT or ignore INTO questions_x_versions VALUES (151, 2, 316, NULL);
INSERT or ignore INTO questions_x_versions VALUES (150, 2, 315, NULL);
INSERT or ignore INTO questions_x_versions VALUES (149, 2, 314, NULL);
INSERT or ignore INTO questions_x_versions VALUES (148, 2, 313, NULL);
INSERT or ignore INTO questions_x_versions VALUES (147, 2, 312, NULL);
INSERT or ignore INTO questions_x_versions VALUES (146, 2, 311, NULL);
INSERT or ignore INTO questions_x_versions VALUES (145, 2, 310, NULL);
INSERT or ignore INTO questions_x_versions VALUES (144, 2, 309, NULL);
INSERT or ignore INTO questions_x_versions VALUES (143, 2, 308, NULL);
INSERT or ignore INTO questions_x_versions VALUES (142, 2, 307, NULL);
INSERT or ignore INTO questions_x_versions VALUES (141, 2, 306, NULL);
INSERT or ignore INTO questions_x_versions VALUES (140, 2, 305, NULL);
INSERT or ignore INTO questions_x_versions VALUES (139, 2, 304, NULL);
INSERT or ignore INTO questions_x_versions VALUES (138, 2, 303, NULL);
INSERT or ignore INTO questions_x_versions VALUES (137, 2, 302, NULL);
INSERT or ignore INTO questions_x_versions VALUES (136, 2, 301, NULL);
INSERT or ignore INTO questions_x_versions VALUES (135, 2, 300, NULL);
INSERT or ignore INTO questions_x_versions VALUES (134, 2, 299, NULL);
INSERT or ignore INTO questions_x_versions VALUES (133, 2, 298, NULL);
INSERT or ignore INTO questions_x_versions VALUES (132, 2, 297, NULL);
INSERT or ignore INTO questions_x_versions VALUES (131, 2, 296, NULL);
INSERT or ignore INTO questions_x_versions VALUES (130, 2, 295, NULL);
INSERT or ignore INTO questions_x_versions VALUES (129, 2, 294, NULL);
INSERT or ignore INTO questions_x_versions VALUES (128, 2, 293, NULL);
INSERT or ignore INTO questions_x_versions VALUES (127, 2, 292, NULL);
INSERT or ignore INTO questions_x_versions VALUES (126, 2, 291, NULL);
INSERT or ignore INTO questions_x_versions VALUES (125, 2, 290, NULL);
INSERT or ignore INTO questions_x_versions VALUES (124, 2, 289, NULL);
INSERT or ignore INTO questions_x_versions VALUES (123, 2, 288, NULL);
INSERT or ignore INTO questions_x_versions VALUES (55, 2, 220, NULL);
INSERT or ignore INTO questions_x_versions VALUES (54, 2, 219, NULL);
INSERT or ignore INTO questions_x_versions VALUES (53, 2, 218, NULL);
INSERT or ignore INTO questions_x_versions VALUES (52, 2, 217, NULL);
INSERT or ignore INTO questions_x_versions VALUES (51, 2, 216, NULL);
INSERT or ignore INTO questions_x_versions VALUES (50, 2, 215, NULL);
INSERT or ignore INTO questions_x_versions VALUES (49, 2, 214, NULL);
INSERT or ignore INTO questions_x_versions VALUES (48, 2, 213, NULL);
INSERT or ignore INTO questions_x_versions VALUES (47, 2, 212, NULL);
INSERT or ignore INTO questions_x_versions VALUES (46, 2, 211, NULL);
INSERT or ignore INTO questions_x_versions VALUES (45, 2, 210, NULL);
INSERT or ignore INTO questions_x_versions VALUES (44, 2, 209, NULL);
INSERT or ignore INTO questions_x_versions VALUES (43, 2, 208, NULL);
INSERT or ignore INTO questions_x_versions VALUES (42, 2, 207, NULL);
INSERT or ignore INTO questions_x_versions VALUES (41, 2, 206, NULL);
INSERT or ignore INTO questions_x_versions VALUES (40, 2, 205, NULL);
INSERT or ignore INTO questions_x_versions VALUES (39, 2, 204, NULL);
INSERT or ignore INTO questions_x_versions VALUES (38, 2, 203, NULL);
INSERT or ignore INTO questions_x_versions VALUES (37, 2, 202, NULL);
INSERT or ignore INTO questions_x_versions VALUES (36, 2, 201, NULL);
INSERT or ignore INTO questions_x_versions VALUES (35, 2, 200, NULL);
INSERT or ignore INTO questions_x_versions VALUES (34, 2, 199, NULL);
INSERT or ignore INTO questions_x_versions VALUES (33, 2, 198, NULL);
INSERT or ignore INTO questions_x_versions VALUES (32, 2, 197, NULL);
INSERT or ignore INTO questions_x_versions VALUES (31, 2, 196, NULL);
INSERT or ignore INTO questions_x_versions VALUES (30, 2, 195, NULL);
INSERT or ignore INTO questions_x_versions VALUES (29, 2, 194, NULL);
INSERT or ignore INTO questions_x_versions VALUES (28, 2, 193, NULL);
INSERT or ignore INTO questions_x_versions VALUES (27, 2, 192, NULL);
INSERT or ignore INTO questions_x_versions VALUES (26, 2, 191, NULL);
INSERT or ignore INTO questions_x_versions VALUES (25, 2, 190, NULL);
INSERT or ignore INTO questions_x_versions VALUES (24, 2, 189, NULL);
INSERT or ignore INTO questions_x_versions VALUES (23, 2, 188, NULL);
INSERT or ignore INTO questions_x_versions VALUES (22, 2, 187, NULL);
INSERT or ignore INTO questions_x_versions VALUES (21, 2, 186, NULL);
INSERT or ignore INTO questions_x_versions VALUES (20, 2, 185, NULL);
INSERT or ignore INTO questions_x_versions VALUES (19, 2, 184, NULL);
INSERT or ignore INTO questions_x_versions VALUES (18, 2, 183, NULL);
INSERT or ignore INTO questions_x_versions VALUES (17, 2, 182, NULL);
INSERT or ignore INTO questions_x_versions VALUES (16, 2, 181, NULL);
INSERT or ignore INTO questions_x_versions VALUES (15, 2, 180, NULL);
INSERT or ignore INTO questions_x_versions VALUES (14, 2, 179, NULL);
INSERT or ignore INTO questions_x_versions VALUES (13, 2, 178, NULL);
INSERT or ignore INTO questions_x_versions VALUES (12, 2, 177, NULL);
INSERT or ignore INTO questions_x_versions VALUES (11, 2, 176, NULL);
INSERT or ignore INTO questions_x_versions VALUES (10, 2, 175, NULL);
INSERT or ignore INTO questions_x_versions VALUES (9, 2, 174, NULL);
INSERT or ignore INTO questions_x_versions VALUES (8, 2, 173, NULL);
INSERT or ignore INTO questions_x_versions VALUES (7, 2, 172, NULL);
INSERT or ignore INTO questions_x_versions VALUES (6, 2, 171, NULL);
INSERT or ignore INTO questions_x_versions VALUES (5, 2, 170, NULL);
INSERT or ignore INTO questions_x_versions VALUES (4, 2, 169, NULL);
INSERT or ignore INTO questions_x_versions VALUES (12, 1, 165, NULL);
INSERT or ignore INTO questions_x_versions VALUES (11, 1, 164, NULL);
INSERT or ignore INTO questions_x_versions VALUES (208, 2, 229, NULL);
INSERT or ignore INTO questions_x_versions VALUES (208, 1, 56, NULL);
INSERT or ignore INTO questions_x_versions VALUES (207, 2, 391, NULL);
INSERT or ignore INTO questions_x_versions VALUES (206, 2, 390, NULL);
INSERT or ignore INTO questions_x_versions VALUES (205, 2, 389, NULL);
INSERT or ignore INTO questions_x_versions VALUES (204, 2, 388, NULL);
INSERT or ignore INTO questions_x_versions VALUES (203, 2, 387, NULL);
INSERT or ignore INTO questions_x_versions VALUES (202, 2, 386, NULL);
INSERT or ignore INTO questions_x_versions VALUES (201, 2, 385, NULL);
INSERT or ignore INTO questions_x_versions VALUES (200, 2, 384, NULL);
INSERT or ignore INTO questions_x_versions VALUES (199, 2, 383, NULL);
INSERT or ignore INTO questions_x_versions VALUES (198, 2, 382, NULL);
INSERT or ignore INTO questions_x_versions VALUES (197, 2, 381, NULL);
INSERT or ignore INTO questions_x_versions VALUES (196, 2, 380, NULL);
INSERT or ignore INTO questions_x_versions VALUES (195, 2, 379, NULL);
INSERT or ignore INTO questions_x_versions VALUES (194, 2, 378, NULL);
INSERT or ignore INTO questions_x_versions VALUES (193, 2, 377, NULL);
INSERT or ignore INTO questions_x_versions VALUES (192, 2, 376, NULL);
INSERT or ignore INTO questions_x_versions VALUES (191, 2, 375, NULL);
INSERT or ignore INTO questions_x_versions VALUES (190, 2, 374, NULL);
INSERT or ignore INTO questions_x_versions VALUES (189, 2, 373, NULL);
INSERT or ignore INTO questions_x_versions VALUES (188, 2, 372, NULL);
INSERT or ignore INTO questions_x_versions VALUES (187, 2, 371, NULL);
INSERT or ignore INTO questions_x_versions VALUES (186, 2, 370, NULL);
INSERT or ignore INTO questions_x_versions VALUES (185, 2, 369, NULL);
INSERT or ignore INTO questions_x_versions VALUES (184, 2, 368, NULL);
INSERT or ignore INTO questions_x_versions VALUES (183, 2, 367, NULL);
INSERT or ignore INTO questions_x_versions VALUES (182, 2, 366, NULL);
INSERT or ignore INTO questions_x_versions VALUES (181, 2, 365, NULL);
INSERT or ignore INTO questions_x_versions VALUES (180, 2, 364, NULL);
INSERT or ignore INTO questions_x_versions VALUES (205, 1, 363, NULL);
INSERT or ignore INTO questions_x_versions VALUES (204, 1, 362, NULL);
INSERT or ignore INTO questions_x_versions VALUES (203, 1, 361, NULL);
INSERT or ignore INTO questions_x_versions VALUES (202, 1, 360, NULL);
INSERT or ignore INTO questions_x_versions VALUES (201, 1, 359, NULL);
INSERT or ignore INTO questions_x_versions VALUES (198, 1, 358, NULL);
INSERT or ignore INTO questions_x_versions VALUES (197, 1, 357, NULL);
INSERT or ignore INTO questions_x_versions VALUES (196, 1, 356, NULL);
INSERT or ignore INTO questions_x_versions VALUES (195, 1, 355, NULL);
INSERT or ignore INTO questions_x_versions VALUES (194, 1, 354, NULL);
INSERT or ignore INTO questions_x_versions VALUES (191, 1, 353, NULL);
INSERT or ignore INTO questions_x_versions VALUES (190, 1, 352, NULL);
INSERT or ignore INTO questions_x_versions VALUES (189, 1, 351, NULL);
INSERT or ignore INTO questions_x_versions VALUES (188, 1, 350, NULL);
INSERT or ignore INTO questions_x_versions VALUES (187, 1, 349, NULL);
INSERT or ignore INTO questions_x_versions VALUES (184, 1, 348, NULL);
INSERT or ignore INTO questions_x_versions VALUES (183, 1, 347, NULL);
INSERT or ignore INTO questions_x_versions VALUES (182, 1, 346, NULL);
INSERT or ignore INTO questions_x_versions VALUES (181, 1, 345, NULL);
INSERT or ignore INTO questions_x_versions VALUES (180, 1, 344, NULL);
INSERT or ignore INTO questions_x_versions VALUES (177, 2, 343, NULL);
INSERT or ignore INTO questions_x_versions VALUES (176, 2, 342, NULL);
INSERT or ignore INTO questions_x_versions VALUES (175, 2, 341, NULL);

--
-- Data for Name: components; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--

INSERT or ignore INTO components VALUES (63, 'TOTAL ANIMALS', 11);
INSERT or ignore INTO components VALUES (1, 'Reading Test', 1);
INSERT or ignore INTO components VALUES (2, 'WL Trial 1 score', 2);
INSERT or ignore INTO components VALUES (3, 'WL Trial 2 score', 2);
INSERT or ignore INTO components VALUES (4, 'WL Trial 3 score', 2);
INSERT or ignore INTO components VALUES (7, 'DELAYED TRIAL SCORE', 8);
INSERT or ignore INTO components VALUES (8, '% DELAYED TRIAL', 8);
INSERT or ignore INTO components VALUES (9, 'True Positives', 9);
INSERT or ignore INTO components VALUES (11, 'Unrelated false Positives', 9);
INSERT or ignore INTO components VALUES (12, 'DISCRIMINATION INDEX', 9);
INSERT or ignore INTO components VALUES (13, 'Flower Hits Immed.', 3);
INSERT or ignore INTO components VALUES (14, 'Hand Commissions Immed.', 3);
INSERT or ignore INTO components VALUES (15, 'Leaf Hits Immed.', 3);
INSERT or ignore INTO components VALUES (16, 'Hand Hits Immed.', 3);
INSERT or ignore INTO components VALUES (17, 'Building Hits Immed.', 3);
INSERT or ignore INTO components VALUES (18, 'Flower Commissions Immed.', 3);
INSERT or ignore INTO components VALUES (20, 'Building Commissions Immed.', 3);
INSERT or ignore INTO components VALUES (21, 'Flower Omissions Immed.', 3);
INSERT or ignore INTO components VALUES (22, 'Leaf Omissions Immed.', 3);
INSERT or ignore INTO components VALUES (23, 'Hand Omissions Immed.', 3);
INSERT or ignore INTO components VALUES (24, 'Building Omissions Immed.', 3);
INSERT or ignore INTO components VALUES (25, 'TOTAL VISUAL MEM. IMMED.', 3);
INSERT or ignore INTO components VALUES (26, '% VISUAL MEM. IMMED.', 3);
INSERT or ignore INTO components VALUES (27, 'Leaf Hits Delayed', 3);
INSERT or ignore INTO components VALUES (28, 'Building Hits Delayed', 3);
INSERT or ignore INTO components VALUES (29, 'TOTAL HITS', 3);
INSERT or ignore INTO components VALUES (30, 'TOTAL COMMISSIONS', 3);
INSERT or ignore INTO components VALUES (31, 'TOTAL OMISSIONS', 3);
INSERT or ignore INTO components VALUES (32, 'TOTAL HITS DELAYED', 3);
INSERT or ignore INTO components VALUES (33, 'Leaf Commissions Delayed', 3);
INSERT or ignore INTO components VALUES (34, 'Building Commissions Delayed', 3);
INSERT or ignore INTO components VALUES (35, 'TOTAL COMMISSIONS DELAYED', 3);
INSERT or ignore INTO components VALUES (36, 'Leaf Omissions Delayed', 3);
INSERT or ignore INTO components VALUES (37, 'Building Omissions Delayed', 3);
INSERT or ignore INTO components VALUES (38, 'TOTAL OMISSIONS DELAYED', 3);
INSERT or ignore INTO components VALUES (39, 'TOTAL VISUAL MEM. DELAYED', 3);
INSERT or ignore INTO components VALUES (40, '% VISUAL MEM. DELAYED', 3);
INSERT or ignore INTO components VALUES (41, 'Arrows Part 1', 4);
INSERT or ignore INTO components VALUES (42, 'Arrows Part 2 LE', 4);
INSERT or ignore INTO components VALUES (43, 'Arrows Part 2 HE', 4);
INSERT or ignore INTO components VALUES (45, 'Blocks score', 5);
INSERT or ignore INTO components VALUES (46, 'Saved coins', 5);
INSERT or ignore INTO components VALUES (47, 'Exceeded Coins', 5);
INSERT or ignore INTO components VALUES (48, 'Coins score', 5);
INSERT or ignore INTO components VALUES (49, 'TOTAL PARTY', 5);
INSERT or ignore INTO components VALUES (50, '% PARTY', 5);
INSERT or ignore INTO components VALUES (51, 'Points & Lines 1', 6);
INSERT or ignore INTO components VALUES (52, 'Points & Lines 2', 6);
INSERT or ignore INTO components VALUES (53, 'Points & Lines 3', 6);
INSERT or ignore INTO components VALUES (54, 'Points & Lines 4', 6);
INSERT or ignore INTO components VALUES (55, 'TOTAL POINTS & LINES', 6);
INSERT or ignore INTO components VALUES (56, '% POINTS & LINES', 6);
INSERT or ignore INTO components VALUES (57, 'LE Personage spontaneous', 7);
INSERT or ignore INTO components VALUES (58, 'LE Personage cues', 7);
INSERT or ignore INTO components VALUES (59, 'HE Personage spontaneous', 7);
INSERT or ignore INTO components VALUES (60, 'HE Personage cues', 7);
INSERT or ignore INTO components VALUES (61, 'TOTAL PERSONAGE', 7);
INSERT or ignore INTO components VALUES (62, '% PERSONAGE', 7);
INSERT or ignore INTO components VALUES (64, '% ANIMALS', 11);
INSERT or ignore INTO components VALUES (5, 'WL TRIALS 1-3 SCORE', 2);
INSERT or ignore INTO components VALUES (6, '% WL TRIALS 1-3', 2);
INSERT or ignore INTO components VALUES (10, 'Related false positives', 9);
INSERT or ignore INTO components VALUES (19, 'Leaf Commissions Immed.', 3);
INSERT or ignore INTO components VALUES (44, 'Blocks travelled', 5);

--
-- Data for Name: versioned_subtests; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--

INSERT or ignore INTO versioned_subtests VALUES (1, 1, 1);
INSERT or ignore INTO versioned_subtests VALUES (2, 1, 2);
INSERT or ignore INTO versioned_subtests VALUES (3, 1, 3);
INSERT or ignore INTO versioned_subtests VALUES (4, 1, 4);
INSERT or ignore INTO versioned_subtests VALUES (5, 1, 5);
INSERT or ignore INTO versioned_subtests VALUES (6, 1, 6);
INSERT or ignore INTO versioned_subtests VALUES (7, 1, 7);
INSERT or ignore INTO versioned_subtests VALUES (8, 1, 8);
INSERT or ignore INTO versioned_subtests VALUES (9, 2, 1);
INSERT or ignore INTO versioned_subtests VALUES (10, 2, 2);
INSERT or ignore INTO versioned_subtests VALUES (11, 2, 3);
INSERT or ignore INTO versioned_subtests VALUES (12, 2, 4);
INSERT or ignore INTO versioned_subtests VALUES (13, 2, 5);
INSERT or ignore INTO versioned_subtests VALUES (14, 2, 6);
INSERT or ignore INTO versioned_subtests VALUES (15, 2, 7);
INSERT or ignore INTO versioned_subtests VALUES (16, 2, 8);
INSERT or ignore INTO versioned_subtests VALUES (19, 1, 9);
INSERT or ignore INTO versioned_subtests VALUES (20, 1, 10);
INSERT or ignore INTO versioned_subtests VALUES (21, 1, 11);
INSERT or ignore INTO versioned_subtests VALUES (22, 2, 9);
INSERT or ignore INTO versioned_subtests VALUES (23, 2, 10);
INSERT or ignore INTO versioned_subtests VALUES (24, 2, 11);


--
-- Data for Name: possible_answers; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--

INSERT or ignore INTO possible_answers VALUES (1, 'Piedra', 1);
INSERT or ignore INTO possible_answers VALUES (2, 'Oreja', 1);
INSERT or ignore INTO possible_answers VALUES (3, 'Cuello', 1);
INSERT or ignore INTO possible_answers VALUES (4, 'Nube', 1);
INSERT or ignore INTO possible_answers VALUES (5, 'Pie', 1);
INSERT or ignore INTO possible_answers VALUES (6, 'Semilla', 1);
INSERT or ignore INTO possible_answers VALUES (7, 'Lengua', 1);
INSERT or ignore INTO possible_answers VALUES (8, 'Arena', 1);
INSERT or ignore INTO possible_answers VALUES (9, 'Fuego', 1);
INSERT or ignore INTO possible_answers VALUES (10, 'Nariz', 1);
INSERT or ignore INTO possible_answers VALUES (11, '0', 3);
INSERT or ignore INTO possible_answers VALUES (12, '1', 3);
INSERT or ignore INTO possible_answers VALUES (14, 'Piedra', 8);
INSERT or ignore INTO possible_answers VALUES (15, 'Oreja', 8);
INSERT or ignore INTO possible_answers VALUES (16, 'Cuello', 8);
INSERT or ignore INTO possible_answers VALUES (17, 'Nube', 8);
INSERT or ignore INTO possible_answers VALUES (18, 'Pie', 8);
INSERT or ignore INTO possible_answers VALUES (19, 'Semilla', 8);
INSERT or ignore INTO possible_answers VALUES (20, 'Lengua', 8);
INSERT or ignore INTO possible_answers VALUES (21, 'Arena', 8);
INSERT or ignore INTO possible_answers VALUES (22, 'Fuego', 8);
INSERT or ignore INTO possible_answers VALUES (23, 'Nariz', 8);
INSERT or ignore INTO possible_answers VALUES (24, 'Árbol', 8);
INSERT or ignore INTO possible_answers VALUES (25, 'Mano', 8);
INSERT or ignore INTO possible_answers VALUES (26, 'Viento', 8);
INSERT or ignore INTO possible_answers VALUES (27, 'Rodilla', 8);


--
-- Data for Name: questions_x_attributes; Type: TABLE DATA; Schema: public; Owner: ashleymusick
--

INSERT or ignore INTO questions_x_attributes VALUES (1, 1, 1, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (60, 6, 16, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (61, 7, 17, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (62, 7, 18, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (63, 7, 19, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (64, 7, 20, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (65, 7, 21, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (66, 7, 22, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (67, 7, 23, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (68, 7, 24, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (69, 7, 25, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (70, 7, 26, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (71, 7, 27, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (72, 7, 28, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (73, 7, 29, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (74, 7, 30, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (75, 7, 31, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (76, 7, 32, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (77, 7, 33, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (78, 7, 34, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (79, 7, 35, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (80, 7, 36, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (81, 7, 37, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (82, 7, 38, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (83, 7, 39, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (84, 7, 40, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (85, 7, 41, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (86, 7, 42, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (87, 7, 43, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (88, 7, 44, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (89, 7, 45, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (90, 7, 46, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (91, 7, 47, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (92, 7, 48, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (93, 7, 49, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (94, 7, 50, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (95, 7, 51, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (96, 7, 52, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (97, 7, 53, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (98, 7, 54, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (99, 7, 55, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (100, 7, 56, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (101, 7, 57, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (102, 7, 58, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (103, 7, 59, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (104, 7, 60, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (105, 7, 61, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (106, 7, 62, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (132, 8, 29, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (133, 8, 30, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (134, 8, 31, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (135, 8, 32, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (136, 8, 33, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (137, 8, 34, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (138, 8, 35, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (139, 8, 36, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (140, 8, 37, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (141, 8, 38, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (142, 8, 39, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (143, 8, 40, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (144, 8, 64, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (145, 8, 65, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (146, 8, 66, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (147, 8, 67, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (148, 8, 68, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (149, 8, 69, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (150, 8, 70, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (151, 8, 71, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (152, 8, 72, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (153, 8, 73, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (154, 8, 74, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (155, 8, 75, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (156, 8, 76, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (157, 8, 77, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (158, 8, 78, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (159, 8, 79, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (160, 8, 80, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (161, 8, 81, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (162, 8, 82, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (163, 8, 83, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (164, 8, 84, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (165, 8, 85, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (166, 8, 86, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (167, 8, 87, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (168, 9, 88, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (169, 10, 89, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (170, 10, 90, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (171, 10, 91, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (172, 11, 92, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (173, 11, 93, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (174, 11, 94, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (175, 11, 95, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (176, 12, 96, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (177, 12, 97, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (178, 12, 98, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (179, 12, 99, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (180, 13, 102, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (181, 13, 103, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (182, 13, 104, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (183, 13, 105, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (184, 13, 106, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (185, 13, 107, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (186, 13, 108, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (187, 13, 109, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (2, 2, 2, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (56, 6, 14, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (55, 6, 13, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (54, 6, 12, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (53, 6, 11, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (52, 6, 10, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (51, 6, 9, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (50, 6, 8, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (49, 6, 7, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (48, 6, 6, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (47, 6, 5, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (45, 6, 3, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (44, 5, 16, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (43, 5, 15, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (42, 5, 14, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (41, 5, 13, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (40, 5, 12, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (39, 5, 11, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (38, 5, 10, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (37, 5, 9, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (35, 5, 7, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (34, 5, 6, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (33, 5, 5, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (32, 5, 4, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (31, 5, 3, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (30, 4, 16, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (29, 4, 15, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (28, 4, 14, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (27, 4, 13, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (26, 4, 12, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (24, 4, 10, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (23, 4, 9, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (22, 4, 8, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (21, 4, 7, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (20, 4, 6, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (19, 4, 5, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (18, 4, 4, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (17, 4, 3, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (15, 3, 15, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (14, 3, 14, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (12, 3, 12, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (11, 3, 11, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (10, 3, 10, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (9, 3, 9, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (8, 3, 8, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (7, 3, 7, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (6, 3, 6, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (5, 3, 5, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (4, 3, 4, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (3, 3, 3, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (131, 7, 86, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (130, 7, 85, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (129, 7, 84, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (128, 7, 83, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (127, 7, 82, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (126, 7, 81, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (125, 7, 80, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (124, 7, 79, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (123, 7, 78, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (122, 7, 77, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (120, 7, 75, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (119, 7, 74, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (118, 7, 73, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (117, 7, 72, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (116, 7, 71, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (115, 7, 70, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (114, 7, 69, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (113, 7, 68, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (112, 7, 67, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (111, 7, 66, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (109, 7, 64, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (107, 7, 63, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (188, 13, 110, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (189, 13, 111, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (190, 13, 112, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (191, 13, 113, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (192, 13, 114, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (193, 13, 115, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (194, 13, 116, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (195, 13, 117, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (196, 14, 102, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (197, 14, 103, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (198, 14, 104, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (199, 14, 105, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (200, 14, 106, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (201, 14, 107, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (202, 14, 108, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (203, 14, 109, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (204, 14, 110, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (205, 14, 111, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (206, 14, 112, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (207, 14, 113, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (208, 14, 114, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (209, 14, 115, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (210, 14, 116, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (211, 14, 117, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (214, 15, 118, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (215, 15, 119, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (216, 15, 120, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (217, 15, 121, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (218, 15, 122, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (219, 15, 123, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (220, 15, 124, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (221, 15, 125, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (222, 15, 126, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (223, 15, 127, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (224, 15, 128, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (225, 15, 129, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (226, 15, 130, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (227, 15, 131, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (228, 15, 132, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (229, 15, 133, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (230, 15, 134, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (231, 15, 135, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (232, 15, 136, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (233, 15, 137, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (234, 15, 138, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (235, 15, 139, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (236, 15, 140, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (237, 15, 141, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (238, 15, 142, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (239, 15, 143, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (240, 15, 144, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (241, 15, 145, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (242, 15, 146, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (243, 15, 147, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (244, 15, 148, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (245, 15, 149, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (246, 15, 150, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (247, 15, 151, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (248, 15, 152, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (249, 15, 153, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (250, 15, 154, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (251, 15, 155, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (252, 15, 156, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (253, 15, 157, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (254, 15, 158, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (255, 15, 159, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (256, 15, 160, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (257, 15, 161, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (258, 15, 162, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (259, 15, 163, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (260, 15, 164, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (261, 15, 165, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (262, 15, 166, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (263, 15, 167, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (264, 15, 168, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (265, 15, 170, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (266, 15, 171, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (267, 15, 172, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (268, 15, 173, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (269, 15, 174, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (270, 15, 175, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (271, 15, 176, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (272, 15, 177, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (273, 15, 178, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (279, 16, 183, 1613);
INSERT or ignore INTO questions_x_attributes VALUES (275, 15, 169, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (59, 6, 15, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (46, 6, 4, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (36, 5, 8, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (280, 16, 184, 1617);
INSERT or ignore INTO questions_x_attributes VALUES (25, 4, 11, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (16, 3, 16, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (13, 3, 13, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (304, 7, 87, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (121, 7, 76, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (110, 7, 65, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (276, 16, 180, 1601);
INSERT or ignore INTO questions_x_attributes VALUES (277, 16, 181, 1605);
INSERT or ignore INTO questions_x_attributes VALUES (278, 16, 182, 1609);
INSERT or ignore INTO questions_x_attributes VALUES (281, 16, 185, 1621);
INSERT or ignore INTO questions_x_attributes VALUES (282, 16, 186, 1625);
INSERT or ignore INTO questions_x_attributes VALUES (108, 7, 208, NULL);
INSERT or ignore INTO questions_x_attributes VALUES (303, 16, 207, 1628);
INSERT or ignore INTO questions_x_attributes VALUES (302, 16, 206, 1624);
INSERT or ignore INTO questions_x_attributes VALUES (301, 16, 205, 1620);
INSERT or ignore INTO questions_x_attributes VALUES (300, 16, 204, 1616);
INSERT or ignore INTO questions_x_attributes VALUES (299, 16, 203, 1612);
INSERT or ignore INTO questions_x_attributes VALUES (298, 16, 202, 1608);
INSERT or ignore INTO questions_x_attributes VALUES (297, 16, 201, 1604);
INSERT or ignore INTO questions_x_attributes VALUES (296, 16, 200, 1627);
INSERT or ignore INTO questions_x_attributes VALUES (295, 16, 199, 1623);
INSERT or ignore INTO questions_x_attributes VALUES (294, 16, 198, 1619);
INSERT or ignore INTO questions_x_attributes VALUES (293, 16, 197, 1615);
INSERT or ignore INTO questions_x_attributes VALUES (292, 16, 196, 1611);
INSERT or ignore INTO questions_x_attributes VALUES (291, 16, 195, 1607);
INSERT or ignore INTO questions_x_attributes VALUES (290, 16, 194, 1603);
INSERT or ignore INTO questions_x_attributes VALUES (289, 16, 193, 1626);
INSERT or ignore INTO questions_x_attributes VALUES (288, 16, 192, 1622);
INSERT or ignore INTO questions_x_attributes VALUES (287, 16, 191, 1618);
INSERT or ignore INTO questions_x_attributes VALUES (286, 16, 190, 1614);
INSERT or ignore INTO questions_x_attributes VALUES (285, 16, 189, 1610);
INSERT or ignore INTO questions_x_attributes VALUES (284, 16, 188, 1606);
INSERT or ignore INTO questions_x_attributes VALUES (283, 16, 187, 1602);